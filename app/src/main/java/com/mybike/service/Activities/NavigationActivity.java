package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.material.navigation.NavigationView;
import com.mybike.service.Fragments.DevicesFragment;
import com.mybike.service.Fragments.DriverFragment;
import com.mybike.service.Fragments.HomeFragment;
import com.mybike.service.Fragments.VehiclesFragment;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;

public class NavigationActivity extends AppCompatActivity implements View.OnClickListener {

    NavigationView nav_view;
    DrawerLayout drawer_layout;
    Toolbar toolbar;
    RelativeLayout relLogout;
    RelativeLayout relHome;
    RelativeLayout relMyTrips;
    RelativeLayout relAccount;
    RelativeLayout relWallet;
    RelativeLayout relAbout;
    RelativeLayout relGeo;
    RelativeLayout relFeed;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation);
        drawer_layout = findViewById(R.id.drawer_layout);
        nav_view = findViewById(R.id.nav_view);
        toolbar = findViewById(R.id.toolbar);

        relGeo = findViewById(R.id.relGeo);
        relHome = findViewById(R.id.relHome);
        relMyTrips = findViewById(R.id.relMyTrips);
        relAccount = findViewById(R.id.relAccount);
        relWallet = findViewById(R.id.relWallet);
        relAbout = findViewById(R.id.relAbout);
        relLogout = findViewById(R.id.relLogout);
        relFeed = findViewById(R.id.relFeed);

        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer_layout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);

        drawer_layout.addDrawerListener(toggle);
        toggle.syncState();

        relLogout.setOnClickListener(this);
        relHome.setOnClickListener(this);
        relMyTrips.setOnClickListener(this);
        relAccount.setOnClickListener(this);
        relWallet.setOnClickListener(this);
        relAbout.setOnClickListener(this);
        relGeo.setOnClickListener(this);
        relFeed.setOnClickListener(this);


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer_layout.isDrawerOpen(GravityCompat.START)) {
            drawer_layout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        Log.e("fdgdfgdfgd","Home");
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.relLogout:

                showPopup();
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

            case R.id.relHome:
                getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new HomeFragment()).commit();
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

            /* my trip api ---
             * https://api-aertrak-india.aeris.com/api/things/data/assets/trips?startDate=2020-08-15&endDate=2020-08-21&filterType=totalTime&accountId=9007889&assetUid=bfb4f730-dae7-11ea-8e74-7defd70ba752  */

            case R.id.relWallet:
                startActivity(new Intent(NavigationActivity.this, WalletActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

            case R.id.relAbout:
                startActivity(new Intent(NavigationActivity.this, AboutUsActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

            case R.id.relGeo:
                startActivity(new Intent(NavigationActivity.this, GeofenceActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

                case R.id.relMyTrips:
                startActivity(new Intent(NavigationActivity.this, MyTripsActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;

            case R.id.relAccount:
                startActivity(new Intent(NavigationActivity.this, ProfileActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;
            case R.id.relFeed:
                startActivity(new Intent(NavigationActivity.this, FormSubmitActivity.class));
                drawer_layout.closeDrawer(GravityCompat.START);
                break;
        }
    }

/*
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {


            case R.id.nav_home:

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.fragment_container, new HomeFragment())
                        .addToBackStack(null)
                        .commit();

                break;
            case R.id.nav_vehicles:


                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new VehiclesFragment())
                        .addToBackStack(null)
                        .commit();
                //  getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, new AddNewFragment()).commit();

                break;

            case R.id.nav_device:

                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new DevicesFragment())
                        .addToBackStack(null)
                        .commit();
                break;

            case R.id.nav_drivers:
                getSupportFragmentManager().beginTransaction()
                        .replace(R.id.fragment_container, new DriverFragment())
                        .addToBackStack(null)
                        .commit();
                break;


        }

        return true;
    }*/


    // first step helper function
    private void showPopup() {
        AlertDialog.Builder alert = new AlertDialog.Builder(NavigationActivity.this);
        alert.setMessage("Are you sure ?")
                .setPositiveButton("Logout", new DialogInterface.OnClickListener() {

                    public void onClick(DialogInterface dialog, int which) {

                        logout(); // Last step. Logout function

                    }
                }).setNegativeButton("Cancel", null);

        AlertDialog alert1 = alert.create();
        alert1.show();
    }

    private void logout() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.UserId, "");
        editor.putString(AppsContants.RideId, "");
        editor.putString(AppsContants.IgnitionType, "");
        editor.putString(AppsContants.StartIgnitiontime, "");
        editor.putString(AppsContants.FirstIgnitionStatus, "");
        editor.putString(AppsContants.DeviceId, "");
        editor.putString(AppsContants.AccoundId, "");
        editor.putString(AppsContants.Maintoken, "");
        editor.putString(AppsContants.FIRSTLAT, "");
        editor.putString(AppsContants.FIRSTLNG, "");
        editor.putString(AppsContants.LASTLAT, "");
        editor.putString(AppsContants.LASTLNG, "");
        editor.commit();
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }
}