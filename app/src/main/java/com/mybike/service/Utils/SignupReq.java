package com.mybike.service.Utils;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class SignupReq implements Serializable {

    @SerializedName("accountname")
    private String accountname;

    @SerializedName("parentid")
    private String parentid;

    @SerializedName("firstname")
    private String firstname;

    @SerializedName("lastname")
    private String lastname;

    @SerializedName("loginPref")
    private String loginPref;

    @SerializedName("phone")
    private String phone;

    @SerializedName("password")
    private String password;

    @SerializedName("location")
    private LocationReq location;

    @SerializedName("timezone")
    private String timezone;

    public String getAccountname() {
        return accountname;
    }

    public void setAccountname(String accountname) {
        this.accountname = accountname;
    }

    public String getParentid() {
        return parentid;
    }

    public void setParentid(String parentid) {
        this.parentid = parentid;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getLoginPref() {
        return loginPref;
    }

    public void setLoginPref(String loginPref) {
        this.loginPref = loginPref;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocationReq getLocation() {
        return location;
    }

    public void setLocation(LocationReq location) {
        this.location = location;
    }

    public String getTimezone() {
        return timezone;
    }

    public void setTimezone(String timezone) {
        this.timezone = timezone;
    }
}
