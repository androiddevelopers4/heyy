package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.KeyguardManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.os.Handler;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polygon;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mybike.service.Modals.TripModal;
import com.mybike.service.R;
import com.mybike.service.Room.DatabaseClient;
import com.mybike.service.Room.Item;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.DirectionsJSONParser;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LoginResponse;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class OperateVehicleActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {

    TextView txtStart;
    TextView txtBreak;
    TextView txtEnd;
    LinearLayout linearBottom;
    String strDeviceId = "";
    String strAccoundId = "";
    String strParentToken = "";
    String strVehicleValue = "";
    String strStatus = "";
    ProgressDialog progressDialog;
    String strAssetUID = "";
    String strFName = "";
    TextView txtShowTimeLeft;
    TextView txtShowKMLeft;
    String strUserKM;
    String strIgnitionType;
    int KmHours = 0;
    int intCount = 0;
    Handler handler;
    Runnable runnable;

    Handler handler2;
    Runnable runnable2;
    int count = 0;

    public static Handler handlerKm;
    public static Runnable runnableKm;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String strUserId = "";
    String strRideId = "";

    GPSTracker gpsTracker;
    String strVehicleSpeed = "";

    int speedcounter = 0;
    double decimalUserKm;
    String strPackageID = "";
    RelativeLayout relBottom;
    String strStationPathSatus = "";
    String strStationLng = "";
    String strStationLat = "";
    String strPACKAGEID = "";
    String strCLat = "";
    String strCLng = "";
    boolean bolRun = true;
    Dialog dialog;
    TextView txtEndRideOnStation;
    String totalRemain = "";
    ProgressDialog commandDialog;

    Handler handlerPopup;
    Runnable runnablePopup;

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_operate_vehicle);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        /*AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.USER_SELECTED_KM, "2");
        editor.apply();*/

        /* this api is to get vehicle speed https://api-aertrak-india.aeris.com/v1.0/api/things/accounts/vehicle/latest-location */
        /* its json body { "dataFlag":"LOCATION"  } */

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        strAccoundId = AppsContants.sharedpreferences.getString(AppsContants.AccoundId, "");
        strFName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        strUserKM = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, "");
        strIgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
        strRideId = AppsContants.sharedpreferences.getString(AppsContants.RideId, "");
        strPackageID = AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID, "");
        strStationPathSatus = AppsContants.sharedpreferences.getString(AppsContants.StationPathSatus, "");
        strPACKAGEID = AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID, "");

      //  strDeviceId = "0358979100012932"; /*Black Scooter*/
        //strAccoundId = "9007889";


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.TestKm, AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, ""));
        editor.commit();


        Log.e("sdhfsjkfsdfsdf", strStationPathSatus);

        if (strStationPathSatus.equals("1")) {

            strStationLat = AppsContants.sharedpreferences.getString(AppsContants.StationLat, "");
            strStationLng = AppsContants.sharedpreferences.getString(AppsContants.StationLng, "");
        }
        commandDialog = new ProgressDialog(OperateVehicleActivity.this);
        decimalUserKm = Double.parseDouble(strUserKM);

        handlerPopup = new Handler();
        handlerKm = new Handler();
        handler = new Handler();
        handler2 = new Handler();

        gpsTracker = new GPSTracker(OperateVehicleActivity.this);
        // GetMyTripId();
        Log.e("asfdasfafafa", strUserId);
        Log.e("dfgdfgdfgdf", strAccoundId);
        Log.e("dfgdfgdfgdf", strUserKM);
        Log.e("dfgdfgdfgdf", strDeviceId);
        Log.e("dfgdfgdfgdf", strRideId);
        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map.getMapAsync(this);

        txtEndRideOnStation = findViewById(R.id.txtEndRide);
        txtShowKMLeft = findViewById(R.id.txtShowKMLeft);
        txtShowTimeLeft = findViewById(R.id.txtShowTimeLeft);
        relBottom = findViewById(R.id.relBottom);
        txtStart = findViewById(R.id.txtStart);
        txtBreak = findViewById(R.id.txtBreak);
        txtEnd = findViewById(R.id.txtEnd);
        linearBottom = findViewById(R.id.linearBottom);


        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                StartVehicle();
            }
        });
        /*for drop off Feature*/
        if (strStationPathSatus.equals("1")) {

            linearBottom.setVisibility(View.GONE);
            txtEndRideOnStation.setVisibility(View.VISIBLE);
            txtStart.setVisibility(View.GONE);
           /* bolRun=false;
            Log.e("sdfsdfssd",bolRun+"");


            if (strIgnitionType.equals("REMOTE_IGNITION_OFF")) {
                linearBottom.setVisibility(View.VISIBLE);
                txtBreak.setText("START AGAIN");
                txtBreak.setBackgroundColor(getResources().getColor(R.color.colorAccent));
            } else {

                linearBottom.setVisibility(View.VISIBLE);
                txtBreak.setText("TAKE A BREAK");
                txtBreak.setBackgroundColor(getResources().getColor(R.color.yellow));
            }
*/
        } else {

            if (strIgnitionType.equals("REMOTE_IGNITION_ON")) {

                txtStart.setVisibility(View.GONE);
                linearBottom.setVisibility(View.VISIBLE);
            } else {
                linearBottom.setVisibility(View.GONE);
                txtStart.setVisibility(View.VISIBLE);
            }
        }


        txtBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (txtBreak.getText().toString().equals("TAKE A BREAK")) {
                    AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
                    alert.setMessage("Are you sure you want to take a break?")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {
                                    TakeBreak();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_OFF");
                                    editor.commit();
                                }
                            }).setNegativeButton("Cancel", null);

                    AlertDialog alert1 = alert.create();
                    alert1.show();

                } else {

                    AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
                    alert.setMessage("Are you sure you want to start again?")
                            .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                                public void onClick(DialogInterface dialog, int which) {

                                    StartAgain();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_ON");
                                    editor.commit();
                                }
                            }).setNegativeButton("Cancel", null);

                    AlertDialog alert1 = alert.create();
                    alert1.show();

                }

            }
        });


        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GPSTracker gpsTracker = new GPSTracker(OperateVehicleActivity.this);
                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
                editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
                editor.putString(AppsContants.RenewStatus, "1");
                editor.commit();
                // EndRideMaually();

                ManualEndRidePopup();
            }
        });

        LinearLayout relBikeDetail = findViewById(R.id.relBikeDetail);
        relBikeDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(OperateVehicleActivity.this, VehicleDetailActivity.class));
            }
        });
        GetParentAccountToken();
        txtShowKMLeft.setText(decimalUserKm + " " + "Km left");


        KeyguardManager keyguardManager = (KeyguardManager) getSystemService(Activity.KEYGUARD_SERVICE);
        KeyguardManager.KeyguardLock lock = keyguardManager.newKeyguardLock(KEYGUARD_SERVICE);
        lock.disableKeyguard();


        if (strStationPathSatus.equals("1")) {

            UserKilometer();
        }

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                OperateVehicleActivity.this, R.raw.maps_style);
        googleMap.setMapStyle(style);

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(OperateVehicleActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }


    }


    public void DrawPath(String strCLat, String strCLng) {
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.25); // offset from edges of the map 10% of screen

        LatLng s_latLng = null;

        double slat = Double.parseDouble(strCLat);
        double slng = Double.parseDouble(strCLng);

        GPSTracker gpsTracker = new GPSTracker(this);
        s_latLng = new LatLng(slat, slng);

        if (s_latLng != null) {

            int heights = 100;
            int widths = 80;
            BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
            Bitmap bs = bitmapdraws.getBitmap();

            Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(s_latLng)
                    .title("User Location")
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers)));
            builder.include(marker.getPosition());
        }


        double dLat = Double.parseDouble(strStationLat);
        double dLng = Double.parseDouble(strStationLng);
        LatLng d_latLng = new LatLng(dLat, dLng);

        if (d_latLng != null) {

            int heights = 100;
            int widths = 90;
            Bitmap bs;
            String Title = "";

            BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.ddd);
            bs = bitmapdraws.getBitmap();
            Title = "Vehicle";

            Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
            Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                    .position(d_latLng)
                    .title(Title)
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))
            );
            builder.include(marker.getPosition());
        }

        if (s_latLng != null && d_latLng != null) {
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            mGoogleMap.animateCamera(cu);
            String url = getDirectionsUrl(s_latLng, d_latLng);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);

            mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(s_latLng, 18F));
        } else {

            Toast.makeText(OperateVehicleActivity.this, "Enter Source and Destination Address", Toast.LENGTH_SHORT).show();
        }


    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        // String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyClkiUQxIeczQEjTrw-dy9h2fj9qjXDk_4";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyAURsUjEy5yIa7CTgW_vyn8f5L7iKXz5DI";
        return url;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            try {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = result.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(R.color.red);
                    mGoogleMap.addPolyline(lineOptions);
                }
            } catch (Exception ex) {
                Log.e("dfgdfgdgd", ex.getMessage());
            }
        }
    }


    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.e("fdgdfgdfgd", "Operate");
    }

    private void isPointInPolygon(LatLng tap, ArrayList<LatLng> vertices) {
        int intersectCount = 0;
        for (int j = 0; j < vertices.size() - 1; j++) {
            if (rayCastIntersect(tap, vertices.get(j), vertices.get(j + 1))) {
                intersectCount++;
            }
        }
        boolean bol = (intersectCount % 2) == 1;
        Log.e("sdsdsdsdsds", bol + "");

        if (bol == false) {
            AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
            alert.setMessage("Scooter is running outside of place..")
                    .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {

                            // startActivity(new Intent(OperateVehicleActivity.this, MyTripsActivity.class));
                            // finish();

                        }
                    }).setNegativeButton("New Package", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {

                    //  startActivity(new Intent(OperateVehicleActivity.this, PricingListActivity.class));
                    //   finish();

                }
            });

            AlertDialog alert1 = alert.create();
            alert1.show();

        }

        // return ((intersectCount % 2) == 1); // odd = inside, even = outside;
    }


    private boolean rayCastIntersect(LatLng tap, LatLng vertA, LatLng vertB) {

        double aY = vertA.latitude;
        double bY = vertB.latitude;
        double aX = vertA.longitude;
        double bX = vertB.longitude;
        double pY = tap.latitude;
        double pX = tap.longitude;

        if ((aY > pY && bY > pY) || (aY < pY && bY < pY)
                || (aX < pX && bX < pX)) {
            return false; // a and b can't both be above or below pt.y, and a or
            // b must be east of pt.x
        }

        double m = (aY - bY) / (aX - bX); // Rise over run
        double bee = (-aX) * m + aY; // y = mx + b
        double x = (pY - bee) / m; // algebra is neat!

        return x > pX;
    }


    @Override
    protected void onResume() {
        super.onResume();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");

        if (FirstIgnitionStatus.equals("Yes")) {

            handler.postDelayed(runnable = new Runnable() {
                @Override
                public void run() {
                    Log.e("payment", "Running");
                    printDifference();
                    handler.postDelayed(runnable, 1000);
                }
            }, 1000);


            handler2.postDelayed(runnable2 = new Runnable() {
                @Override
                public void run() {
                    Log.e("payment", "Trip is Running");
                    GetTripDetails();
                    handler2.postDelayed(runnable2, 2000);
                }
            }, 2000);

            handlerKm.removeCallbacks(runnableKm);
            handlerKm = new Handler();
            handlerKm.postDelayed(runnableKm = new Runnable() {
                @Override
                public void run() {
                    Log.e("payment", "Running");
                    Log.e("awfasfasfasfaf", "on Resume Km Timer");
                    UserKilometer();
                    handlerKm.postDelayed(runnableKm, 10500);
                }
            }, 10500);
        }

        if (mGoogleApiClient != null &&
                ContextCompat.checkSelfPermission(OperateVehicleActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }


    @Override
    public void onPause() {
        super.onPause();

       /* //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }*/
    }


    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {
            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
            String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");
            if (FirstIgnitionStatus.equals("Yes")) {

                AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
                alert.setMessage("Are you sure you want to end this ride?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                handler.removeCallbacksAndMessages(null);
                                handler2.removeCallbacksAndMessages(null);
                                handlerKm.removeCallbacksAndMessages(null);

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
                                editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
                                editor.putString(AppsContants.RenewStatus, "1");
                                editor.commit();
                                EndRideMaually();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();

                    }
                });

                AlertDialog alert1 = alert.create();
                alert1.show();

            } else {

                startActivity(new Intent(OperateVehicleActivity.this, ScanQrcodeActivity.class));
                finish();
            }
            return true;
        } else {
            return false;
        }
    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(OperateVehicleActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(0.1f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(OperateVehicleActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }


    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {

        // strVehicleSpeed = String.valueOf(location.getSpeed());
        // Toast.makeText(this, strVehicleSpeed, Toast.LENGTH_SHORT).show();
    }

    public void GetParentAccountToken() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<LoginResponse> loginResponseCall = service.userLogin("suryanbajpai33@gmail.com", "Manirock33");

        progressDialog = new ProgressDialog(OperateVehicleActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                //Log.e("ddsgdfgdfg",response.toString());
                try {
                    String status = response.body().getToken();
                    Log.e("ddsgdfgdfg", status);
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.Maintoken, status);
                    editor.commit();
                    // txtAccountId.setText(strFName);
                    // txtDeviceId.setText("Test Vehicle_1");
                    progressDialog.dismiss();
                    GetAssetUID();
                    // UserKilometer();

                } catch (Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(OperateVehicleActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ddsgdfgdfg", t.getMessage());
                Toast.makeText(OperateVehicleActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void GetAssetUID() {
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        AndroidNetworking.get("https://api-aertrak-india.aeris.com/api/fleet/assets")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.e("wetwertwerwerwer", response.toString());

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                if (strDeviceId.equals(jsonObject.getString("deviceId"))) {
                                    strAssetUID = jsonObject.getString("assetUid");
                                }
                            }
                            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.AssetId, strAssetUID);
                            editor.commit();
                            GetTripDetails();
                            Log.e("dsfgsdfsdfsd", strAssetUID);

                        } catch (Exception ex) {

                            Log.e("wetwertwerwerwer", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("wetwertwerwerwer", "Error" + anError.getMessage());
                    }
                });
    }

    public void printDifference() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strWhenStopVehicleTime = AppsContants.sharedpreferences.getString(AppsContants.StartIgnitiontime, "");

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.getDefault());
        String currentTime = df.format(c);
        Log.e("eryuweyuui", currentTime);
        Log.e("eryuweyuui", strWhenStopVehicleTime);
        Date start = null;
        Date end = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        try {
            start = simpleDateFormat.parse(currentTime);
            end = simpleDateFormat.parse(strWhenStopVehicleTime);
        } catch (ParseException e) {
            Log.e("sdfsdfsdsdf", e.getMessage());
        }

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        //milliseconds
        try {
            long different = end.getTime() - start.getTime();
            System.out.println("startDate : " + start);
            System.out.println("endDate : " + end);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;

            long elapsedHours = different / hoursInMilli;

            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;

            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            System.out.printf("%d hours, %d minutes, %d seconds%n", elapsedHours, elapsedMinutes, elapsedSeconds);
            Log.e("wrewerwerewrr", elapsedHours + "");/*agar minus me ata h to hme AM or PM bhi lena pdega date format me*/
            Log.e("wrewerwerewrr", elapsedMinutes + "");

            // Log.e("dfdfsfsdfsdfdfdsd",elapsedHours, elapsedMinutes, elapsedSeconds)

            if (elapsedHours < 1) {
                Log.e("dfggdgdfgd", "Hour is less than 0");

                if (elapsedMinutes < 1) {

                    Log.e("dfggdgdfgd", "Minute is less than 0");

                    if (elapsedSeconds < 1) {
                        txtShowTimeLeft.setText("0" + " " + "h" + " " + "0" + " " + "m" + " " + "0" + " " + "s");
                        txtShowKMLeft.setText("0" + " " + "Km left");
                        Log.e("dfggdgdfgd", "Seconds is less than 0");
                        EndRide();
                        Log.e("zzzzzzzzzz", "0 Seconds");
                    } else {

                        txtShowTimeLeft.setText("0" + " " + "h" + " " + "0" + " " + "m" + " " + elapsedSeconds + " " + "s");
                    }
                } else {

                    txtShowTimeLeft.setText("0" + " " + "h" + " " + elapsedMinutes + " " + "m" + " " + elapsedSeconds + " " + "s");
                }
            } else {

                txtShowTimeLeft.setText(elapsedHours + " " + "h" + " " + elapsedMinutes + " " + "m" + " " + elapsedSeconds + " " + "s");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetTripDetails() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String strtDate = dateFormat.format(date);
        Log.e("wertwerwerew", strtDate);

        AndroidNetworking.get("https://api-aertrak-india.aeris.com/api/things/data/assets/trips?startDate=" + strtDate + "&endDate=" + strtDate + "&filterType=totalTime&accountId=" + strAccoundId + "&assetUid=" + strAssetUID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {


                            String tripId = "";

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                String TotalKm = jsonObject.getString("totalKm");
                                tripId = jsonObject.getString("_id");
                                strCLat = jsonObject.getString("endLat");
                                strCLng = jsonObject.getString("endLong");

                            }

                            if (strStationPathSatus.equals("1")) {
                                mGoogleMap.clear();
                                DrawPath(strCLat, strCLng);
                            } else {

                                mGoogleMap.clear();
                                if (!strCLat.equals("") || !strCLat.equals(null)) {
                                    LatLng latLng = new LatLng(Double.parseDouble(strCLat), Double.parseDouble(strCLng));
                                    int heights = 80;
                                    int widths = 80;
                                    BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                    Bitmap bs = bitmapdraws.getBitmap();

                                    Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
                                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .flat(true)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))
                                    );

                                    mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));


                                }
                            }


                        } catch (Exception ex) {

                            Log.e("asfasfasfasf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("asfafasfasfa", "Error" + anError.getMessage());
                    }
                });

    }


    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }


    public void UserKilometer() {

        Log.e("dgdjgdkghdkgkd", "gfhgfhfgh" + strStationPathSatus);

        if (strStationPathSatus.equals("1")) {

            gpsTracker = new GPSTracker(OperateVehicleActivity.this);
            Location startPoint = new Location("locationA");
            startPoint.setLatitude(gpsTracker.getLatitude());
            startPoint.setLongitude(gpsTracker.getLongitude());

            double stationLat = Double.parseDouble(strStationLat);
            double stationLng = Double.parseDouble(strStationLng);
            Location endPoint = new Location("locationB");
            endPoint.setLatitude(stationLat);
            endPoint.setLongitude(stationLng);

            String strStrAddLat = String.valueOf(gpsTracker.getLatitude());
            String strStrAddLNG = String.valueOf(gpsTracker.getLongitude());


            Log.e("Hom", "UserLocation" + " " + strStrAddLat);
            Log.e("ssffdfdsfds", "UserLocation" + " " + strStrAddLNG);

            Log.e("ssffdfdsfds", "stationLat" + " " + strStationLat);
            Log.e("ssffdfdsfds", "stationLng" + " " + strStationLng);


            double distance = startPoint.distanceTo(endPoint);
            Log.e("ssffdfdsfds", distance + "");

            String strDistance = String.valueOf(distance);

            if (strDistance.contains(".")) {

                String strSplit[] = strDistance.split("\\.");
                totalRemain = strSplit[0];
            }
            Log.e("sdfsdjfsdkfjsdf", totalRemain);


            // Toast.makeText(OperateVehicleActivity.this, totalRemain+" "+"metres away from drop point", Toast.LENGTH_SHORT).show();
            txtEndRideOnStation.setText("END RIDE");
            txtEndRideOnStation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (distance < 50.0) {

                        dialog = new Dialog(OperateVehicleActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custum_popup);

                        Button dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
                        dialog_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button dialog_ok = (Button) dialog.findViewById(R.id.dialog_ok);
                        dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                GPSTracker gpsTracker = new GPSTracker(OperateVehicleActivity.this);
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
                                editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
                                editor.putString(AppsContants.RenewStatus, "1");
                                editor.commit();
                                EndRideOnStopage();
                                dialog.dismiss();
                            }
                        });

                        dialog.show();

                    } else {


                        dialog = new Dialog(OperateVehicleActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.custum_popup);

                        Button dialog_cancel = (Button) dialog.findViewById(R.id.dialog_cancel);
                        dialog_cancel.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });

                        Button dialog_ok = (Button) dialog.findViewById(R.id.dialog_ok);
                        dialog_ok.setText(totalRemain + " " + "Metres away");
                       /* dialog_ok.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {

                                GPSTracker gpsTracker= new GPSTracker(OperateVehicleActivity.this);
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
                                editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
                                editor.putString(AppsContants.RenewStatus, "1");
                                editor.commit();
                                EndRideOnStopage();
                                dialog.dismiss();
                            }
                        });*/

                        dialog.show();


                    }

                }
            });

               /* AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
                alert.setMessage("Scooter is nearby of drop point.")
                        .setPositiveButton("End Ride", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                EndRideMaually();
                            }
                        }).setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                    }
                });
                AlertDialog alert1 = alert.create();
                //alert1.show();*/


        }



        /*center point*/

     /*   LatLng latLng1=new LatLng(23.2329791028709,77.39879584873005);
        LatLng latLng2=new LatLng(23.232504646412877,77.39878511989399);
        LatLng latLng3=new LatLng(23.232585981925467,77.3995254095821);
        LatLng latLng4=new LatLng(23.232612972969083,77.39963225896324);
        LatLng latLng5=new LatLng(23.233098520202777,77.39958800251449);
        LatLng latLng6=new LatLng(23.233064014366295,77.39918030674423);

        ArrayList<LatLng> vertices=new ArrayList<>();
        vertices.add(latLng1);
        vertices.add(latLng2);
        vertices.add(latLng3);
        vertices.add(latLng4);
        vertices.add(latLng5);
        vertices.add(latLng6);*/

        // isPointInPolygon(userLocation,vertices);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();
        try {

            jsonObject.put("dataFlag", "LOCATION");
            Log.e("DFgdfgdfgf", jsonObject.toString());
            Log.e("DFgdfgdfgf", strDeviceId);
            Log.e("DFgdfgdfgf", strParentToken);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/accounts/vehicle/latest-location")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("wetwerwerwer", response.toString());
                            if (response.getString("status").equals("Success")) {
                                JSONArray jsonArray = new JSONArray(response.getString("data"));
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    JSONObject jsonObject1 = jsonArray.getJSONObject(i);
                                    if (jsonObject1.getString("deviceId").equals(strDeviceId)) {

                                        JSONArray jsonArray1 = new JSONArray(jsonObject1.getString("vehicleSpeed"));
                                        for (int j = 0; j < jsonArray1.length(); j++) {
                                            JSONObject jsonObject2 = jsonArray1.getJSONObject(j);

                                            if (jsonObject2.getString("unit").equals("km/hr")) {
                                                strVehicleValue = jsonObject2.getString("value");
                                                Log.e("etggdgaggag", strVehicleValue);
                                                if (strVehicleValue.contains(".")) {

                                                    double strGetSpeed = Double.parseDouble(strVehicleValue);
                                                    Log.e("ewtwetwetwe", "If" + strGetSpeed + "");

                                                    if (strGetSpeed > 2.0) {

                                                        speedcounter = 0;
                                                        if (speedcounter == 0) {
                                                            // Toast.makeText(this, "Start lapsing km", Toast.LENGTH_SHORT).show();
                                                            if (intCount == 0) {
                                                                BigDecimal val1BD;
                                                                BigDecimal val2BD;
                                                                BigDecimal result;
                                                                val1BD = new BigDecimal(decimalUserKm);
                                                                val2BD = new BigDecimal(0.1);
                                                                result = val1BD.subtract(val2BD);
                                                                double userKm = result.doubleValue();
                                                                Log.e("werwerwerwer", strUserKM);
                                                                Log.e("werwerwerwer", userKm + "");

                                                                if (userKm < 0.1) {
                                                                    txtShowKMLeft.setText("0" + " " + "Km left");

                                                                    EndRide();
                                                                    Log.e("zzzzzzzzzz", "If 0 Left");
                                                                } else {
                                                                    txtShowKMLeft.setText(userKm + " " + "Km left");
                                                                }

                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                                editor.putString(AppsContants.UserLapseKm, userKm + "");
                                                                editor.putString(AppsContants.USER_SELECTED_KM, userKm + "");
                                                                editor.commit();
                                                                intCount = 1;
                                                            } else {
                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                String strUserLapseKm = AppsContants.sharedpreferences.getString(AppsContants.UserLapseKm, "");
                                                                decimalUserKm = Double.parseDouble(strUserLapseKm);
                                                                BigDecimal val1BD;
                                                                BigDecimal val2BD;
                                                                BigDecimal result;
                                                                val1BD = new BigDecimal(decimalUserKm);
                                                                val2BD = new BigDecimal(0.1);
                                                                result = val1BD.subtract(val2BD);
                                                                double userKm = result.doubleValue();
                                                                Log.e("werwerwerwer", strUserKM);
                                                                Log.e("werwerwerwer", result.doubleValue() + "");

                                                                if (userKm < 0.1) {
                                                                    txtShowKMLeft.setText("0" + " " + "Km left");

                                                                    EndRide();
                                                                    Log.e("zzzzzzzzzz", "0 Left");
                                                                } else {
                                                                    DecimalFormat precision = new DecimalFormat("0.0");
                                                                    txtShowKMLeft.setText(precision.format(userKm) + " " + "Km left");
                                                                }

                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                                editor.putString(AppsContants.UserLapseKm, userKm + "");
                                                                editor.putString(AppsContants.USER_SELECTED_KM, userKm + "");
                                                                editor.commit();
                                                            }
                                                        }
                                                    }
                                                    else {

                                                        speedcounter = 1;
                                                        //  Toast.makeText(this, "Stop lapsing km", Toast.LENGTH_SHORT).show();
                                                    }
                                                } else {
                                                    Log.e("ewtwetwetwe", "Else" + strVehicleValue);
                                                    int strGetSpeed = Integer.parseInt(strVehicleValue);
                                                    Log.e("ewtwetwetwe", "Else" + strGetSpeed + "");

                                                    if (strGetSpeed > 2) {

                                                        speedcounter = 0;

                                                        if (speedcounter == 0) {
                                                            // Toast.makeText(this, "Start lapsing km", Toast.LENGTH_SHORT).show();

                                                            if (intCount == 0) {
                                                                BigDecimal val1BD;
                                                                BigDecimal val2BD;
                                                                BigDecimal result;
                                                                val1BD = new BigDecimal(decimalUserKm);
                                                                val2BD = new BigDecimal(0.1);
                                                                result = val1BD.subtract(val2BD);
                                                                double userKm = result.doubleValue();
                                                                Log.e("werwerwerwer", strUserKM);
                                                                Log.e("werwerwerwer", userKm + "");

                                                                if (userKm < 0.1) {
                                                                    txtShowKMLeft.setText("0" + " " + "Km left");

                                                                    EndRide();
                                                                    Log.e("zzzzzzzzzz", "If 0 Left");
                                                                } else {
                                                                    txtShowKMLeft.setText(userKm + " " + "Km left");
                                                                }

                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                                editor.putString(AppsContants.UserLapseKm, userKm + "");
                                                                editor.putString(AppsContants.USER_SELECTED_KM, userKm + "");
                                                                editor.commit();
                                                                intCount = 1;
                                                            } else {
                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                String strUserLapseKm = AppsContants.sharedpreferences.getString(AppsContants.UserLapseKm, "");
                                                                decimalUserKm = Double.parseDouble(strUserLapseKm);
                                                                BigDecimal val1BD;
                                                                BigDecimal val2BD;
                                                                BigDecimal result;
                                                                val1BD = new BigDecimal(decimalUserKm);
                                                                val2BD = new BigDecimal(0.1);
                                                                result = val1BD.subtract(val2BD);
                                                                double userKm = result.doubleValue();
                                                                Log.e("werwerwerwer", strUserKM);
                                                                Log.e("werwerwerwer", result.doubleValue() + "");

                                                                if (userKm < 0.1) {
                                                                    txtShowKMLeft.setText("0" + " " + "Km left");

                                                                    EndRide();
                                                                    Log.e("zzzzzzzzzz", "0 Left");
                                                                } else {
                                                                    DecimalFormat precision = new DecimalFormat("0.0");
                                                                    txtShowKMLeft.setText(precision.format(userKm) + " " + "Km left");
                                                                }

                                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                                editor.putString(AppsContants.UserLapseKm, userKm + "");
                                                                editor.putString(AppsContants.USER_SELECTED_KM, userKm + "");
                                                                editor.commit();
                                                            }
                                                        }
                                                    } else {
                                                        speedcounter = 1;
                                                        //  Toast.makeText(this, "Stop lapsing km", Toast.LENGTH_SHORT).show();
                                                    }

                                                }
                                            }
                                        }
                                    }
                                }
                            } else {
                                Toast.makeText(OperateVehicleActivity.this, (response.getString("status")), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {


                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

      /*  if (strVehicleSpeed.contains(".")) {
            String strSpilit[] = strVehicleSpeed.split("\\.");
            int strGetSpeed = Integer.parseInt(strSpilit[0]);

            Log.e("dfgdfgdgd", strGetSpeed + "");*/
        // strGetSpeed=2;


        //Log.e("etggdgaggag", "check"+strVehicleValue);


    }


    // }


    public void EndRideOnStopage() {

        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_OFF");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog commandDialog = new ProgressDialog(OperateVehicleActivity.this);
        commandDialog.setMessage("Ignition off..");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */

        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {
                                    // linearBottom.setVisibility(View.GONE);
                                    // txtStart.setVisibility(View.GONE);
                                    // relBottom.setVisibility(View.GONE);
                                    commandDialog.dismiss();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_OFF");
                                    editor.commit();

                                    // txtBreak.setText("START AGAIN");
                                    //  txtBreak.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                                    handler.removeCallbacksAndMessages(null);
                                    handler2.removeCallbacksAndMessages(null);
                                    handlerKm.removeCallbacksAndMessages(null);


                                    AddTripDetail();


                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void EndRideMaually() {

        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_OFF");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */

        commandDialog.setMessage("Ending Ride...");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();

        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {
                                    // linearBottom.setVisibility(View.GONE);
                                    // txtStart.setVisibility(View.GONE);
                                    // relBottom.setVisibility(View.GONE);
                                    commandDialog.dismiss();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_OFF");
                                    editor.commit();

                                    txtBreak.setText("START AGAIN");
                                    txtBreak.setBackgroundColor(getResources().getColor(R.color.colorAccent));

                                   /* handler.removeCallbacksAndMessages(null);
                                    handler2.removeCallbacksAndMessages(null);
                                    handlerKm.removeCallbacksAndMessages(null);*/

                                    //  ManualEndRidePopup();  /*end ride maually*/

                                    // AddTripDetail();

                                    deductCoins(strUserId);


                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void ManualEndRidePopup() {

        AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
        alert.setMessage("End Ride")
                .setPositiveButton("Pickup Service", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        //  AddTripDetailMaually();
                        ShopArrangePopup();

                    }
                }).setNegativeButton("Drop Off", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                // AddTripDetailDropOff();
                // handlerKm.removeCallbacks(runnableKm);

                startActivity(new Intent(OperateVehicleActivity.this, DropVehicleStations.class));


                //  EndRideMaually();

            }
        });

        AlertDialog alert1 = alert.create();
        alert1.show();
    }


    public void EndRide() {

        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_OFF");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/


        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */

        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {
                                    //  linearBottom.setVisibility(View.GONE);
                                    // txtStart.setVisibility(View.GONE);
                                    relBottom.setVisibility(View.GONE);
                                    commandDialog.dismiss();

                                    handler.removeCallbacks(runnable);
                                    handler2.removeCallbacks(runnable2);
                                    handlerKm.removeCallbacks(runnable2);


                                    showPopup();

                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void StartVehicle() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        Log.e("dgdfgdfgdfg", strAssetUID);
        Log.e("dgdfgdfgdfg", strParentToken);
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_ON");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog onDialog = new ProgressDialog(OperateVehicleActivity.this);
        onDialog.setMessage("Ignition on..");
        onDialog.setIndeterminate(true);
        onDialog.setCancelable(false);
        onDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());


                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {


                                    onDialog.dismiss();

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");
                                    if (FirstIgnitionStatus.equals("")) {

                                        if (strUserKM.equals("60")) {
                                            KmHours = 10;
                                        } else if (strUserKM.equals("30")) {

                                            KmHours = 5;
                                        } else if (strUserKM.equals("20")) {

                                            KmHours = 3;
                                        } else {
                                            KmHours = 1;
                                        }

                                        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                                        String currentDateandTime = sdf.format(new Date());

                                        Calendar calendar = null;
                                        try {
                                            Date date = sdf.parse(currentDateandTime);
                                            calendar = Calendar.getInstance();
                                            calendar.setTime(date);
                                            calendar.add(Calendar.HOUR, KmHours);
                                        } catch (ParseException e) {
                                            Log.e("gsdgsgssgsg", e.getMessage());
                                        }

                                        Log.e("werfffasfa", sdf.format(calendar.getTime()));
                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                        editor.putString(AppsContants.StartIgnitiontime, sdf.format(calendar.getTime()));
                                        editor.commit();

                                    }

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_ON");
                                    editor.putString(AppsContants.FIRSTLAT, gpsTracker.getLatitude() + "");
                                    editor.putString(AppsContants.FIRSTLNG, gpsTracker.getLongitude() + "");
                                    editor.commit();
                                    handler.postDelayed(runnable = new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("awfasfasfasfaf", "Button Resume Hour Timer");
                                            printDifference();
                                            handler.postDelayed(runnable, 1000);
                                        }
                                    }, 1000);

                                    handler2.postDelayed(runnable2 = new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("awfasfasfasfaf", "Button Resume Trip Timer");
                                            GetTripDetails();
                                            handler2.postDelayed(runnable2, 3000);
                                        }
                                    }, 3000);


                                    linearBottom.setVisibility(View.VISIBLE);
                                    txtStart.setVisibility(View.GONE);
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editors = AppsContants.sharedpreferences.edit();
                                    editors.putString(AppsContants.FirstIgnitionStatus, "Yes");
                                    editors.commit();
                                    printDifference();

                                    handlerKm.postDelayed(runnableKm = new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("payment", "Running");

                                            Log.e("awfasfasfasfaf", "Button Resume Km Timer");
                                            UserKilometer();
                                            handlerKm.postDelayed(runnableKm, 10500);
                                        }
                                    }, 10500);
                                } else {
                                    onDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                onDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            onDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        onDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void TakeBreak() {
        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_OFF");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog OffDialog = new ProgressDialog(OperateVehicleActivity.this);
        OffDialog.setMessage("Ignition off..");
        OffDialog.setIndeterminate(true);
        OffDialog.setCancelable(false);
        OffDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {
                                    // handlerKm.removeCallbacksAndMessages(null);

                                    txtBreak.setText("START AGAIN");
                                    txtBreak.setBackgroundColor(getResources().getColor(R.color.colorAccent));
                                    OffDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                                } else {
                                    OffDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                OffDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            OffDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        OffDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void StartAgain() {
        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_ON");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/
        ProgressDialog IgnitionDialog = new ProgressDialog(OperateVehicleActivity.this);
        IgnitionDialog.setMessage("Ignition on..");
        IgnitionDialog.setIndeterminate(true);
        IgnitionDialog.setCancelable(false);
        IgnitionDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("status")) {
                                if (response.getString("status").equals("accepted")) {

                                   /* handlerKm.postDelayed(runnableKm = new Runnable() {
                                        @Override
                                        public void run() {
                                            Log.e("payment", "Running");
                                            Log.e("awfasfasfasfaf", "Button Resume Km Timer");
                                            UserKilometer();
                                            handlerKm.postDelayed(runnableKm, 11000);
                                        }
                                    }, 11000);*/
                                    IgnitionDialog.dismiss();
                                    txtBreak.setText("TAKE A BREAK");
                                    txtBreak.setBackgroundColor(getResources().getColor(R.color.yellow));
                                    Toast.makeText(OperateVehicleActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                                } else {
                                    IgnitionDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                IgnitionDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            IgnitionDialog.dismiss();
                            Toast.makeText(OperateVehicleActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        IgnitionDialog.dismiss();
                        Toast.makeText(OperateVehicleActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }

    public void AddTripDetail() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
        editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
        editor.putString(AppsContants.RenewStatus, "1");
        editor.commit();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strFLat = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLAT, "");
        String strFLng = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLNG, "");
        String strEndLat = AppsContants.sharedpreferences.getString(AppsContants.LASTLAT, "");
        String strEndLng = AppsContants.sharedpreferences.getString(AppsContants.LASTLNG, "");

        Log.e("dsfsdfsdfsdf", strFLat);
        Log.e("dsfsdfsdfsdf", strFLng);
        Log.e("dsfsdfsdfsdf", strEndLat);
        Log.e("dsfsdfsdfsdf", strEndLng);
        Log.e("dsfsdfsdfsdf", strPackageID);

        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip_history")
                .addBodyParameter("userID", strUserId)
                .addBodyParameter("packageID", strPackageID)
                .addBodyParameter("device_id", strDeviceId)
                .addBodyParameter("start_latitude", strFLat)
                .addBodyParameter("start_longitude", strFLng)
                .addBodyParameter("end_latitude", strEndLat)
                .addBodyParameter("end_longitude", strEndLng)
                .addBodyParameter("date", formattedDate)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("sdgsdgsdgsdg", response.toString());
                            if (response.getString("message").equals("add Successfully")) {


                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.putString(AppsContants.StationPathSatus, "");
                                editor.commit();

                                getVehicleBookStatus();

                            } else {
                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            commandDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        commandDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


    public void AddTripDetailDropOff() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
        editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
        editor.putString(AppsContants.RenewStatus, "1");
        editor.commit();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strFLat = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLAT, "");
        String strFLng = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLNG, "");
        String strEndLat = AppsContants.sharedpreferences.getString(AppsContants.LASTLAT, "");
        String strEndLng = AppsContants.sharedpreferences.getString(AppsContants.LASTLNG, "");

        Log.e("dsfsdfsdfsdf", strFLat);
        Log.e("dsfsdfsdfsdf", strFLng);
        Log.e("dsfsdfsdfsdf", strEndLat);
        Log.e("dsfsdfsdfsdf", strEndLng);
        Log.e("dsfsdfsdfsdf", strPackageID);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip_history")
                .addBodyParameter("userID", strUserId)
                .addBodyParameter("packageID", strPackageID)
                .addBodyParameter("device_id", strDeviceId)
                .addBodyParameter("start_latitude", strFLat)
                .addBodyParameter("start_longitude", strFLng)
                .addBodyParameter("end_latitude", strEndLat)
                .addBodyParameter("end_longitude", strEndLng)
                .addBodyParameter("date", formattedDate)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("sdgfsadfasfasf", response.toString());
                            if (response.getString("message").equals("add Successfully")) {
                                commandDialog.dismiss();
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.commit();
                                startActivity(new Intent(OperateVehicleActivity.this, DropVehicleStations.class));
                                finish();
                            } else {
                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            commandDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        commandDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


    public void AddTripDetailMaually() {


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
        editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
        editor.putString(AppsContants.RenewStatus, "1");
        editor.commit();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strFLat = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLAT, "");
        String strFLng = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLNG, "");
        String strEndLat = AppsContants.sharedpreferences.getString(AppsContants.LASTLAT, "");
        String strEndLng = AppsContants.sharedpreferences.getString(AppsContants.LASTLNG, "");

        Log.e("dsfsdfsdfsdf", strFLat);
        Log.e("dsfsdfsdfsdf", strFLng);
        Log.e("dsfsdfsdfsdf", strEndLat);
        Log.e("dsfsdfsdfsdf", strEndLng);
        Log.e("dsfsdfsdfsdf", strPackageID);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip_history")
                .addBodyParameter("userID", strUserId)
                .addBodyParameter("packageID", strPackageID)
                .addBodyParameter("device_id", strDeviceId)
                .addBodyParameter("start_latitude", strFLat)
                .addBodyParameter("start_longitude", strFLng)
                .addBodyParameter("end_latitude", strEndLat)
                .addBodyParameter("end_longitude", strEndLng)
                .addBodyParameter("date", formattedDate)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("asfasfsafasfasf", response.toString());
                            if (response.getString("message").equals("add Successfully")) {

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.putString(AppsContants.StationPathSatus, "");
                                editor.commit();


                                getVehicleBookStatusMaually();


                            } else {
                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            commandDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        commandDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }

    public void ShopArrangePopup() {

        final Dialog dialog = new Dialog(OperateVehicleActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.arrange_vehicle_popup);

        Button btnPay = (Button) dialog.findViewById(R.id.btnPay);
        btnPay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                handler.removeCallbacksAndMessages(null);
                handler2.removeCallbacksAndMessages(null);
                handlerKm.removeCallbacksAndMessages(null);
                handlerPopup.removeCallbacksAndMessages(null);
                EndRideMaually();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void deductCoins(String user_id) {
        Log.e("deductCoinsfdfd", "" + user_id);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "deduct_coin")
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("device_id", strDeviceId)
                .addBodyParameter("coin", "50")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("asdfasfasfafa", "" + response);

                        try {
                            if (response.has("result")) {
                                if (response.getString("result").equals("true")) {

                                    AddTripDetailMaually();

                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(OperateVehicleActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            commandDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError", "" + anError);
                        commandDialog.dismiss();
                    }
                });
    }

    private void showPopup() {

        AlertDialog.Builder alert = new AlertDialog.Builder(OperateVehicleActivity.this);
        alert.setMessage("Your package has been expired")
                .setPositiveButton("End Ride", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        ManualEndRidePopup();
                    }
                }).setNegativeButton("Renew package", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                AddTripDetail();
            }
        });

        AlertDialog alert1 = alert.create();
        alert1.show();


        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
        String currentDateandTime = sdf.format(new Date());

        Calendar calendar = null;
        try {
            Date date = sdf.parse(currentDateandTime);
            calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MINUTE, 15); /*15 minutes popup*/
        } catch (ParseException e) {
            Log.e("gsdgsgssgsg", e.getMessage());
        }

        Log.e("werfffasfa", sdf.format(calendar.getTime()));

        String strEndtime = sdf.format(calendar.getTime());


        handlerPopup.postDelayed(runnablePopup = new Runnable() {
            @Override
            public void run() {

                Date c = Calendar.getInstance().getTime();
                SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a", Locale.getDefault());
                String currentTime = df.format(c);
                Log.e("afasfafaffafas", currentTime);
                Log.e("afasfafaffafas", strEndtime);
                Date start = null;
                Date end = null;

                SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss a");
                try {
                    start = simpleDateFormat.parse(currentTime);
                    end = simpleDateFormat.parse(strEndtime);
                } catch (ParseException e) {
                    Log.e("saasfasfas", e.getMessage());
                }

                //1 minute = 60 seconds
                //1 hour = 60 x 60 = 3600
                //1 day = 3600 x 24 = 86400

                //milliseconds
                try {
                    long different = end.getTime() - start.getTime();
                    System.out.println("startDate : " + start);
                    System.out.println("endDate : " + end);
                    System.out.println("different : " + different);

                    long secondsInMilli = 1000;
                    long minutesInMilli = secondsInMilli * 60;
                    long hoursInMilli = minutesInMilli * 60;

                    long elapsedHours = different / hoursInMilli;

                    different = different % hoursInMilli;

                    long elapsedMinutes = different / minutesInMilli;

                    different = different % minutesInMilli;

                    long elapsedSeconds = different / secondsInMilli;

                    System.out.printf("%d hours, %d minutes, %d seconds%n", elapsedHours, elapsedMinutes, elapsedSeconds);
                    Log.e("dccZc", elapsedHours + "");/*agar minus me ata h to hme AM or PM bhi lena pdega date format me*/
                    Log.e("dccZc", elapsedMinutes + "");
                    Log.e("dccZc", elapsedSeconds + "" + " Seconds");

                    // Log.e("dfdfsfsdfsdfdfdsd",elapsedHours, elapsedMinutes, elapsedSeconds)

                    if (elapsedSeconds < 1) {

                        Log.e("asdfasfasfasf", "0 Seconds");
                        handler.removeCallbacksAndMessages(null);
                        handler2.removeCallbacksAndMessages(null);
                        handlerKm.removeCallbacksAndMessages(null);
                        handlerPopup.removeCallbacksAndMessages(null);
                        EndRideMaually();
                        alert1.dismiss();


                    } else {
                        Log.e("asdfasfasfasf", elapsedSeconds + "");

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                handlerPopup.postDelayed(runnablePopup, 1000);
            }
        }, 1000);


    }


    public void getVehicleBookStatusMaually() {


        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "vehical_status")
                .addBodyParameter("vehical_no", strDeviceId)
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("package_id", strPACKAGEID)
                .addBodyParameter("status", "0")   /*1-to boook ride - 0 to finish ride*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("sdsdggsdgsdgsd", response.toString());
                            if (response.getString("message").equals("vehical not booked")) {
                                commandDialog.dismiss();

                                Toast.makeText(OperateVehicleActivity.this, "50 coins has been deducted from wallet.", Toast.LENGTH_SHORT).show();
                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:918109562764"));
                                startActivity(intent);
                                finishAffinity();

                            } else {
                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            commandDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        commandDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }


    public void getVehicleBookStatus() {

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "vehical_status")
                .addBodyParameter("vehical_no", strDeviceId)
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("package_id", strPACKAGEID)
                .addBodyParameter("status", "0")   /*1-to boook ride - 0 to finish ride*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("sdsdggsdgsdgsd", response.toString());
                            if (response.getString("message").equals("vehical not booked")) {
                                commandDialog.dismiss();
                                startActivity(new Intent(OperateVehicleActivity.this, NavigationActivity.class));
                                finishAffinity();

                            } else {
                                commandDialog.dismiss();
                                Toast.makeText(OperateVehicleActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            commandDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        commandDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }

}
