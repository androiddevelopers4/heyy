package com.mybike.service.draggable;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AppComponentFactory;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.Activities.DrawPathVehicle;
import com.mybike.service.Activities.PayNowActivity;
import com.mybike.service.Activities.ShowVehicleActivity;
import com.mybike.service.Modals.DistanceModal;
import com.mybike.service.R;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LoginResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SelectLocationActivity extends AppCompatActivity implements MapWrapperLayout.OnDragListener, OnMapReadyCallback {

    private GoogleMap googleMap;
    private ImageView marker;
    private ImageView imgCurrentLocation;
    private int centerX = -1;
    private int centerY = -1;
    String lat = "", lng = "";
    TextView txtAddress;
    TextView txtLocalAddress;
    Button btnConfirmLocation;
    GPSTracker gpsTracker;
    double lat1, lng1;
    LatLng s_latLng;
    String address = "";
    CheckBox check_box1;
    String strAccurateAdd = "";
    String status = "";
    Marker thisUserMarker;
    Bitmap driverMarker;
    int width = 100;
    int height = 100;
    String checkbox_status = "false";

    ArrayList<DistanceModal>distanceList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_changed_adddress);
        GetParentAccountToken();





        txtLocalAddress = findViewById(R.id.txtLocalAddress);
        txtAddress = findViewById(R.id.txtAddress);
        btnConfirmLocation = findViewById(R.id.btnConfirmLocation);
        check_box1 = findViewById(R.id.check_box1);
        ImageView backImage = findViewById(R.id.backImage);
        TextView tv_arrange = findViewById(R.id.tv_arrange);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        check_box1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_box1.isChecked()) {
                    tv_arrange.setText("If you arrange a vehicle you have to pay rs.50 extra.");
                    checkbox_status = "true";
                } else {
                    tv_arrange.setText("Arrange A Vehicle");
                    checkbox_status = "false";
                }
            }
        });


        marker = findViewById(R.id.marker);
        imgCurrentLocation = findViewById(R.id.imgCurrentLocation);
        imgCurrentLocation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AnimateCamera();
            }
        });

        gpsTracker = new GPSTracker(SelectLocationActivity.this);
        lat1 = gpsTracker.getLatitude();
        lng1 = gpsTracker.getLongitude();

        Log.e("uyfuyhj", lat1 + "");
        Log.e("uyfuyhj", lng1 + "");

    }


    @Override
    protected void onResume() {
        super.onResume();
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            initilizeMap();

        } else {

            gpsTracker.showSettingsAlert();
        }

    }

    private void initilizeMap() {
        if (googleMap == null) {

            CustomMapFragment supportMapFragment = (CustomMapFragment)
                    getFragmentManager().findFragmentById(R.id.map);
            supportMapFragment.setOnDragListener(SelectLocationActivity.this);
            supportMapFragment.getMapAsync(this);

            if (googleMap == null) {

            }
        }
    }

    @Override
    public void onMapReady(GoogleMap map) {

        googleMap = map;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                this, R.raw.maps_style);
        googleMap.setMapStyle(style);
        googleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {
            s_latLng = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
            updateLocation(s_latLng);
            AnimateCamera();

        } else {

            gpsTracker.showSettingsAlert();
        }


        googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Geocoder geocoder = new Geocoder(SelectLocationActivity.this, Locale.getDefault());
                //    addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1); //1 num of possible location returned


                String a = marker.getId();
                int g;
                for (g = 0; g < a.length(); g++) {
                    char c = a.charAt(g);
                    if ('0' <= c && c <= '9')
                        break;
                }
                String alphaPart = a.substring(0, g);
                String numberPart = a.substring(g);
                Log.e("numberPart", "" + numberPart);
                if (Integer.parseInt(numberPart) == 0) {
                    numberPart = "1";

                } else {

                }
                return false;
            }
        });
    }

    @Override
    public void onDrag(MotionEvent motionEvent) {


        Log.e("uyttttuy", "DRAG");
        if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
            Projection projection = (googleMap != null && googleMap
                    .getProjection() != null) ? googleMap.getProjection()
                    : null;
            if (projection != null) {
                LatLng centerLatLng = projection.fromScreenLocation(new Point(
                        centerX, centerY));


                GPSTracker gpsTracker = new GPSTracker(this);


                if (gpsTracker.canGetLocation()) {
                    //
                    int secondsDelayed = 1;
                    new Handler().postDelayed(new Runnable() {
                        public void run() {

                            updateLocation(centerLatLng);

                        }
                    }, secondsDelayed * 3);


                } else {

                    gpsTracker.showSettingsAlert();
                }

            }
        }
    }

//F:\flutter\flutter_windows_1.17.3-stable\flutter\bin
    private void updateLocation(final LatLng centerLatLng) {
        strAccurateAdd = "";
        if (centerLatLng != null) {

            try {
                Geocoder geocoder;
                List<Address> addresses;
                geocoder = new Geocoder(this, Locale.getDefault());

                addresses = geocoder.getFromLocation(centerLatLng.latitude, centerLatLng.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                if (addresses.size() != 0) {

                    address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                    final String city = addresses.get(0).getLocality();
                    String state = addresses.get(0).getAdminArea();
                    String knownName = addresses.get(0).getFeatureName();
                    final String pincode = addresses.get(0).getPostalCode();

                    Log.e("gigjk", addresses.get(0).getPostalCode() + "");

                    Log.e("gigjk", "add" + address);
                    Log.e("gigjk", "city" + city);

                    String strAdd[] = address.split(",");

                    for (int i = 0; i < strAdd.length; i++) {

                        Log.e("fgdgdfgdfgf", i + " " + strAdd[i]);
                        if (i != 0) {

                            if (strAccurateAdd.equals("")) {

                                strAccurateAdd = strAdd[i];
                            } else {
                                strAccurateAdd = strAccurateAdd + "," + strAdd[i];
                            }
                        }

                    }


                    // txtLocalAddress.setText(knownName);
                    txtLocalAddress.setText(city);
                    txtAddress.setText(strAccurateAdd);
                    lat = String.valueOf(centerLatLng.latitude);
                    lng = String.valueOf(centerLatLng.longitude);


                    btnConfirmLocation.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {


                            if (btnConfirmLocation.getText().toString().equals("Confirm Location")) {

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.ARRANGE_VEHICLE_LAT, String.valueOf(lat));
                                editor.putString(AppsContants.ARRANGE_VEHICLE_LNG, String.valueOf(lng));
                                editor.putString(AppsContants.ARRANGE_VEHICLE_CHECKBOX_STATUS, checkbox_status);
                                editor.putString(AppsContants.PaymentUsedStatus, "true");
                                editor.commit();
                                Log.e("sddsdsfddv", "Start" + checkbox_status);

                               // startActivity(new Intent(SelectLocationActivity.this, PayNowActivity.class));
                                startActivity(new Intent(SelectLocationActivity.this, DrawPathVehicle.class));
                                finish();
                            } else {

                                Toast.makeText(SelectLocationActivity.this, "No vehicle found", Toast.LENGTH_SHORT).show();
                            }


                        }
                    });

                }


            } catch (IOException e) {
                e.printStackTrace();
            }

        }
    }


    public void AnimateCamera() {
        GPSTracker gpsTracker = new GPSTracker(this);

        if (gpsTracker.canGetLocation()) {

            try {
                LatLng latLngs = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
                googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLngs)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            gpsTracker.showSettingsAlert();
        }

    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }


    public void showVehicles(String s) {

        AndroidNetworking.get(HTTPCALL.AllAssets)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", status)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("response", "" + response);
                        try {
                            distanceList=new ArrayList<>();
                            String latitude = "";
                            String longitude = "";
                            String BATTERY_PERC = "";
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);
                                if (s.equals("60")) {

                                    // Double VOLTAGE=49.7; /* 1 mode= 80 km**/
                                    //Double VOLTAGE=49.4; /* 2 mode= 70 km**/
                                    Double VOLTAGE = 49.4; /* 3 mode= 60 km**/

                                    if (!object.getString("devicePowerVoltage").isEmpty() && !object.getString("devicePowerVoltage").equals("null")) {

                                        Double API_VOLTAGE = Double.parseDouble(object.getString("devicePowerVoltage"));
                                        if (API_VOLTAGE >= VOLTAGE) {/*Showing Only for 60km and above */
                                            latitude = object.getString("latitude");
                                            longitude = object.getString("longitude");
                                            BATTERY_PERC = object.getString("devicePowerVoltage");


                                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                            Bitmap b = bitmapdraw.getBitmap();

                                            if (!latitude.isEmpty() && !longitude.isEmpty()) {
                                                btnConfirmLocation.setText("Confirm Location");


                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                editor.putString(AppsContants.PickPointVehicleLat, latitude);
                                                editor.putString(AppsContants.PickPointVehicleLng, longitude);
                                                editor.commit();
                                                try {
                                                    driverMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                                    Double aDouble_user_latitude = Double.parseDouble(latitude);
                                                    Double aDouble_user_longitude = Double.parseDouble(longitude);
                                                    LatLng Bhopal = new LatLng(aDouble_user_latitude, aDouble_user_longitude);
                                                    thisUserMarker = googleMap.addMarker(new MarkerOptions().position(Bhopal));
                                                    thisUserMarker.setTag("id");
                                                    thisUserMarker.setTitle(BATTERY_PERC);
                                                    thisUserMarker.setIcon(BitmapDescriptorFactory.fromBitmap(driverMarker));
                                                }
                                                catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {

                                                btnConfirmLocation.setText("No vehicle found");

                                                Toast.makeText(SelectLocationActivity.this, "No Vehicle Found!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                } else if (s.equals("30")) {/*Showing Only for 30km and above */

                                    //Double VOLTAGE=48.7;/* 1 mode= 80 km**/
                                    // Double VOLTAGE=49.1;/* 2 mode= 70 km**/
                                    Double VOLTAGE = 49.4;/* 3 mode= 60 km**/

                                    if (!object.getString("devicePowerVoltage").isEmpty() && !object.getString("devicePowerVoltage").equals("null")) {
                                        Double API_VOLTAGE = Double.parseDouble(object.getString("devicePowerVoltage"));
                                        if (API_VOLTAGE >= VOLTAGE) {
                                            latitude = object.getString("latitude");
                                            longitude = object.getString("longitude");
                                            BATTERY_PERC = object.getString("devicePowerVoltage");


                                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                            Bitmap b = bitmapdraw.getBitmap();

                                            if (!latitude.isEmpty() && !longitude.isEmpty()) {
                                                btnConfirmLocation.setText("Confirm Location");


                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                editor.putString(AppsContants.PickPointVehicleLat, latitude);
                                                editor.putString(AppsContants.PickPointVehicleLng, longitude);
                                                editor.commit();
                                                try {
                                                    driverMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                                    Double aDouble_user_latitude = Double.parseDouble(latitude);
                                                    Double aDouble_user_longitude = Double.parseDouble(longitude);
                                                    LatLng Bhopal = new LatLng(aDouble_user_latitude, aDouble_user_longitude);
                                                    thisUserMarker = googleMap.addMarker(new MarkerOptions().position(Bhopal));
                                                    thisUserMarker.setTag("id");
                                                    thisUserMarker.setTitle(BATTERY_PERC);
                                                    thisUserMarker.setIcon(BitmapDescriptorFactory.fromBitmap(driverMarker));
                                                } catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {

                                                btnConfirmLocation.setText("No vehicle found");

                                                Toast.makeText(SelectLocationActivity.this, "No Vehicle Found!", Toast.LENGTH_SHORT).show();
                                            }
                                        }

                                    }

                                } else if (s.equals("10")) {/*Showing Only for 30km and above */

                                    //  Double VOLTAGE=48.5;/* 1 mode= 80 km**/
                                    // Double VOLTAGE=48.7;/* 2 mode= 70 km**/
                                    Double VOLTAGE = 49.1;/* 3 mode= 60 km**/

                                    if (!object.getString("devicePowerVoltage").isEmpty() && !object.getString("devicePowerVoltage").equals("null")) {
                                        Double API_VOLTAGE = Double.parseDouble(object.getString("devicePowerVoltage"));
                                        if (API_VOLTAGE >= VOLTAGE) {
                                            latitude = object.getString("latitude");
                                            longitude = object.getString("longitude");
                                            BATTERY_PERC = object.getString("devicePowerVoltage");


                                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                            Bitmap b = bitmapdraw.getBitmap();

                                            if (!latitude.isEmpty() && !longitude.isEmpty()) {
                                                btnConfirmLocation.setText("Confirm Location");


                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                editor.putString(AppsContants.PickPointVehicleLat, latitude);
                                                editor.putString(AppsContants.PickPointVehicleLng, longitude);
                                                editor.commit();
                                                try {
                                                    driverMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                                    Double aDouble_user_latitude = Double.parseDouble(latitude);
                                                    Double aDouble_user_longitude = Double.parseDouble(longitude);

                                                    Location startPoint = new Location("locationA");
                                                    startPoint.setLatitude(aDouble_user_latitude);
                                                    startPoint.setLongitude(aDouble_user_longitude);

                                                    Location endPoint = new Location("locationA");
                                                    endPoint.setLatitude(23.2309732);
                                                    endPoint.setLongitude(77.4351752);

                                                    double distance = startPoint.distanceTo(endPoint);

                                                    DistanceModal distanceModal= new DistanceModal();
                                                    distanceModal.setMin(distance);
                                                    distanceModal.setLat(latitude);
                                                    distanceModal.setLng(longitude);
                                                    distanceList.add(distanceModal);


                                                    LatLng Bhopal = new LatLng(aDouble_user_latitude, aDouble_user_longitude);
                                                   // latLngArrayList.add(Bhopal);
                                                    thisUserMarker = googleMap.addMarker(new MarkerOptions().position(Bhopal));
                                                    thisUserMarker.setTag("id");
                                                    thisUserMarker.setTitle(BATTERY_PERC);
                                                    thisUserMarker.setIcon(BitmapDescriptorFactory.fromBitmap(driverMarker));
                                                } catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {

                                                btnConfirmLocation.setText("No vehicle found");

                                                Toast.makeText(SelectLocationActivity.this, "No Vehicle Found!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }

                                } else if (s.equals("2")) {/*Showing Only for 30km and above */
                                    Log.e("sdgdssdgs","2 km");
                                    // Double VOLTAGE=48.0;/* 1 mode= 80 km**/
                                    // Double VOLTAGE=48.0;/* 2 mode= 70 km**/
                                    Double VOLTAGE = 48.0;/* 3 mode= 60 km**/
                                    if (!object.getString("devicePowerVoltage").isEmpty() && !object.getString("devicePowerVoltage").equals("null")) {
                                        Double API_VOLTAGE = Double.parseDouble(object.getString("devicePowerVoltage"));
                                        if (API_VOLTAGE >= VOLTAGE) {
                                            latitude = object.getString("latitude");
                                            longitude = object.getString("longitude");
                                            BATTERY_PERC = object.getString("devicePowerVoltage");


                                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                            Bitmap b = bitmapdraw.getBitmap();

                                            if (!latitude.isEmpty() && !longitude.isEmpty()) {
                                                btnConfirmLocation.setText("Confirm Location");

                                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                                editor.putString(AppsContants.PickPointVehicleLat, latitude);
                                                editor.putString(AppsContants.PickPointVehicleLng, longitude);
                                                editor.commit();

                                                try {
                                                    driverMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                                    Double aDouble_user_latitude = Double.parseDouble(latitude);
                                                    Double aDouble_user_longitude = Double.parseDouble(longitude);
                                                    LatLng Bhopal = new LatLng(aDouble_user_latitude, aDouble_user_longitude);
                                                    thisUserMarker = googleMap.addMarker(new MarkerOptions().position(Bhopal));
                                                    thisUserMarker.setTag("id");
                                                    thisUserMarker.setTitle(BATTERY_PERC);
                                                    thisUserMarker.setIcon(BitmapDescriptorFactory.fromBitmap(driverMarker));
                                                } catch (NumberFormatException e) {
                                                    e.printStackTrace();
                                                }
                                            } else {

                                                btnConfirmLocation.setText("No vehicle found");

                                                Toast.makeText(SelectLocationActivity.this, "No Vehicle Found!", Toast.LENGTH_SHORT).show();
                                            }
                                        }
                                    }
                                } else {

                                    // Toast.makeText(ShowVehicleActivity.this, "Something Went Worng!", Toast.LENGTH_SHORT).show();
                                }

                            }


                         // getMax(distanceList);
                          //  double min=getMin(distanceList);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError", "" + anError.getMessage());
                    }
                });
    }


    // Method for getting the maximum value
    public void getMax(ArrayList<DistanceModal> distanceList){
        double maxValue = distanceList.get(0).getMin();
        String strLat="";
        String strLng="";
        for(int i=0;i < distanceList.size();i++){
            if(distanceList.get(i).getMin()> maxValue){
                maxValue = distanceList.get(i).getMin();
                strLat=distanceList.get(i).getLat();
                strLng=distanceList.get(i).getLng();
            }
        }
        Log.e("dfgfdgdgdfgdf",maxValue+"");
        Log.e("dfgfdgdgdfgdf",strLat);
        Log.e("dfgfdgdgdfgdf",strLng);

    }


    // Method for getting the maximum value
    public static double getMin(ArrayList<Double> distanceList){
        double minValue = distanceList.get(0);
        for(int i=1;i < distanceList.size();i++){
            if(distanceList.get(i)< minValue){
                minValue = distanceList.get(i);
            }
        }
        return minValue;
    }



    public void GetParentAccountToken() {
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String KM = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, "");
        String price = AppsContants.sharedpreferences.getString(AppsContants.Price, "");
        Log.e("USER_SELECTED_KM", "" + KM);
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<LoginResponse> loginResponseCall = service.userLogin("suryanbajpai33@gmail.com", "Manirock33");

      /*  progressDialog = new ProgressDialog(OperateVehicleActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);*/

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                //Log.e("ddsgdfgdfg",response.toString());
                try {
                    status = response.body().getToken();
                    Log.e("ddsgdfgdfg", status);
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.Maintoken, status);
                    editor.commit();
                    showVehicles(KM);

                } catch (Exception e) {

                    Toast.makeText(SelectLocationActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ddsgdfgdfg", t.getMessage());
                Toast.makeText(SelectLocationActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }

}
