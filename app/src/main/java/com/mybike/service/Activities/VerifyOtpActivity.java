package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.gson.internal.bind.ObjectTypeAdapter;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONException;
import org.json.JSONObject;

public class VerifyOtpActivity extends AppCompatActivity {
    EditText et1, et2, et3, et4;
    Button btnVerify;
    String strMobile="";
    String strOTP="";
    String strEmail="";
    String strPassword="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verify_otp);

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strMobile = AppsContants.sharedpreferences.getString(AppsContants.mobile, "");
        strOTP = AppsContants.sharedpreferences.getString(AppsContants.OTP, "");
        strEmail = AppsContants.sharedpreferences.getString(AppsContants.email, "");
        strPassword = AppsContants.sharedpreferences.getString(AppsContants.password, "");
        Log.e("sdfsdfsdfsf", strMobile);

        TextView edit_mobile_number = findViewById(R.id.edit_mobile_number);
        edit_mobile_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        TextView entered_mobile_number = findViewById(R.id.entered_mobile_number);
        entered_mobile_number.setText(strMobile);
        ImageView ic_back = findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btnVerify = findViewById(R.id.btnVerify);
        et1 = findViewById(R.id.et1);
        et2 = findViewById(R.id.et2);
        et3 = findViewById(R.id.et3);
        et4 = findViewById(R.id.et4);

        et1.addTextChangedListener(new GenericTextWatcher(et1));
        et2.addTextChangedListener(new GenericTextWatcher(et2));
        et3.addTextChangedListener(new GenericTextWatcher(et3));
        et4.addTextChangedListener(new GenericTextWatcher(et4));


        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String Otpnumber = et1.getText().toString() + et2.getText().toString() + et3.getText().toString() + et4.getText().toString();

                if(Otpnumber.equals(strOTP)){
                    Login(strEmail, strPassword);


                }
                else {
                    Toast.makeText(VerifyOtpActivity.this, "Please enter correct otp", Toast.LENGTH_SHORT).show();
                }


            //    verifyOTP(Otpnumber);
                Log.e("sdffjsdbhfksdfs", Otpnumber);
            }
        });


    }

    public class GenericTextWatcher implements TextWatcher {
        private View view;

        private GenericTextWatcher(View view) {
            this.view = view;
        }

        @Override
        public void afterTextChanged(Editable editable) {
            // TODO Auto-generated method stub
            String text = editable.toString();
            switch (view.getId()) {

                case R.id.et1:
                    if (text.length() == 1)
                        et2.requestFocus();
                    break;
                case R.id.et2:
                    if (text.length() == 1)
                        et3.requestFocus();
                    else if (text.length() == 0)
                        et1.requestFocus();
                    break;
                case R.id.et3:
                    if (text.length() == 1)
                        et4.requestFocus();
                    else if (text.length() == 0)
                        et2.requestFocus();
                    break;
                case R.id.et4:
                    if (text.length() == 0)
                        et3.requestFocus();
                    else if (text.length() == 1)
                        if (et1.getText().toString().isEmpty() || et2.getText().toString().isEmpty() || et3.getText().toString().isEmpty() || et4.getText().toString().isEmpty()) {
                            Toast.makeText(VerifyOtpActivity.this, "Please enter all missing digits", Toast.LENGTH_SHORT).show();
                        } else {

                            if (!validateForm()) {
                                return;
                            }


                        }


                    break;
            }
        }

        @Override
        public void beforeTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }

        @Override
        public void onTextChanged(CharSequence arg0, int arg1, int arg2, int arg3) {
            // TODO Auto-generated method stub
        }
    }

    private boolean validateForm() {
        boolean result = true;
        if (TextUtils.isEmpty(et1.getText().toString())) {
            et1.setError("Required");
            result = false;
        } else {
            et1.setError(null);
        }

        if (TextUtils.isEmpty(et2.getText().toString())) {
            et2.setError("Required");
            result = false;
        } else {
            et2.setError(null);

        }
        if (TextUtils.isEmpty(et3.getText().toString())) {
            et3.setError("Required");
            result = false;
        } else {
            et3.setError(null);
        }
        if (TextUtils.isEmpty(et4.getText().toString())) {
            et4.setError("Required");
            result = false;
        } else {
            et4.setError(null);

        }

        return result;
    }




    public void Login(String strEmail, String strPassword) {

       ProgressDialog  progressDialog = new ProgressDialog(VerifyOtpActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "login")
                .addBodyParameter("email", strEmail)
                .addBodyParameter("password", strPassword)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                                JSONObject jsonObject = new JSONObject(response.getString("data"));

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.UserId, jsonObject.getString("userID"));
                                editor.putString(AppsContants.fname, jsonObject.getString("fname"));
                                editor.putString(AppsContants.lname, jsonObject.getString("lname"));
                                editor.putString(AppsContants.email, jsonObject.getString("email"));
                                editor.putString(AppsContants.mobile, jsonObject.getString("mobile"));
                                editor.putString(AppsContants.Path, jsonObject.getString("path"));
                                editor.putString(AppsContants.ProfileImage, jsonObject.getString("profile_image"));
                                editor.putString(AppsContants.AdharNo, jsonObject.getString("adhar_no"));
                                editor.commit();
                                progressDialog.dismiss();
                                startActivity(new Intent(VerifyOtpActivity.this, NavigationActivity.class));
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOtpActivity.this, "Login details invalid", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }

    public void verifyOTP(String otpnumber) {

        ProgressDialog progressDialog = new ProgressDialog(VerifyOtpActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        Log.e("sdfsdfsdf",otpnumber);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "verify_otp")
                .addBodyParameter("mobile", strMobile)
                .addBodyParameter("otp", otpnumber)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                                progressDialog.dismiss();

                                JSONObject jsonObject = new JSONObject(response.getString("data"));
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.UserId, jsonObject.getString("userID"));
                                editor.putString(AppsContants.fname, jsonObject.getString("fname"));
                                editor.putString(AppsContants.lname, jsonObject.getString("lname"));
                                editor.putString(AppsContants.email, jsonObject.getString("email"));
                                editor.putString(AppsContants.mobile, jsonObject.getString("mobile"));
                                editor.putString(AppsContants.Path, jsonObject.getString("path"));
                                editor.putString(AppsContants.ProfileImage, jsonObject.getString("profile_image"));
                                editor.putString(AppsContants.AdharNo, jsonObject.getString("adhar_no"));
                                editor.commit();
                                startActivity(new Intent(VerifyOtpActivity.this, NavigationActivity.class));
                                finishAffinity();


                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(VerifyOtpActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }
}