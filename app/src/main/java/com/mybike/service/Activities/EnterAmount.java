package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;

public class EnterAmount extends AppCompatActivity {

    EditText editPrice;
    Button rl_next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enter_amount);

        ImageView imgBack=findViewById(R.id.imgBack);

        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        rl_next=findViewById(R.id.rl_next);
        editPrice=findViewById(R.id.editPrice);


        editPrice.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                String strData= String.valueOf(charSequence);
                if(strData.length()!=0){

                    rl_next.setBackgroundColor(Color.parseColor("#079EE3"));

                }
                else {
                    rl_next.setBackgroundColor(Color.parseColor("#CDD2D5"));
                }

            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });




        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strPrice=editPrice.getText().toString();
                if(!strPrice.equals("")){
                    int price= Integer.parseInt(strPrice);
                    if(price<260){
                        Toast.makeText(EnterAmount.this, "Minimum amount should be 260 INR", Toast.LENGTH_SHORT).show();
                    }
                    else {

                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.Price, strPrice);
                        editor.commit();
                        startActivity(new Intent(EnterAmount.this,PaytmActivity.class));
                    }
                }
                else {
                    Toast.makeText(EnterAmount.this, "Enter Amount", Toast.LENGTH_SHORT).show();
                }

            }
        });

    }
}