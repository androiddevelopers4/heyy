package com.mybike.service.Utils;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ApiService {

    @FormUrlEncoded
    @POST("login")
    Call<LoginResponse> userLogin(@Field("username") String username, @Field("password")String password);

    @FormUrlEncoded
    @POST("api/things/accounts/create")
    Call<SignupResponse> userRegister(@Field("accountname") String accountname, @Field("parentid") String parentid, @Field("firstname") String firstname, @Field("lastname") String lastname
    ,@Field("loginPref") String loginPref, @Field("phone") String phone,@Field("password") String password, @Field("location") String location,@Field("timezone") String timezone);


    @POST("api/things/accounts/create")
    Call<SignupResponse> userRegister(@Body SignupReq signupResponse);



}