package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.R;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LoginResponse;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RoughActivity extends AppCompatActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener{


    TextView txtStart;
    TextView txtBreak;
    TextView txtEnd;
    LinearLayout linearBottom;
    String strDeviceId = "";
    String strAccoundId = "";
    String strParentToken = "";
    String strStatus = "";
    ProgressDialog progressDialog;
    String strAssetUID = "";
    String strFName = "";
    TextView txtShowTimeLeft;
    TextView txtShowKMLeft;
    String strUserKM;
    String strIgnitionType;
    int KmHours = 0;

    Handler handler;
    Runnable runnable;

    Handler handler2;
    Runnable runnable2;
    GoogleMap mGoogleMap;
    GoogleApiClient mGoogleApiClient;
    LocationRequest mLocationRequest;
    String strUserId="";
    String strRideId="";
    String strTripStatus="";
    String userUsedKm="";
    String strLastKm="";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rough);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        strAccoundId = AppsContants.sharedpreferences.getString(AppsContants.AccoundId, "");
        strFName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        strUserKM = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, "");
        strIgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
        strRideId = AppsContants.sharedpreferences.getString(AppsContants.RideId, "");
        strLastKm = AppsContants.sharedpreferences.getString(AppsContants.LastKm, "");
        GetMyTripId();
        Log.e("dfgdfgdfgdf", strAccoundId);
        Log.e("dfgdfgdfgdf", strUserKM);
        Log.e("dfgdfgdfgdf", strDeviceId);


        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map.getMapAsync(this);

        txtShowKMLeft = findViewById(R.id.txtShowKMLeft);
        txtShowTimeLeft = findViewById(R.id.txtShowTimeLeft);

        txtStart = findViewById(R.id.txtStart);
        txtBreak = findViewById(R.id.txtBreak);
        txtEnd = findViewById(R.id.txtEnd);
        linearBottom = findViewById(R.id.linearBottom);


        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        if(strIgnitionType.equals("REMOTE_IGNITION_ON")){

            txtStart.setVisibility(View.GONE);
            linearBottom.setVisibility(View.VISIBLE);
        }
        else {
            linearBottom.setVisibility(View.GONE);
            txtStart.setVisibility(View.VISIBLE);
        }

        txtStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");

                if (FirstIgnitionStatus.equals("")) {

                    if (strUserKM.equals("60")) {

                        KmHours = 10;
                    } else if (strUserKM.equals("30")) {

                        KmHours = 5;
                    } else if (strUserKM.equals("20")) {

                        KmHours = 3;
                    } else {

                        KmHours = 1;
                    }


                    SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
                    String currentDateandTime = sdf.format(new Date());

                    Calendar calendar = null;
                    try {
                        Date date = sdf.parse(currentDateandTime);
                        calendar = Calendar.getInstance();
                        calendar.setTime(date);
                        calendar.add(Calendar.HOUR, KmHours);
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }

                    Log.e("werfffasfa", sdf.format(calendar.getTime()));

                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.StartIgnitiontime, sdf.format(calendar.getTime()));
                    editor.commit();

                }

                strStatus = "REMOTE_IGNITION_ON";
                StartVehicle(strStatus);

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_ON");
                editor.commit();

                handler = new Handler();
                handler.postDelayed(runnable = new Runnable() {
                    @Override
                    public void run() {
                        Log.e("payment", "Running");
                        printDifference();
                        handler.postDelayed(runnable, 1000);
                    }
                }, 1000);
            }
        });

        txtBreak.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(RoughActivity.this);
                alert.setMessage("Are you sure you want to take a break?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {
                                strStatus = "REMOTE_IGNITION_OFF";
                                TakeBreak(strStatus);
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.IgnitionType, "REMOTE_IGNITION_OFF");
                                editor.commit();
                            }
                        }).setNegativeButton("Cancel", null);

                AlertDialog alert1 = alert.create();
                alert1.show();


            }
        });


        txtEnd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AlertDialog.Builder alert = new AlertDialog.Builder(RoughActivity.this);
                alert.setMessage("Are you sure you want to end this ride?")
                        .setPositiveButton("Ok", new DialogInterface.OnClickListener() {

                            public void onClick(DialogInterface dialog, int which) {


                                AddLastKiloMeter();


                            }
                        }).setNegativeButton("Cancel", null);

                AlertDialog alert1 = alert.create();
                alert1.show();



            }
        });


        LinearLayout relBikeDetail=findViewById(R.id.relBikeDetail);
        relBikeDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(RoughActivity.this,VehicleDetailActivity.class));
            }
        });
        GetParentAccountToken();


    }


    @Override
    protected void onResume() {
        super.onResume();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");

        if (FirstIgnitionStatus.equals("Yes")) {

            handler = new Handler();
            handler.postDelayed(runnable = new Runnable() {
                @Override
                public void run() {
                    Log.e("payment", "Running");
                    printDifference();
                    handler.postDelayed(runnable, 1000);
                }
            }, 1000);


            handler2 = new Handler();
            handler2.postDelayed(runnable2 = new Runnable() {
                @Override
                public void run() {
                    Log.e("payment", "Trip is Running");
                    GetTripDetails();
                    handler2.postDelayed(runnable2, 5000);
                }
            }, 5000);


        }


        if (mGoogleApiClient != null &&
                ContextCompat.checkSelfPermission(RoughActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }

    }


    @Override
    public void onPause() {
        super.onPause();

        //stop location updates when Activity is no longer active
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                RoughActivity.this, R.raw.maps_style);
        googleMap.setMapStyle(style);

        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(RoughActivity.this,
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mGoogleMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mGoogleMap.setMyLocationEnabled(true);
        }



    }


    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(RoughActivity.this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setSmallestDisplacement(0.1f);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(RoughActivity.this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
    }



    @SuppressLint("MissingPermission")
    @Override
    public void onLocationChanged(Location location) {


    }

    public void GetParentAccountToken() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<LoginResponse> loginResponseCall = service.userLogin("suryanbajpai33@gmail.com", "Manirock33");

        progressDialog = new ProgressDialog(RoughActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                //Log.e("ddsgdfgdfg",response.toString());
                try {
                    String status = response.body().getToken();
                    Log.e("ddsgdfgdfg", status);
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.Maintoken, status);
                    editor.commit();
                    // txtAccountId.setText(strFName);
                    // txtDeviceId.setText("Test Vehicle_1");
                    progressDialog.dismiss();
                    GetAssetUID();

                } catch (Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(RoughActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }


            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ddsgdfgdfg", t.getMessage());
                Toast.makeText(RoughActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }

    public void GetAssetUID() {
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        AndroidNetworking.get("https://api-aertrak-india.aeris.com/api/fleet/assets")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {
                            Log.e("wetwertwerwerwer", response.toString());

                            for (int i = 0; i < response.length(); i++) {
                                JSONObject jsonObject = response.getJSONObject(i);
                                if (strDeviceId.equals(jsonObject.getString("deviceId"))) {

                                    strAssetUID = jsonObject.getString("assetUid");
                                }
                            }
                            GetTripDetails();
                            Log.e("dsfgsdfsdfsd", strAssetUID);

                        } catch (Exception ex) {

                            Log.e("wetwertwerwerwer", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("wetwertwerwerwer", "Error" + anError.getMessage());
                    }
                });
    }

    public void TakeBreak(String strStatus) {
        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", strStatus);
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog commandDialog = new ProgressDialog(RoughActivity.this);
        commandDialog.setMessage("Commanding..");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("response")) {
                                if (response.getString("response").equals("Command has been accepted successfully")) {

                                    txtStart.setVisibility(View.VISIBLE);
                                    linearBottom.setVisibility(View.GONE);
                                    commandDialog.dismiss();
                                    Toast.makeText(RoughActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(RoughActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(RoughActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(RoughActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(RoughActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void printDifference() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strWhenStopVehicleTime = AppsContants.sharedpreferences.getString(AppsContants.StartIgnitiontime, "");

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
        String currentTime = df.format(c);
        Log.e("eryuweyuui", currentTime);
        Log.e("eryuweyuui", strWhenStopVehicleTime);
        Date start = null;
        Date end = null;

        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        try {
            start = simpleDateFormat.parse(currentTime);
            end = simpleDateFormat.parse(strWhenStopVehicleTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        //milliseconds
        try {
            long different = end.getTime() - start.getTime();
            System.out.println("startDate : " + start);
            System.out.println("endDate : " + end);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;


            long elapsedHours = different / hoursInMilli;
            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;
            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            System.out.printf("%d hours, %d minutes, %d seconds%n", elapsedHours, elapsedMinutes, elapsedSeconds);
            Log.e("wrewerwerewrr", elapsedHours + "");
            Log.e("wrewerwerewrr", elapsedMinutes + "");
            txtShowTimeLeft.setText(elapsedHours + " " + "h" + " " + elapsedMinutes + " " + "m" + " " + elapsedSeconds + " " + "s");
            // Log.e("dfdfsfsdfsdfdfdsd",elapsedHours, elapsedMinutes, elapsedSeconds)

            if(elapsedHours<1){
                Log.e("dfggdgdfgd","Hour is less than 0");

                if(elapsedMinutes<1){

                    Log.e("dfggdgdfgd","Minute is less than 0");

                    if(elapsedSeconds<1){
                        txtShowTimeLeft.setText("0" + " " + "h" + " " + "0" + " " + "m" + " " + "0" + " " + "s");
                        Log.e("dfggdgdfgd","Seconds is less than 0");

                        AddLastKiloMeter();
                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                        editor.putString(AppsContants.IgnitionType, "");
                        editor.putString(AppsContants.StartIgnitiontime, "");
                        editor.putString(AppsContants.FirstIgnitionStatus, "");
                        editor.putString(AppsContants.DeviceId, "");
                        editor.putString(AppsContants.AccoundId, "");
                        editor.putString(AppsContants.Maintoken, "");
                        editor.commit();
                        Toast.makeText(RoughActivity.this, "Your package has been expired please choose new package", Toast.LENGTH_SHORT).show();
                        startActivity(new Intent(RoughActivity.this,PricingListActivity.class));
                        finish();
                    }
                    else {

                        txtShowTimeLeft.setText("0" + " " + "h" + " " + "0" + " " + "m" + " " + elapsedSeconds + " " + "s");
                    }
                }
                else {

                    txtShowTimeLeft.setText("0" + " " + "h" + " " + elapsedMinutes + " " + "m" + " " + elapsedSeconds + " " + "s");
                }
            }
            else {

                txtShowTimeLeft.setText(elapsedHours + " " + "h" + " " + elapsedMinutes + " " + "m" + " " + elapsedSeconds + " " + "s");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void GetTripDetails() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date();
        String strtDate = dateFormat.format(date);
        Log.e("dfgdgdfgdf", strtDate);

        AndroidNetworking.get("https://api-aertrak-india.aeris.com/api/things/data/assets/trips?startDate=" + strtDate + "&endDate=" + strtDate + "&filterType=totalTime&accountId=" + strAccoundId + "&assetUid=" + strAssetUID)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            BigDecimal val1BD;
                            BigDecimal val2BD;
                            BigDecimal result ;
                            Log.e("asfdafafsaf", response.length() + "");
                            Log.e("asfdafafsaf", response.toString());
                            BigDecimal ActualKmOfVehicle = BigDecimal.ZERO;

                            String strCLat = "";
                            String strCLng="";
                            for (int i = 0; i < response.length(); i++) {

                                JSONObject jsonObject = response.getJSONObject(i);

                                String TotalKm = jsonObject.getString("totalKm");
                                strCLat = jsonObject.getString("endLat");
                                strCLng = jsonObject.getString("endLong");
                                Log.e("sdfsfsfsd", "out" + " " + TotalKm);

                                if (!TotalKm.equals(0)) {
                                    Log.e("sdfsfsfsd", TotalKm);
                                    ActualKmOfVehicle = ActualKmOfVehicle.add(new BigDecimal(TotalKm));
                                    Log.e("ssfafasfasfasfasfs", ActualKmOfVehicle + "");
                                } else {
                                    Log.e("sdfsfsfsd", "Else" + " " + ActualKmOfVehicle);
                                }
                            }

                            String strStore=ActualKmOfVehicle+"";
                            if(strStore.contains(".")){
                                String strGet[]=strStore.split("\\.");
                                userUsedKm=strGet[0];
                            }

                            /*First lets apply user kilometer + Actual Km Of Vehicle*/
                            BigDecimal valueaddOne=new BigDecimal(strUserKM+"."+"0");
                            BigDecimal valueaddTwo=new BigDecimal(ActualKmOfVehicle+"");
                            BigDecimal totaAddedKm=valueaddOne.add(valueaddTwo);
                            Log.e("ghjgjgjg",""+totaAddedKm+"");

                            /*lets totaAddedKm - Actual Km Of Vehicle*/
                            BigDecimal vOne = new BigDecimal(totaAddedKm+"");
                            BigDecimal vTwo = new BigDecimal(ActualKmOfVehicle+"");
                            BigDecimal totaRemainingKm =vOne.subtract(vTwo);
                            String strFinalKmsd=totaRemainingKm+"";
                            Log.e("ertertetr",""+totaAddedKm+"");
                            Log.e("ertertetr", strFinalKmsd + "");
                            txtShowKMLeft.setText(strFinalKmsd+ " " + "Km");

                            /*ab check krna h ki gaadi ke km user ke km se less than hai ya grater tha ha */


                            /*lets getUserRemaining - Actual Km Of Vehicle*/
                            val1BD = new BigDecimal(totaAddedKm+"");
                            val2BD = new BigDecimal(ActualKmOfVehicle+"");
                            result =val1BD.subtract(val2BD);
                            String strFinalKm=result+"";
                           // Log.e("ghjgjgjg",""+totaAddedKm+"");
                            Log.e("edreryeyeyryrey", strFinalKm + "");



                             /*Marker work*/
                            if(!strCLat.equals("")||!strCLat.equals(null)){
                                LatLng latLng = new LatLng(Double.parseDouble(strCLat),Double.parseDouble(strCLng));
                                int heights = 80;
                                int widths = 80;
                                BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                                Bitmap bs = bitmapdraws.getBitmap();

                                Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
                                Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                        .position(latLng)
                                        .flat(true)
                                        .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))

                                );

                                mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));

                            }




                        } catch (Exception ex) {

                            Log.e("asfasfasfasf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("asfafasfasfa", "Error" + anError.getMessage());
                    }
                });

    }


    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }



    public void GetMyTripId(){
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_trip")
                .addBodyParameter("user_id", strUserId)
                .setTag("Show Trips")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("wrwerwer", response.toString());
                        try {
                            JSONArray jsonArray =new JSONArray(response.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {

                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if(strRideId.equals(jsonObject.getString("rand_no")));{
                                    strTripStatus="True";
                                }
                            }


                        } catch (Exception ex) {

                            Log.e("fghfhf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("tyretw", anError.getMessage());

                    }
                });
    }





    public void StartVehicle(String strStatus) {
        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", strStatus);
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog commandDialog = new ProgressDialog(RoughActivity.this);
        commandDialog.setMessage("Commanding..");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */
        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("response")) {
                                if (response.getString("response").equals("Command has been accepted successfully")) {

                                    linearBottom.setVisibility(View.VISIBLE);
                                    txtStart.setVisibility(View.GONE);

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    String FirstIgnitionStatus = AppsContants.sharedpreferences.getString(AppsContants.FirstIgnitionStatus, "");

                                    if (FirstIgnitionStatus.equals("")) {

                                        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                        editor.putString(AppsContants.FirstIgnitionStatus, "Yes");
                                        editor.commit();
                                        printDifference();
                                    } else {
                                        printDifference();
                                    }


                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(RoughActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(RoughActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(RoughActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(RoughActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }




    public void AddLastKiloMeter(){

        ProgressDialog progressDialog = new ProgressDialog(RoughActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.setCancelable(false);
        progressDialog.show();

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserID = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        Log.e("fdghdfdfghdf",strUserID);
        Log.e("fdghdfdfghdf",strRideId);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip")
                .addBodyParameter("user_id", strUserID)
                .addBodyParameter("rand_no", strRideId)
                .setTag("Add Trip Detail")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("wrwerwer", response.toString());
                        try {
                            if(response.getString("message").equals("add Successfully")){
                                progressDialog.dismiss();
                                strStatus = "REMOTE_IGNITION_OFF";
                                EndRide(strStatus);

                            }
                            else {
                                progressDialog.dismiss();
                                Toast.makeText(RoughActivity.this, "Error in trip details", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("fghfhf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("tyretw", anError.getMessage());

                    }
                });

    }



    public void EndRide(String strStatus) {
        Log.e("dgdfgdfgdfg", strAssetUID);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");

        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", strStatus);
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog commandDialog = new ProgressDialog(RoughActivity.this);
        commandDialog.setMessage("Commanding..");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */

        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetUID + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("response")) {
                                if (response.getString("response").equals("Command has been accepted successfully")) {
                                    linearBottom.setVisibility(View.GONE);
                                    txtStart.setVisibility(View.VISIBLE);
                                    commandDialog.dismiss();

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.IgnitionType, "");
                                    editor.putString(AppsContants.StartIgnitiontime, "");
                                    editor.putString(AppsContants.FirstIgnitionStatus, "");
                                    editor.putString(AppsContants.DeviceId, "");
                                    editor.putString(AppsContants.AccoundId, "");
                                    editor.putString(AppsContants.Maintoken, "");
                                    editor.commit();
                                    startActivity(new Intent(RoughActivity.this,MainActivity.class));
                                    finish();

                                    Toast.makeText(RoughActivity.this, response.getString("status"), Toast.LENGTH_SHORT).show();
                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(RoughActivity.this, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(RoughActivity.this, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(RoughActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(RoughActivity.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }

}