package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.Modals.DistanceModal;
import com.mybike.service.Modals.VehicleModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class VehicleDetailActivity extends AppCompatActivity {


    TextView txtIgnition;
    TextView txtVoltage;
    TextView txtAddress;
    TextView txtVin;
    TextView txtTYPE;
    TextView txtCompanyName;
    String strToken;
    String strDeviceId;
    String strLat = "";
    String strLng = "";
    String strAccurateAdd = "";
    String address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_detail);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtCompanyName = findViewById(R.id.txtCompanyName);
        txtTYPE = findViewById(R.id.txtTYPE);
        txtVin = findViewById(R.id.txtVin);
        txtAddress = findViewById(R.id.txtAddress);
        txtVoltage = findViewById(R.id.txtVoltage);
        txtIgnition = findViewById(R.id.txtIgnition);
    }

    @Override
    protected void onResume() {
        super.onResume();
        BikeDetail();
    }

    public void BikeDetail() {
        ProgressDialog progressDialog = new ProgressDialog(VehicleDetailActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();

        AndroidNetworking.get(HTTPCALL.AllAssets)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strToken)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("response", "" + response);
                        try {
                            for (int i = 0; i < response.length(); i++) {
                                JSONObject object = response.getJSONObject(i);

                                if (strDeviceId.equals(object.getString("deviceId"))) {

                                    txtIgnition.setText(object.getString("assetState"));
                                    txtVoltage.setText(object.getString("devicePowerVoltage"));
                                    txtVin.setText(object.getString("vin"));
                                    txtTYPE.setText(object.getString("assetType"));
                                    txtCompanyName.setText(object.getString("accountName"));
                                    strLat = object.getString("latitude");
                                    strLng = object.getString("longitude");

                                }
                            }

                            try {

                                if(!strLat.equals("")||!strLat.equals(null)){

                                    double lat= Double.parseDouble(strLat);
                                    double lng= Double.parseDouble(strLng);

                                    Geocoder geocoder;
                                    List<Address> addresses;
                                    geocoder = new Geocoder(VehicleDetailActivity.this, Locale.getDefault());

                                    addresses = geocoder.getFromLocation(lat, lng, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                                    if (addresses.size() != 0) {

                                        address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                                        final String city = addresses.get(0).getLocality();
                                        String state = addresses.get(0).getAdminArea();
                                        String knownName = addresses.get(0).getFeatureName();
                                        final String pincode = addresses.get(0).getPostalCode();

                                        Log.e("gigjk", addresses.get(0).getPostalCode() + "");

                                        Log.e("gigjk", "add" + address);
                                        Log.e("gigjk", "city" + city);

                                        String strAdd[] = address.split(",");

                                        for (int i = 0; i < strAdd.length; i++) {

                                            Log.e("fgdgdfgdfgf", i + " " + strAdd[i]);
                                            if (i != 0) {

                                                if (strAccurateAdd.equals("")) {

                                                    strAccurateAdd = strAdd[i];
                                                } else {
                                                    strAccurateAdd = strAccurateAdd + "," + strAdd[i];
                                                }
                                            }

                                        }
                                        txtAddress.setText(strAccurateAdd);
                                    }

                                }

                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            progressDialog.dismiss();

                        } catch (JSONException e) {
                            progressDialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("anError", "" + anError.getMessage());
                    }
                });
    }

}