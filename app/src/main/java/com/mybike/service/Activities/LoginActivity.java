package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.R;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LoginResponse;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {


    TextView login_forget_text, login_sign_txt;
    Button login_btn;
    EditText editPassword;
    EditText editEmail;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        login_forget_text = findViewById(R.id.login_forgot_txt);
        login_sign_txt = findViewById(R.id.login_sign_txt);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
       TextView login_forgot_txt = findViewById(R.id.login_forgot_txt);
        login_forgot_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, ForgetpasswordActivity.class);
                startActivity(intent);
            }
        });
        login_btn = findViewById(R.id.login_btn);
        login_sign_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(com.mybike.service.Activities.LoginActivity.this, Signup.class);
                startActivity(intent);
            }
        });


        login_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strEmail = editEmail.getText().toString().trim();
                String strPassword = editPassword.getText().toString().trim();

                if (strEmail.equals("")) {
                    Toast.makeText(getApplicationContext(), "Username cannot be blank", Toast.LENGTH_SHORT).show();

                } else if (strPassword.equals("")) {

                    Toast.makeText(getApplicationContext(), "Password cannot be blank", Toast.LENGTH_SHORT).show();
                } else {
                  //  startActivity(new Intent(com.mybike.service.Activities.LoginActivity.this, NavigationActivity.class));
                   Login(strEmail, strPassword);
                }
            }
        });
    }


    public void Login(String strEmail, String strPassword) {

        progressDialog = new ProgressDialog(com.mybike.service.Activities.LoginActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "login")
                .addBodyParameter("email", strEmail)
                .addBodyParameter("password", strPassword)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                                JSONObject jsonObject = new JSONObject(response.getString("data"));

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.UserId, jsonObject.getString("userID"));
                                editor.putString(AppsContants.fname, jsonObject.getString("fname"));
                                editor.putString(AppsContants.lname, jsonObject.getString("lname"));
                                editor.putString(AppsContants.email, jsonObject.getString("email"));
                                editor.putString(AppsContants.mobile, jsonObject.getString("mobile"));
                                editor.putString(AppsContants.Path, jsonObject.getString("path"));
                                editor.putString(AppsContants.ProfileImage, jsonObject.getString("profile_image"));
                                editor.putString(AppsContants.AdharNo, jsonObject.getString("adhar_no"));
                                editor.commit();
                                progressDialog.dismiss();
                                startActivity(new Intent(LoginActivity.this, NavigationActivity.class));
                                finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(LoginActivity.this, "Login details invalid", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


 /*   public void Login(String strEmail, String strPassword) {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<LoginResponse> loginResponseCall = service.userLogin(strEmail, strPassword);

        progressDialog = new ProgressDialog(LoginActivity.LoginActivity.this);
        progressDialog.setMessage("Loading...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                //Log.e("ddsgdfgdfg",response.toString());
                try {
                    String status = response.body().getToken();
                    Log.e("ddsgdfgdfg", status);
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.Maintoken, status);
                    editor.commit();
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.LoginActivity.this, "Successsfull", Toast.LENGTH_SHORT).show();

                    startActivity(new Intent(LoginActivity.LoginActivity.this, NavigationActivity.class));

                    // startActivity(new Intent(LoginActivity.this, Signup.class));
                    finish();
                } catch (Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(LoginActivity.LoginActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ddsgdfgdfg", t.getMessage());
                Toast.makeText(LoginActivity.LoginActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();
                progressDialog.dismiss();
            }
        });
    }
*/
}
    
    