package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.FragmentActivity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mybike.service.R;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LoginResponse;
import com.mybike.service.Utils.PlacesAdapter;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ShowVehicleActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;
    private LocationManager mLocationManager;
    private LocationListener mLocationListener;
    private LatLng mOrigin;
    PlacesClient placesClient;
    PlacesAdapter placeAdapter;

    AutoCompleteTextView edt_source;
    double ll;
    double lo;
    String strStartPoint = "";
    CheckBox check_box1;
    TextView tv_arrange;
    CardView cardStart,cardLocation;
    RelativeLayout rl_next,rl3;
    String status="";
    Marker thisUserMarker;
    Bitmap driverMarker;
    int width = 100 ;
    int height = 100;
String KM="60";
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_arrange_vehicle);

        GetParentAccountToken();
        edt_source = findViewById(R.id.edt_search);
        rl_next = findViewById(R.id.rl_next);
        check_box1 = findViewById(R.id.check_box1);
        tv_arrange = findViewById(R.id.tv_arrange);
        cardStart = findViewById(R.id.cardStart);
        cardLocation = findViewById(R.id.cardLocation);
        rl3 = findViewById(R.id.rl3);
        strStartPoint = edt_source.getText().toString();
        check_box1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check_box1.isChecked()) {
                    edt_source.setVisibility(View.VISIBLE);
                    cardStart.setVisibility(View.VISIBLE);
                    rl3.setVisibility(View.VISIBLE);
                    tv_arrange.setVisibility(View.GONE);
                } else {
                    edt_source.getText().clear();
                    edt_source.setVisibility(View.GONE);
                    cardStart.setVisibility(View.GONE);
                    rl3.setVisibility(View.GONE);
                    tv_arrange.setVisibility(View.VISIBLE);

                }
            }
        });
        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ShowVehicleActivity.this, PayNowActivity.class));
            }
        });

        ImageView ic_back=findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(ShowVehicleActivity.this, getResources().getString(R.string.google_maps_key));
        }
        placesClient = com.google.android.libraries.places.api.Places.createClient(ShowVehicleActivity.this);

        edt_source.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                edt_source.setOnItemClickListener(autocompleteClickListener);

                placeAdapter = new PlacesAdapter(ShowVehicleActivity.this, placesClient);
                try {
                    edt_source.setAdapter(placeAdapter);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        });

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        getMyLocation();
        mMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
            @Override
            public boolean onMarkerClick(Marker marker) {
                Geocoder geocoder = new Geocoder(ShowVehicleActivity.this, Locale.getDefault());
                //    addresses = geocoder.getFromLocation(marker.getPosition().latitude, marker.getPosition().longitude, 1); //1 num of possible location returned


                String a = marker.getId();

                int g;
                for(g = 0; g < a.length(); g++){
                    char c = a.charAt(g);
                    if( '0' <= c && c <= '9' )
                        break;
                }
                String alphaPart = a.substring(0, g);
                String numberPart = a.substring(g);
                Log.e("numberPart", ""+numberPart );
                if (Integer.parseInt(numberPart)==0){
                    numberPart="1";
                }
                return false;
            }
        });
    }
    private void getMyLocation(){

        // Getting LocationManager object from System Service LOCATION_SERVICE
        mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);

        mLocationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                mOrigin = new LatLng(location.getLatitude(), location.getLongitude());


            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

            }
        };

        if (mOrigin==null){
            AnimateCamera();
        }else
        { }

        int currentApiVersion = Build.VERSION.SDK_INT;
        if (currentApiVersion >= Build.VERSION_CODES.M) {

            if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_DENIED) {
                //mMap.setMyLocationEnabled(false);
                mMap.setMyLocationEnabled(true);
                mMap.getUiSettings().setMyLocationButtonEnabled(true);
                mLocationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,10000,0,mLocationListener);

            }else{
                requestPermissions(new String[]{
                        android.Manifest.permission.ACCESS_FINE_LOCATION
                },100);
            }
        }
    }
    public void AnimateCamera() {
        GPSTracker gpsTracker = new GPSTracker(ShowVehicleActivity.this);
        LatLng latLngs = new LatLng(gpsTracker.getLatitude(), gpsTracker.getLongitude());
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLngs)));
    }
    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(15).build();
    }
    private AdapterView.OnItemClickListener autocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            try {
                final AutocompletePrediction item = placeAdapter.getItem(i);
                String placeID = null;
                if (item != null) {
                    placeID = item.getPlaceId();
                }

//                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
//                Use only those fields which are required.

                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS
                        , Place.Field.LAT_LNG);

                FetchPlaceRequest request = null;

                if (placeID != null) {
                    request = FetchPlaceRequest.builder(placeID, placeFields)
                            .build();
                }

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onSuccess(FetchPlaceResponse task) {
                            final LatLng lotlonaddress = task.getPlace().getLatLng();
                            ll = lotlonaddress.latitude;
                            lo = lotlonaddress.longitude;

                            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                            editor.putString(AppsContants.ARRANGE_VEHICLE_LAT, String.valueOf(ll));
                            editor.putString(AppsContants.ARRANGE_VEHICLE_LNG, String.valueOf(lo));
                            editor.commit();
                            Log.e("dsfsdfsdf", "Start"+lo);


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("ukyhkii", e.getMessage());
                            // responseView.setText(e.getMessage());
                        }
                    });
                }
            } catch (Exception e) {
                Log.e("ukyhkii", e.getMessage());
            }

        }
    };


    public void showVehicles(String s){
        AndroidNetworking.get(HTTPCALL.AllAssets)
                .addHeaders("Content-Type","application/json")
                .addHeaders("token",status)
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        Log.e("response", "" +response);
                        try {
                            String latitude="";
                            String longitude="";
                            String BATTERY_PERC="";
                            for (int i = 0; i <response.length() ; i++) {
                                JSONObject object=response.getJSONObject(i);
                                if (s.equals("60")){
                                    Double VOLTAGE=49.7;
                                    Double API_VOLTAGE=Double.parseDouble(object.getString("devicePowerVoltage"));
                                    if (API_VOLTAGE>=VOLTAGE) {/*Showing Only for 60km and above */
                                        latitude = object.getString("latitude");
                                        longitude = object.getString("longitude");
                                        BATTERY_PERC = object.getString("devicePowerVoltage");
                                    }
                                }
                                else if (s.equals("30")){/*Showing Only for 30km and above */
                                    Double VOLTAGE=48.7;
                                    Double API_VOLTAGE=Double.parseDouble(object.getString("devicePowerVoltage"));
                                    if (API_VOLTAGE>=VOLTAGE) {
                                        latitude = object.getString("latitude");
                                        longitude = object.getString("longitude");
                                        BATTERY_PERC = object.getString("devicePowerVoltage");
                                    }
                                }
                                else {
                                   // Toast.makeText(ShowVehicleActivity.this, "Something Went Worng!", Toast.LENGTH_SHORT).show();
                                }

                            }

                            BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                            Bitmap b = bitmapdraw.getBitmap();

                            if (!latitude.isEmpty() && !longitude.isEmpty()) {
                                try {
                                    driverMarker = Bitmap.createScaledBitmap(b, width, height, false);
                                    Double aDouble_user_latitude = Double.parseDouble(latitude);
                                    Double aDouble_user_longitude = Double.parseDouble(longitude);
                                    LatLng Bhopal = new LatLng(aDouble_user_latitude, aDouble_user_longitude);
                                    thisUserMarker = mMap.addMarker(new MarkerOptions().position(Bhopal));
                                    thisUserMarker.setTag("id");
                                    thisUserMarker.setTitle(BATTERY_PERC);
                                    thisUserMarker.setIcon(BitmapDescriptorFactory.fromBitmap(driverMarker));
                                } catch (NumberFormatException e) {
                                    e.printStackTrace();
                                }
                            } else {
                                cardLocation.setVisibility(View.GONE);
                                Toast.makeText(ShowVehicleActivity.this, "No Vehicle Found!", Toast.LENGTH_SHORT).show();
                            }


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError", "" +anError.getMessage());
                    }
                });
    }



    public void GetParentAccountToken() {
        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String KM = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, "");
        Log.e("USER_SELECTED_KM", "" +KM);
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<LoginResponse> loginResponseCall = service.userLogin("suryanbajpai33@gmail.com", "Manirock33");

      /*  progressDialog = new ProgressDialog(OperateVehicleActivity.this);
        progressDialog.setMessage("Loading..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);*/

        loginResponseCall.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {

                //Log.e("ddsgdfgdfg",response.toString());
                try {
                     status = response.body().getToken();
                    Log.e("ddsgdfgdfg", status);
                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.Maintoken, status);
                    editor.commit();
                    showVehicles(KM);
                }
                catch (Exception e) {
                    Toast.makeText(ShowVehicleActivity.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                    e.printStackTrace();
                }
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("ddsgdfgdfg", t.getMessage());
                Toast.makeText(ShowVehicleActivity.this, "" + t.getMessage(), Toast.LENGTH_SHORT).show();

            }
        });
    }
}
