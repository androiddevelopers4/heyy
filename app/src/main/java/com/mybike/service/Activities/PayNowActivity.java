package com.mybike.service.Activities;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;

import com.bumptech.glide.Glide;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import de.hdodenhof.circleimageview.CircleImageView;

public class PayNowActivity extends AppCompatActivity {
    Button btn_paynow;
    TextView tv1, tv_price, tv2, tv_price_num, total_amnt, txtCoins;
    RelativeLayout rl2;
    String Price = "";
    int i = 0;

    CircleImageView sign_email;
    TextView txtUserName;
    String strRideExists = "";
    String strDeviceId = "";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pay_now);
        btn_paynow = findViewById(R.id.btn_paynow);


        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserImage = AppsContants.sharedpreferences.getString(AppsContants.ProfileImage, "");
        String strPath = AppsContants.sharedpreferences.getString(AppsContants.Path, "");
        String strFirstName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        String strLastName = AppsContants.sharedpreferences.getString(AppsContants.lname, "");
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        strRideExists = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
        strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        Log.e("dfgdgdfgfd", strUserId);
        Log.e("dfgdgdfgfd", strPath + strUserImage);

        txtCoins = findViewById(R.id.txtCoins);
        txtUserName = findViewById(R.id.txtUserName);
        txtUserName.setText(strFirstName + " " + strLastName);
        sign_email = findViewById(R.id.sign_email);


        if (!strUserImage.equals("")) {
            Glide.with(this).load(strPath + strUserImage).into(sign_email);
        } else {

            Glide.with(this).load(R.drawable.profile).into(sign_email);
        }

        tv1 = findViewById(R.id.tv1);
        tv2 = findViewById(R.id.tv2);
        tv_price_num = findViewById(R.id.tv_price_num);
        tv_price = findViewById(R.id.tv_price);
        rl2 = findViewById(R.id.rl2);
        total_amnt = findViewById(R.id.total_amnt);
        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String USER_ID = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String KM = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_KM, "");
        String USER_SELECTED_HOUR = AppsContants.sharedpreferences.getString(AppsContants.USER_SELECTED_HOUR, "");
        Price = AppsContants.sharedpreferences.getString(AppsContants.Price, "");
        String ARRANGE_VEHICLE_LNG = AppsContants.sharedpreferences.getString(AppsContants.ARRANGE_VEHICLE_CHECKBOX_STATUS, "");
        Log.e("USER_SELECTED_KM", "" + KM);
        Log.e("USER_SELECTED_KM", "" + USER_ID);
        Log.e("dsdsddsgsdg", "" + Price);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.StationPathSatus, "");
        editor.commit();

        i = Integer.parseInt(Price);
        tv1.setText(USER_SELECTED_HOUR);
        if (ARRANGE_VEHICLE_LNG.equals("true")) {

            tv_price_num.setText("INR 50");
            rl2.setVisibility(View.VISIBLE);
            tv2.setVisibility(View.VISIBLE);
            tv_price.setText("INR " + i);
            i = i + 50;
            total_amnt.setText("INR " + i);
        } else {
            rl2.setVisibility(View.GONE);
            tv2.setVisibility(View.GONE);
            tv_price.setText("INR " + i);
            total_amnt.setText("INR " + i);
        }


        ImageView ic_back = findViewById(R.id.ic_back);
        ic_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_paynow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                /* startActivity(new Intent(PayNowActivity.this,PaytmActivity.class));*/
                SweetAlertDialog sweetAlertDialog = new SweetAlertDialog(PayNowActivity.this, SweetAlertDialog.NORMAL_TYPE);
                sweetAlertDialog.setTitleText("Are you sure?");
                sweetAlertDialog.setContentText("Your Price is " + " " + total_amnt.getText().toString());
                sweetAlertDialog.setCancelText("No,cancel");
                sweetAlertDialog.setConfirmText("Yes,Book it!");
                sweetAlertDialog.setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sDialog) {

                        if (strRideExists.equals("")) {
                            sweetAlertDialog.dismiss();
                            deductCoins(USER_ID, String.valueOf(i));
                        } else {
                            AlreadyRidePopup();
                            sweetAlertDialog.dismiss();
                        }
                    }
                })
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sDialog) {
                                //startActivity(new Intent(PayNowActivity.this, DrawPathVehicle.class));
                                sDialog.cancel();
                            }
                        })
                        .show();

            }
        });


        Button btnRecharge = findViewById(R.id.btnRecharge);
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.RechargeStatus, "2");
                editor.commit();
                startActivity(new Intent(PayNowActivity.this, EnterAmount.class));

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowCoins();
    }


    public void AlreadyRidePopup() {
        final Dialog dialog = new Dialog(PayNowActivity.this);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.previous_ride_popup);

        Button btnOK = (Button) dialog.findViewById(R.id.btnOK);
        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(PayNowActivity.this, NavigationActivity.class));
                finish();
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    public void deductCoins(String user_id, String coins) {
        Log.e("deductCoinsfdfd", "" + coins);
        Log.e("deductCoinsfdfd", "" + user_id);
        ProgressDialog progressDialog = new ProgressDialog(PayNowActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "deduct_coin")
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("coin", coins)
                .addBodyParameter("device_id", strDeviceId)

                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", "" + response);
                        progressDialog.dismiss();
                        try {
                            if (response.has("result")) {
                                if (response.getString("result").equals("true")) {

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.PaymentUsedStatus, "true");
                                    editor.commit();
                                   // startActivity(new Intent(PayNowActivity.this, DrawPathVehicle.class));
                                    startActivity(new Intent(PayNowActivity.this, OperateVehicleActivity.class));
                                    finish();
                                } else {
                                    Toast.makeText(PayNowActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.RechargeStatus, "2");
                                    editor.commit();
                                    startActivity(new Intent(PayNowActivity.this, EnterAmount.class));
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError", "" + anError);
                        progressDialog.dismiss();
                    }
                });
    }


    public void ShowCoins() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");


        ProgressDialog progressDialogg = new ProgressDialog(PayNowActivity.this);
        progressDialogg.setMessage("Please wait...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_coin")
                .addBodyParameter("user_id", strUserId)
                .setTag("Show Coins")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("dfgdfgdfgd", response.toString());
                            if (response.getString("message").equals("coin showing Successfully")) {

                                JSONObject jsonObject = new JSONObject(response.getString("data"));
                                txtCoins.setText(jsonObject.getString("coin") + " " + "INR");
                                progressDialogg.dismiss();

                            } else {
                                progressDialogg.dismiss();
                                Toast.makeText(PayNowActivity.this, "Coins Not Added", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("dfgdfgdfgd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("dfgdfgdfgd", anError.toString());
                    }
                });

    }


}