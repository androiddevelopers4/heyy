package com.mybike.service.Modals;

public class PriceListModel {
    String PRICE,HOUR,MINUTE;
    String ID;

    public String getID() {
        return ID;
    }

    public void setID(String ID) {
        this.ID = ID;
    }

    public String getPRICE() {
        return PRICE;
    }

    public void setPRICE(String PRICE) {
        this.PRICE = PRICE;
    }

    public String getHOUR() {
        return HOUR;
    }

    public void setHOUR(String HOUR) {
        this.HOUR = HOUR;
    }

    public String getMINUTE() {
        return MINUTE;
    }

    public void setMINUTE(String MINUTE) {
        this.MINUTE = MINUTE;
    }
}