package com.mybike.service.Utils;

import android.content.SharedPreferences;

public class AppsContants {

    public static SharedPreferences sharedpreferences;
    private static String myprefrencesKey;
    public static final String MyPREFERENCES = myprefrencesKey;
    public static final String AccessToken = "AccessToken";
    public static final String RefreshToken = "RefreshToken";
    public static final String Maintoken = "Maintoken";
    public static final String VehicleOnOffStatus = "VehicleOnOffStatus";
    public static final String Price = "Price";
    public static final String UserId = "UserId";
    public static final String fname = "fname";
    public static final String lname = "lname";
    public static final String email = "email";
    public static final String mobile = "mobile";
    public static final String pickPointName = "pickPointName";
    public static final String dropPointName = "dropPointName";
    public static final String pickPointLat = "pickPointLat";
    public static final String pickPointLng = "pickPointLng";
    public static final String dropPointLat = "dropPointLat";
    public static final String dropPointLng = "dropPointLng";
    public static final String DeviceId= "deviceId";
    public static final String AccoundId = "AccoundId";
    public static final String PickPointVehicleLat = "PickPointVehicleLat";
    public static final String PickPointVehicleLng = "PickPointVehicleLng";
    public static final String PaymentUsedStatus = "PaymentUsedStatus";

    public static final String ARRANGE_VEHICLE_LAT = "ARRANGE_VEHICLE_LAT";
    public static final String ARRANGE_VEHICLE_LNG = "ARRANGE_VEHICLE_LNG";
    public static final String ARRANGE_VEHICLE_CHECKBOX_STATUS = "ARRANGE_VEHICLE_CHECKBOX_STATUS";
    public static final String USER_SELECTED_KM = "USER_SELECTED_KM"; // Storing for to show vehicle on ShowVehicleActivity
    public static final String USER_SELECTED_HOUR = "USER_SELECTED_HOUR";
    public static final String Path = "Path";
    public static final String ProfileImage = "ProfileImage";
    public static final String AdharNo = "AdharNo";
    public static final String RechargeStatus = "RechargeStatus";
    public static final String StartIgnitiontime = "StartIgnitiontime";
    public static final String IgnitionType = "IgnitionType";
    public static final String RideId = "RideId";
    public static final String FirstIgnitionStatus = "FirstIgnitionStatus";
    public static final String LastKm = "LastKm";
    public static final String UserLapseKm = "UserLapseKm";
    public static final String FIRSTLAT = "FIRSTLAT";
    public static final String FIRSTLNG = "FIRSTLNG";
    public static final String LASTLAT = "LASTLAT";
    public static final String LASTLNG = "LASTLNG";
    public static final String PACKAGEID = "PACKAGEID";
    public static final String AssetId = "AssetId";
    public static final String RenewStatus = "RenewStatus";
    public static final String StationPathSatus = "StationPathSatus";
    public static final String StationLat = "StationLat";
    public static final String StationLng = "StationLng";
    public static final String TestKm = "TestKm";
    public static final String PayPrice = "PayPrice";
    public static final String password = "password";
    public static final String OTP = "OTP";


}