package com.mybike.service.Modals;

public class DistanceModal {

    double min;
    String lat;
    String lng;


    public double getMin() {
        return min;
    }

    public void setMin(double min) {
        this.min = min;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }
}