package com.mybike.service.Fragments;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;

import android.os.Handler;
import android.os.SystemClock;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.Projection;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.libraries.places.api.model.AutocompletePrediction;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.api.net.FetchPlaceRequest;
import com.google.android.libraries.places.api.net.FetchPlaceResponse;
import com.google.android.libraries.places.api.net.PlacesClient;
import com.mybike.service.Activities.ArrangeVehicleActivity;
import com.mybike.service.Activities.GeofenceActivity;
import com.mybike.service.Activities.NavigationActivity;
import com.mybike.service.Activities.OperateVehicleActivity;
import com.mybike.service.Activities.PricingListActivity;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LatLngInterpolator;
import com.mybike.service.Utils.MarkerAnimation;
import com.mybike.service.Utils.PlacesAdapter;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;


public class HomeFragment extends Fragment implements OnMapReadyCallback {

    RelativeLayout rl_next;
    GoogleMap mGoogleMap;
    LocationRequest mLocationRequest;
    GoogleApiClient mGoogleApiClient;
    Location mLastLocation;
    Marker mCurrLocationMarker;
    SupportMapFragment mapFrag;
    boolean hideMarker;
    String strUserToken = "";
    String strDeviceID = "";
    CardView cardStart;

    String strStatus = "";
    AutoCompleteTextView edt_source;
    AutoCompleteTextView edt_destination;
    PlacesClient placesClient;
    PlacesAdapter placeAdapter;
    double ll;
    double lo;
    int add_status;
    String strStartPoint = "";
    String strEndPoint = "";
    CardView btnBackToVehicle;
    ProgressDialog progressDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
        Log.e("sdfsdfsdfsf", strUserToken);

        btnBackToVehicle = view.findViewById(R.id.btnBackToVehicle);
        edt_source = view.findViewById(R.id.edt_search);
        edt_destination = view.findViewById(R.id.edt_search_drop);
        cardStart = view.findViewById(R.id.cardStart);

        rl_next = view.findViewById(R.id.rl_next);
         progressDialog = new ProgressDialog(getActivity());

        mapFrag = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        hideMarker = false;


        cardStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
               /* strStatus="RESET_IMMOBILIZE";
                ImmobilizeVehicle(strStatus);*/


                strStartPoint = edt_source.getText().toString();
                strEndPoint = edt_destination.getText().toString();

                if (strStartPoint.equals("")) {

                    edt_source.setHintTextColor(getResources().getColor(R.color.red));
                    edt_destination.setTextColor(getResources().getColor(R.color.sendotp_grey));
                    Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
                    edt_source.startAnimation(shake);

                } else if (strEndPoint.equals("")) {

                    edt_destination.setHintTextColor(getResources().getColor(R.color.red));
                    edt_source.setTextColor(getResources().getColor(R.color.sendotp_grey));
                    Animation shake = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
                    edt_destination.startAnimation(shake);
                } else {

                    AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                    editor.putString(AppsContants.pickPointName, strStartPoint);
                    editor.putString(AppsContants.dropPointName, strEndPoint);
                    editor.commit();
                    startActivity(new Intent(getActivity(), PricingListActivity.class));
                }


            }
        });

        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            com.google.android.libraries.places.api.Places.initialize(getActivity(), getResources().getString(R.string.google_maps_key));
        }
        placesClient = com.google.android.libraries.places.api.Places.createClient(getActivity());

        edt_source.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                add_status = 1;
                edt_source.setOnItemClickListener(autocompleteClickListener);
                placeAdapter = new PlacesAdapter(getActivity(), placesClient);
                edt_source.setAdapter(placeAdapter);
            }
        });

        edt_destination.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                add_status = 2;
                edt_destination.setOnItemClickListener(autocompleteClickListener);

                placeAdapter = new PlacesAdapter(getActivity(), placesClient);
                edt_destination.setAdapter(placeAdapter);
            }
        });


        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.RechargeStatus, "FALSE");
                editor.commit();
                startActivity(new Intent(getActivity(), PricingListActivity.class));
            }
        });


        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        }

        btnBackToVehicle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(getActivity(), OperateVehicleActivity.class));
            }
        });


       /* UserLocation 23.24734278
        2020-10-12 12:17:27.406 13797-13797/com.mybike.service E/ssffdfdsfds: UserLocation 77.39483233
        2020-10-12 12:17:27.406 13797-13797/com.mybike.service E/ssffdfdsfds: stationLat 23.2285125
        2020-10-12 12:17:27.406 13797-13797/com.mybike.service E/ssffdfdsfds: stationLng 77.4352748
        2020-10-12 12:17:27.407 13797-13797/com.mybike.service E/ssffdfdsfds: 4634.642578125*/


        GPSTracker gpsTracker = new GPSTracker(getActivity());


        String strStrAddLat = String.valueOf(gpsTracker.getLatitude());
        String strStrAddLNG = String.valueOf(gpsTracker.getLongitude());


        Log.e("ssffdfdsfds", "UserLocation" + " " + strStrAddLat);
        Log.e("ssffdfdsfds", "UserLocation" + " " + strStrAddLNG);
        return view;
    }


    private void buildAlertMessageNoGps() {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage("Your GPS seems to be disabled, do you want to enable it?")
                .setCancelable(false)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    public void onClick(final DialogInterface dialog, final int id) {
                        dialog.cancel();
                    }
                });
        final AlertDialog alert = builder.create();
        alert.show();
    }


    private AdapterView.OnItemClickListener autocompleteClickListener = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

            try {
                final AutocompletePrediction item = placeAdapter.getItem(i);
                String placeID = null;
                if (item != null) {
                    placeID = item.getPlaceId();
                }

//                To specify which data types to return, pass an array of Place.Fields in your FetchPlaceRequest
//                Use only those fields which are required.

                List<Place.Field> placeFields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS
                        , Place.Field.LAT_LNG);

                FetchPlaceRequest request = null;

                if (placeID != null) {
                    request = FetchPlaceRequest.builder(placeID, placeFields)
                            .build();
                }

                if (request != null) {
                    placesClient.fetchPlace(request).addOnSuccessListener(new OnSuccessListener<FetchPlaceResponse>() {
                        @SuppressLint("SetTextI18n")
                        @Override
                        public void onSuccess(FetchPlaceResponse task) {
                            final LatLng lotlonaddress = task.getPlace().getLatLng();
                            ll = lotlonaddress.latitude;
                            lo = lotlonaddress.longitude;

                            if (add_status == 1) {
                                // updateLocation();
                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.pickPointLat, String.valueOf(ll));
                                editor.putString(AppsContants.pickPointLng, String.valueOf(lo));
                                editor.commit();
                                Log.e("dsfsdfsdf", "Start");
                            } else if (add_status == 2) {
                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.dropPointLat, String.valueOf(ll));
                                editor.putString(AppsContants.dropPointLng, String.valueOf(lo));
                                editor.commit();
                                Log.e("dsfsdfsdf", "End");
                            }


                        }
                    }).addOnFailureListener(new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.e("ukyhkii", e.getMessage());
                            // responseView.setText(e.getMessage());
                        }
                    });
                }
            } catch (Exception e) {
                Log.e("ukyhkii", e.getMessage());
            }

        }
    };


    @Override
    public void onResume() {

        printDifference();

        /*AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
        if (IgnitionType.equals("")) {

            Log.e("dfggdgdfgd", "Then");
            btnBackToVehicle.setVisibility(View.GONE);
            cardStart.setVisibility(View.VISIBLE);
        } else {
            btnBackToVehicle.setVisibility(View.VISIBLE);
            cardStart.setVisibility(View.GONE);
        }*/
        super.onResume();
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                getActivity(), R.raw.maps_style);
        googleMap.setMapStyle(style);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        final LocationManager manager = (LocationManager) getActivity().getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            buildAlertMessageNoGps();

        } else {

            GPSTracker gpsTracker = new GPSTracker(getActivity());
            double lat1 = gpsTracker.getLatitude();
            double lng1 = gpsTracker.getLongitude();
            LatLng latLng = new LatLng(lat1, lng1);
            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
        }

        GetStations();
    }


    public void printDifference() {

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strWhenStopVehicleTime = AppsContants.sharedpreferences.getString(AppsContants.StartIgnitiontime, "");

        Log.e("sdfsdfsdf", "Vehicle time" + " " + strWhenStopVehicleTime);

        if (strWhenStopVehicleTime.equals("")) {

            AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
            String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
            if (IgnitionType.equals("")) {

                Log.e("dfggdgdfgd", "Then");
                btnBackToVehicle.setVisibility(View.GONE);
                cardStart.setVisibility(View.VISIBLE);
            } else {
                btnBackToVehicle.setVisibility(View.VISIBLE);
                cardStart.setVisibility(View.GONE);
            }
        }

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss", Locale.getDefault());
        String currentTime = df.format(c);
        Log.e("eryuweyuui", currentTime);
        Log.e("eryuweyuui", strWhenStopVehicleTime);
        Date start = null;
        Date end = null;
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
        try {
            start = simpleDateFormat.parse(currentTime);
            end = simpleDateFormat.parse(strWhenStopVehicleTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        //1 minute = 60 seconds
        //1 hour = 60 x 60 = 3600
        //1 day = 3600 x 24 = 86400

        //milliseconds
        try {
            long different = end.getTime() - start.getTime();
            System.out.println("startDate : " + start);
            System.out.println("endDate : " + end);
            System.out.println("different : " + different);

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;

            long elapsedHours = different / hoursInMilli;

            different = different % hoursInMilli;

            long elapsedMinutes = different / minutesInMilli;

            different = different % minutesInMilli;

            long elapsedSeconds = different / secondsInMilli;

            System.out.printf("%d hours, %d minutes, %d seconds%n", elapsedHours, elapsedMinutes, elapsedSeconds);
            Log.e("dfggdgdfgd", elapsedHours + "");
            Log.e("dfggdgdfgd", elapsedMinutes + "");

            if (elapsedHours < 1) {
                Log.e("dfggdgdfgd", "Hour is less than 0");

                if (elapsedMinutes < 1) {

                    Log.e("dfggdgdfgd", "Minute is less than 0");

                    if (elapsedSeconds < 1) {
                        Log.e("dfggdgdfgd", "Seconds is less than 0");
                       // getVehicleBookStatus();
                        AddTripDetailMaually();
                    } else {
                        Log.e("dfggdgdfgd", "Sec Else");
                        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
                        if (IgnitionType.equals("")) {

                            Log.e("dfggdgdfgd", "Then");
                            btnBackToVehicle.setVisibility(View.GONE);
                            cardStart.setVisibility(View.VISIBLE);
                        } else {
                            btnBackToVehicle.setVisibility(View.VISIBLE);
                            cardStart.setVisibility(View.GONE);
                        }

                    }
                } else {
                    Log.e("dfggdgdfgd", "Min Else");
                    AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                    String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
                    if (IgnitionType.equals("")) {

                        Log.e("dfggdgdfgd", "Then");
                        btnBackToVehicle.setVisibility(View.GONE);
                        cardStart.setVisibility(View.VISIBLE);
                    } else {
                        btnBackToVehicle.setVisibility(View.VISIBLE);
                        cardStart.setVisibility(View.GONE);
                    }

                }
            } else {

                Log.e("dfggdgdfgd", "Hour Else");

                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
                if (IgnitionType.equals("")) {

                    Log.e("dfggdgdfgd", "Then");
                    btnBackToVehicle.setVisibility(View.GONE);
                    cardStart.setVisibility(View.VISIBLE);
                } else {
                    btnBackToVehicle.setVisibility(View.VISIBLE);
                    cardStart.setVisibility(View.GONE);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void GetStations() {

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_station")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String strName = "";
                            LatLng latLng = null;
                            Log.e("dfsdfsdsfsd", response.toString());

                            JSONArray jsonArray = new JSONArray(response.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (!jsonObject.getString("latitude").equals(null)) {

                                    double latVehicle = Double.parseDouble(jsonObject.getString("latitude"));
                                    double lngVehicle = Double.parseDouble(jsonObject.getString("longitude"));
                                    strName = jsonObject.getString("name");
                                    latLng = new LatLng(latVehicle, lngVehicle);
                                    int heights = 120;
                                    int widths = 110;
                                    BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.ddd);
                                    Bitmap bs = bitmapdraws.getBitmap();


                                    Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
                                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .title(strName)
                                            .flat(true)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))

                                    );
                                }
                            }

                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));

                            //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16F));


                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }


    public void getVehicleBookStatus() {

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        String strPACKAGEID = AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID, "");

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "vehical_status")
                .addBodyParameter("vehical_no", strDeviceId)
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("package_id", strPACKAGEID)
                .addBodyParameter("status", "0")   /*1-to boook ride - 0 to finish ride*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("sdsdggsdgsdgsd", response.toString());
                            if (response.getString("message").equals("vehical not booked")) {
                                progressDialog.dismiss();

                                Log.e("dfggdgdfgd", "Second is less than 0");

                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.putString(AppsContants.StationPathSatus, "");
                                editor.putString(AppsContants.RenewStatus, "");
                                editor.commit();


                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                String IgnitionType = AppsContants.sharedpreferences.getString(AppsContants.IgnitionType, "");
                                if (IgnitionType.equals("")) {

                                    Log.e("dfggdgdfgd", "Then");
                                    btnBackToVehicle.setVisibility(View.GONE);
                                    cardStart.setVisibility(View.VISIBLE);
                                } else {
                                    btnBackToVehicle.setVisibility(View.VISIBLE);
                                    cardStart.setVisibility(View.GONE);
                                }

                            } else {
                                progressDialog.dismiss();
                                Log.e("dfggdgdfgd", "Second is less than 0");

                                AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.putString(AppsContants.StationPathSatus, "");
                                editor.commit();

                                Toast.makeText(getActivity(), response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }



    public void AddTripDetailMaually() {

        GPSTracker gpsTracker = new GPSTracker(getActivity());
        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
        editor.putString(AppsContants.LASTLAT, gpsTracker.getLatitude() + "");
        editor.putString(AppsContants.LASTLNG, gpsTracker.getLongitude() + "");
        editor.putString(AppsContants.RenewStatus, "1");
        editor.commit();


        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strFLat = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLAT, "");
        String strFLng = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLNG, "");
        String strEndLat = AppsContants.sharedpreferences.getString(AppsContants.LASTLAT, "");
        String strEndLng = AppsContants.sharedpreferences.getString(AppsContants.LASTLNG, "");
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        String strPackageID = AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID, "");

        Log.e("dsfsdfsdfsdf", strFLat);
        Log.e("dsfsdfsdfsdf", strFLng);
        Log.e("dsfsdfsdfsdf", strEndLat);
        Log.e("dsfsdfsdfsdf", strEndLng);
        Log.e("dsfsdfsdfsdf", strPackageID);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip_history")
                .addBodyParameter("userID", strUserId)
                .addBodyParameter("packageID", strPackageID)
                .addBodyParameter("device_id", strDeviceId)
                .addBodyParameter("start_latitude", strFLat)
                .addBodyParameter("start_longitude", strFLng)
                .addBodyParameter("end_latitude", strEndLat)
                .addBodyParameter("end_longitude", strEndLng)
                .addBodyParameter("date", formattedDate)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("asfasfsafasfasf", response.toString());
                            if (response.getString("message").equals("add Successfully")) {

                                getVehicleBookStatus();

                            } else {

                                Toast.makeText(getActivity(), response.getString("result"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {


                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


}
