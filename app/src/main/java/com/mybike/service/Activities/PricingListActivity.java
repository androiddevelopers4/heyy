package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.Adapter.ShowPriceAdapter;
import com.mybike.service.GPSTracker;
import com.mybike.service.Modals.PriceListModel;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class PricingListActivity extends Activity {
    TextView select_trip;
    public static Button rl_next;
    String value = "";
    RecyclerView recyclerView;
    ArrayList<PriceListModel> priceListModels;
    ShowPriceAdapter showPriceAdapter;
    public static  String strHour="";



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pricing_list);
        select_trip = findViewById(R.id.select_trip);
        rl_next = findViewById(R.id.rl_next);
        recyclerView = findViewById(R.id.recyclerView);
        RelativeLayout rel_240 = findViewById(R.id.rel_240);


        showPrice();

        rl_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                startActivity(new Intent(PricingListActivity.this, SelectLocationActivity.class));

              /*  try {

                    Date c = Calendar.getInstance().getTime();
                    SimpleDateFormat df = new SimpleDateFormat("hh:mm:ss'Z'", Locale.getDefault());
                    String CurrentDate = df.format(c);
                    // Log.e("======= Hours", "hhshshsdhdh" + CurrentDate);
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm:ss'Z'");

                    Date date1 = simpleDateFormat.parse(CurrentDate);
                    //  Date date1 = simpleDateFormat.parse(CurrentDate);
                   // Date date2 = simpleDateFormat.parse("07:00 p.m.");
                    Date date2 = simpleDateFormat.parse("07:00:00Z");

                    long difference = date2.getTime() - date1.getTime();
                    int days = (int) (difference / (1000 * 60 * 60 * 24));
                    int hours = (int) ((difference - (1000 * 60 * 60 * 24 * days)) / (1000 * 60 * 60));
                    int min = (int) (difference - (1000 * 60 * 60 * 24 * days) - (1000 * 60 * 60 * hours)) / (1000 * 60);
                    hours = (hours < 0 ? -hours : hours);
                    Log.e("======= Hours", "hhshshsdhdh" + hours);
                    Log.e("======= Hours", "hhshshsdhdh" + min);

                    int packageHour= Integer.parseInt(strHour);
                    if(hours>=packageHour){
                        startActivity(new Intent(PricingListActivity.this, SelectLocationActivity.class));
                    }
                    else {
                        Dialog dialog = new Dialog(PricingListActivity.this);
                        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                        dialog.setCancelable(false);
                        dialog.setContentView(R.layout.service_popup);
                        TextView dialog_refund = dialog.findViewById(R.id.dialog_refund);
                       // dialog_refund.setText("We have refunded ride price"+" "+strPrice+" "+"INR"+" "+"in your wallet.");

                        Button dialog_scan = dialog.findViewById(R.id.dialog_scan);
                        dialog_scan.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                        dialog.show();
                    }
                }
                catch (Exception ex){
                    Toast.makeText(PricingListActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();

                }
                */
            }
        });




    }

    public void showPrice() {

        ProgressDialog commandDialog = new ProgressDialog(PricingListActivity.this);
        commandDialog.setMessage("Please wait..");
        commandDialog.setIndeterminate(true);
        commandDialog.show();
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_price")

                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", "" + response);
                        try {
                            if (response.has("result")) {

                                if (response.getString("result").equals("true")) {
                                    priceListModels = new ArrayList<>();

                                    String s = response.getString("data");
                                    JSONArray jsonArray = new JSONArray(s);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        PriceListModel priceListModel = new PriceListModel();
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        priceListModel.setID(object.getString("priceID"));
                                        priceListModel.setHOUR(object.getString("hour"));
                                        priceListModel.setMINUTE(object.getString("km"));
                                        priceListModel.setPRICE(object.getString("price"));
                                        priceListModels.add(priceListModel);
                                    }
                                    recyclerView.setHasFixedSize(true);
                                    recyclerView.setLayoutManager(new LinearLayoutManager(PricingListActivity.this));
                                    recyclerView.setAdapter(new ShowPriceAdapter(priceListModels, PricingListActivity.this));
                                    commandDialog.dismiss();
                                }
                            }
                        } catch (JSONException e) {
                            commandDialog.dismiss();
                            e.printStackTrace();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Log.e("anError", "" + anError);
                    }
                });
    }




}





