package com.mybike.service.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.android.volley.toolbox.ImageLoader;
import com.mybike.service.Modals.VehicleModal;
import com.mybike.service.R;

import java.util.List;

public class VehicleAdapter extends RecyclerView.Adapter<VehicleAdapter.ViewHolder> {

    Context context;

    List<VehicleModal> dataAdapters;

    public VehicleAdapter(List<VehicleModal> getDataAdapter, Context context){

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.vehicle_items, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, int position) {

        VehicleModal dataAdapterOBJ =  dataAdapters.get(position);

      Viewholder.txtid.setText(dataAdapterOBJ.getId());
      Viewholder.txtName.setText(dataAdapterOBJ.getName());
      Viewholder.txtMake.setText(dataAdapterOBJ.getMake());
      Viewholder.txtModal.setText(dataAdapterOBJ.getModal());
      Viewholder.txtYear.setText(dataAdapterOBJ.getYear());
      Viewholder.txtStatus.setText(dataAdapterOBJ.getStatus());
      Viewholder.txtAccount.setText(dataAdapterOBJ.getAccount());
      Viewholder.txtSpeed.setText(dataAdapterOBJ.getSpeed()+" "+"km/hr");
      Log.e("dfggdsfgdf",dataAdapterOBJ.getSpeed());

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtid,txtName,txtMake,txtModal;
        public TextView txtYear,txtStatus,txtAccount,txtSpeed;

        public ViewHolder(View itemView) {

            super(itemView);

            txtid =itemView.findViewById(R.id.txtid);
            txtName =itemView.findViewById(R.id.txtName);
            txtMake =itemView.findViewById(R.id.txtMake);
            txtModal =itemView.findViewById(R.id.txtModal);
            txtYear =itemView.findViewById(R.id.txtYear);
            txtStatus =itemView.findViewById(R.id.txtStatus);
            txtAccount =itemView.findViewById(R.id.txtAccount);
            txtSpeed =itemView.findViewById(R.id.txtSpeed);
        }
    }
}
