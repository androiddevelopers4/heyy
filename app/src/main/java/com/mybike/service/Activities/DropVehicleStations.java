package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.Adapter.DropStationsAdapter;
import com.mybike.service.Adapter.ShowPriceAdapter;
import com.mybike.service.Modals.DropStationsModal;
import com.mybike.service.Modals.PriceListModel;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class DropVehicleStations extends AppCompatActivity {

    RecyclerView recyclerView;
    ArrayList<DropStationsModal> priceListModels;
    DropStationsAdapter dropStationsAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drop_vehicle_stations);

        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        GetStations();
    }


    public void GetStations() {
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_station")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String strName = "";
                            LatLng latLng = null;
                            Log.e("dfsdfsdsfsd", response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("data"));

                            priceListModels=new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (!jsonObject.getString("latitude").equals(null)) {

                                    double latVehicle = Double.parseDouble(jsonObject.getString("latitude"));
                                    double lngVehicle = Double.parseDouble(jsonObject.getString("longitude"));
                                    strName = jsonObject.getString("name");

                                    DropStationsModal dropStationsModal=new DropStationsModal();
                                    dropStationsModal.setId(jsonObject.getString("stationID"));
                                    dropStationsModal.setName(strName);
                                    dropStationsModal.setLat(latVehicle+"");
                                    dropStationsModal.setLng(lngVehicle+"");
                                    priceListModels.add(dropStationsModal);

                                }
                                dropStationsAdapter=new DropStationsAdapter(priceListModels,DropVehicleStations.this);
                                recyclerView.setAdapter(dropStationsAdapter);
                            }

                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

    }



}