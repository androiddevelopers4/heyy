package com.mybike.service.Room;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.io.Serializable;

@Entity
public class Item implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "Tripid")
    private String tripId;

    @ColumnInfo(name = "TotalKm")
    private String totalkmVehicle;

    @ColumnInfo(name = "TripAerisID")
    private String tripAerisID;
    @ColumnInfo(name = "LastLat")
    private String lat;
    @ColumnInfo(name = "LastLng")
    private String lng;

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTripAerisID() {
        return tripAerisID;
    }

    public void setTripAerisID(String tripAerisID) {
        this.tripAerisID = tripAerisID;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTotalkmVehicle() {
        return totalkmVehicle;
    }

    public void setTotalkmVehicle(String totalkmVehicle) {
        this.totalkmVehicle = totalkmVehicle;
    }
}
