package com.mybike.service.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.location.Location;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mybike.service.Modals.TripsModal;
import com.mybike.service.R;

import java.util.List;

public class TripsAdapter extends RecyclerView.Adapter<TripsAdapter.ViewHolder> {

    Context context;
    List<TripsModal> dataAdapters;
    int count = 0;
    String totalRemain="";


    public TripsAdapter(List<TripsModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.trips_adapter, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        TripsModal dataAdapterOBJ = dataAdapters.get(position);

        Viewholder.txtDate.setText(dataAdapterOBJ.getDate());
        Viewholder.txtStartAddress.setText(dataAdapterOBJ.getStartAdd());
        Viewholder.txtEndAddress.setText(dataAdapterOBJ.getEndAdd());
        Viewholder.txtName.setText(dataAdapterOBJ.getUsername());

        try {
            Location startPoint = new Location("locationA");
            double startLat = Double.parseDouble(dataAdapterOBJ.getStartLat());
            double startLng = Double.parseDouble(dataAdapterOBJ.getStartLng());
            startPoint.setLatitude(startLat);
            startPoint.setLongitude(startLng);

            double stationLat = Double.parseDouble(dataAdapterOBJ.getEndLat());
            double stationLng = Double.parseDouble(dataAdapterOBJ.getEndLng());
            Location endPoint = new Location("locationB");
            endPoint.setLatitude(stationLat);
            endPoint.setLongitude(stationLng);

            double distance = startPoint.distanceTo(endPoint);
            Log.e("ssffdfdsfds", distance + "");

            String strDistance = String.valueOf(distance);

            if (strDistance.contains(".")) {

                String strSplit[] = strDistance.split("\\.");
                totalRemain = strSplit[0];
            }
            Log.e("sdfsdjfsdkfjsdf", totalRemain);
        }
        catch (NumberFormatException e) {
            e.printStackTrace();
        }


        Viewholder.txtKm.setText(totalRemain+ " " + "km");
        Viewholder.txtpackageType.setText("Package" + " " + dataAdapterOBJ.getTotalkm() + " " + "km" + " " + "in" + " " + dataAdapterOBJ.getHour() + " " + "hour");
        if (position == 0) {
            count = 1;
        }

        SpannableStringBuilder builder = new SpannableStringBuilder();
        String green = "Trip" + "#";
        SpannableString redSpannable = new SpannableString(green);
        redSpannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.green)), 0, green.length(), 0);
        builder.append(redSpannable);

        String black = count + "";
        SpannableString blackSpannable = new SpannableString(black);
        blackSpannable.setSpan(new ForegroundColorSpan(context.getResources().getColor(R.color.black)), 0, blackSpannable.length(), 0);
        builder.append(blackSpannable);

        Viewholder.txtTrip.setText(builder, TextView.BufferType.SPANNABLE);
        count++;

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        private TextView txtTrip;
        private TextView txtDate;
        private TextView txtpackageType;
        private TextView txtName;
        private TextView txtStartAddress;
        private TextView txtEndAddress;
        private TextView txtKm;
        private LinearLayout linearShowAddress;

        public ViewHolder(View itemView) {

            super(itemView);
            txtTrip = itemView.findViewById(R.id.txtTrip);
            txtDate = itemView.findViewById(R.id.txtDate);
            txtpackageType = itemView.findViewById(R.id.txtpackageType);
            txtName = itemView.findViewById(R.id.txtName);
            txtStartAddress = itemView.findViewById(R.id.txtStartAddress);
            txtEndAddress = itemView.findViewById(R.id.txtEndAddress);
            txtKm = itemView.findViewById(R.id.txtKm);
            linearShowAddress = itemView.findViewById(R.id.linearShowAddress);

        }
    }

}