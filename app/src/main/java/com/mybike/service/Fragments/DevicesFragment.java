package com.mybike.service.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.Adapter.DeviceAdapter;
import com.mybike.service.Adapter.VehicleAdapter;
import com.mybike.service.Modals.DeviceModal;
import com.mybike.service.Modals.VehicleModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class DevicesFragment extends Fragment {


    RecyclerView recyclerview1;
    RecyclerView.LayoutManager layoutManager;
    DeviceAdapter deviceAdapter;
    ArrayList<DeviceModal> deviceModals;
    String strUserToken = "";
    TextView txtNotFound;
    SwipeRefreshLayout refreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_devices, container, false);


        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        Log.e("sdfsdfsdfsf", strUserToken);

        refreshLayout = view.findViewById(R.id.refreshLayout);
        txtNotFound = view.findViewById(R.id.txtNotFound);
        recyclerview1 = view.findViewById(R.id.recyclerview1);
        recyclerview1.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerview1.setLayoutManager(layoutManager);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ShowDevicesRefresh();
            }
        });
        ShowDevices();
        return view;
    }


    public void ShowDevices() {


        ProgressDialog progressDialogg = new ProgressDialog(getActivity());
        progressDialogg.setMessage("Loading...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();

        AndroidNetworking.get(HTTPCALL.GetAllDevices)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strUserToken)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("dfsdfsdsfsd", response.toString());
                            deviceModals = new ArrayList<>();
                            if (response.has("deviceCount")) {
                                String strData = response.getString("deviceIds");
                                JSONArray jsonArray = new JSONArray(strData);
                                for (int i = 0; i < jsonArray.length(); i++) {
                                    DeviceModal deviceModal = new DeviceModal();
                                    deviceModal.setId( jsonArray.getString(i));
                                    deviceModals.add(deviceModal);

                                }

                                deviceAdapter = new DeviceAdapter(deviceModals, getActivity());
                                recyclerview1.setAdapter(deviceAdapter);
                                refreshLayout.setRefreshing(false);
                                progressDialogg.dismiss();

                                if (deviceModals.size() == 0) {

                                    txtNotFound.setVisibility(View.VISIBLE);
                                } else {

                                    txtNotFound.setVisibility(View.GONE);
                                }

                            } else {
                                progressDialogg.dismiss();
                                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("gjmhgjfghjdh", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("gjmhgjfghjdh", "Error" + anError.getMessage());
                    }
                });
    }




    /*For refresh*/


    public void ShowDevicesRefresh() {



        AndroidNetworking.get(HTTPCALL.GetAllVehicles)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strUserToken)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("dfsdfsdsfsd", response.toString());
                            deviceModals = new ArrayList<>();
                            if (response.has("status")) {

                                if (response.getString("status").equals("Success")) {

                                    String strData = response.getString("data");
                                    JSONArray jsonArray = new JSONArray(strData);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        DeviceModal deviceModal = new DeviceModal();
                                        deviceModal.setId(jsonObject.getString("vehicleUid"));
                                        deviceModals.add(deviceModal);

                                    }

                                    deviceAdapter = new DeviceAdapter(deviceModals, getActivity());
                                    recyclerview1.setAdapter(deviceAdapter);
                                    refreshLayout.setRefreshing(false);

                                    if (deviceModals.size() == 0) {

                                        txtNotFound.setVisibility(View.VISIBLE);
                                    } else {

                                        txtNotFound.setVisibility(View.GONE);
                                    }


                                } else {

                                    Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }

}