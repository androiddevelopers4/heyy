package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.Manifest;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.budiyev.android.codescanner.CodeScanner;
import com.budiyev.android.codescanner.CodeScannerView;
import com.budiyev.android.codescanner.DecodeCallback;
import com.google.zxing.Result;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONException;
import org.json.JSONObject;

public class ScanQrcodeActivity extends AppCompatActivity {

    private static int SPLASH_TIME_OUT = 1000;
    private static final int PERMISSIONS_REQUEST_READ_CONTACTS = 100;
    String strRideExists = "";
    ProgressDialog progressDialog;
    String strDeviceID = "", strAccountID = "",strUserId="",strPACKAGEID="",strPrice="";
    Dialog dialog;

    private CodeScanner mCodeScanner;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan_qrcode);

        AppsContants.sharedpreferences=getSharedPreferences(AppsContants.MyPREFERENCES,Context.MODE_PRIVATE);
        strUserId=AppsContants.sharedpreferences.getString(AppsContants.UserId,"");
        strPACKAGEID=AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID,"");
        strPrice=AppsContants.sharedpreferences.getString(AppsContants.Price, "");

      /*  new Handler().postDelayed(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
            public void run() {
                //  Intent i1 = new Intent(SplashActivity.this, HomeActivity .class);
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                        checkSelfPermission(Manifest.permission.CAMERA)
                                != PackageManager.PERMISSION_GRANTED) {
                    requestPermissions(new String[]
                                    {Manifest.permission.CAMERA},
                            PERMISSIONS_REQUEST_READ_CONTACTS);
                    //After this point you wait for callback in onRequestPermissionsResult(int, String[], int[]) overriden method
                } else {
                    IntentIntegrator integrator = new IntentIntegrator(ScanQrcodeActivity.this);// Use a specific camera of the device
                    integrator.setOrientationLocked(true);
                    integrator.setBeepEnabled(true);
                    integrator.setCaptureActivity(CaptureActivityPortrait.class);
                    integrator.initiateScan();
                }
            }
        }, SPLASH_TIME_OUT);*/




        CodeScannerView scannerView = findViewById(R.id.scanner_view);
        mCodeScanner = new CodeScanner(this, scannerView);
        mCodeScanner.setDecodeCallback(new DecodeCallback() {
            @Override
            public void onDecoded(@NonNull final Result result) {
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                       // Toast.makeText(ScanQrcodeActivity.this, result.getText(), Toast.LENGTH_SHORT).show();
                        try {

                            //converting the data to json
                            Log.e("fghfghfhfg", result.getText());
                            JSONObject obj = new JSONObject(result.getText());
                            strDeviceID = obj.getString("deviceid");
                            strAccountID = obj.getString("accountId");

                            Log.e("fghfghfhfg", strDeviceID);
                            Log.e("fghfghfhfg", strAccountID);
                            // Toast.makeText(this, strDeviceID, Toast.LENGTH_SHORT).show();

                            getVehicleBookStatus(strDeviceID);

                        } catch (Exception e) {
                            Log.e("fghfghfhfg", e.getMessage());
                        }
                    }
                });
            }
        });
        scannerView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCodeScanner.startPreview();
            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        mCodeScanner.startPreview();
    }

    @Override
    protected void onPause() {
        mCodeScanner.releaseResources();
        super.onPause();
    }


    //Getting the scan results
   /* @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
        if (result != null) {

            if (result.getContents() == null) {
                Toast.makeText(this, "Data Not Found", Toast.LENGTH_LONG).show();
            }

            else {
                //if qr contains data
                try {

                    //converting the data to json
                    Log.e("fghfghfhfg", result.getContents());
                    JSONObject obj = new JSONObject(result.getContents());
                    strDeviceID = obj.getString("deviceid");
                    strAccountID = obj.getString("accountId");

                    Log.e("fghfghfhfg", strDeviceID);
                    Log.e("fghfghfhfg", strAccountID);
                    // Toast.makeText(this, strDeviceID, Toast.LENGTH_SHORT).show();

                    getVehicleBookStatus(strDeviceID);

                } catch (Exception e) {
                    Log.e("fghfghfhfg", e.getMessage());
                }
            }
        } else {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }*/


    public void getVehicleBookStatus(String strDeviceID) {


        progressDialog = new ProgressDialog(ScanQrcodeActivity.this);
        progressDialog.setMessage("Checking Vehicle...");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "vehical_status")
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("package_id", strPACKAGEID)
                .addBodyParameter("vehical_no", strDeviceID)
                .addBodyParameter("status", "1")   /*1-to boook ride - 0 to finish ride*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("message").equals("vehical booked successfully")) {
                                progressDialog.dismiss();

                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.DeviceId, strDeviceID);
                                editor.putString(AppsContants.AccoundId, strAccountID);
                                editor.putString(AppsContants.StationPathSatus, "");
                                editor.putString(AppsContants.StationLat, "");
                                editor.putString(AppsContants.StationLng, "");
                                editor.commit();
                               // startActivity(new Intent(ScanQrcodeActivity.this, OperateVehicleActivity.class));
                                startActivity(new Intent(ScanQrcodeActivity.this, PayNowActivity.class));
                                finish();

                            } else if (response.getString("message").equals("vehical already booked")) {
                                progressDialog.dismiss();

                                dialog = new Dialog(ScanQrcodeActivity.this);
                                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                                dialog.setCancelable(false);
                                dialog.setContentView(R.layout.ride_status_popup);

                                TextView dialog_refund = dialog.findViewById(R.id.dialog_refund);
                                dialog_refund.setText("We have refunded ride price"+" "+strPrice+" "+"INR"+" "+"in your wallet.");

                                Button dialog_scan = dialog.findViewById(R.id.dialog_scan);
                                dialog_scan.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View v) {
                                        dialog.dismiss();
                                        startActivity(new Intent(ScanQrcodeActivity.this,NavigationActivity.class));
                                        finishAffinity();
                                    }
                                });
                                dialog.show();

                            } else {

                                progressDialog.dismiss();
                                Toast.makeText(ScanQrcodeActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }

}