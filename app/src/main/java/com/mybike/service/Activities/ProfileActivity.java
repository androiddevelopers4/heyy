package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;

public class ProfileActivity extends AppCompatActivity {

    TextView sign_login_txt;
    EditText editUserName;
    EditText editMobile;
    EditText editEmail;
    EditText editAdhar_no;
    EditText editLastName;
    Button btnRegister;
    ImageView img_profile;

    private static final String IMAGE_DIRECTORY = "/AppNameHere";
    private int GALLERY = 1, CAMERA = 2;
    File file;
    ProgressDialog progressDialog;
    String strUserId="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
         strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strUserImage = AppsContants.sharedpreferences.getString(AppsContants.ProfileImage, "");
        String strPath = AppsContants.sharedpreferences.getString(AppsContants.Path, "");
        String strFirstName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        String strLastName = AppsContants.sharedpreferences.getString(AppsContants.lname, "");
        String strEmail = AppsContants.sharedpreferences.getString(AppsContants.email, "");
        String strMobile = AppsContants.sharedpreferences.getString(AppsContants.mobile, "");
        String strAdhar = AppsContants.sharedpreferences.getString(AppsContants.AdharNo, "");



        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        editUserName = findViewById(R.id.editUserName);
        editEmail = findViewById(R.id.editEmail);
        editMobile = findViewById(R.id.editMobile);
        editLastName = findViewById(R.id.editLastName);
        btnRegister = findViewById(R.id.btnRegister);
        img_profile = findViewById(R.id.img_profile);

        if(strUserImage.equals("")){
            Glide.with(this).load(R.drawable.profile).into(img_profile);
        }
        else {
            try {
                Glide.with(this).load(strPath+strUserImage).into(img_profile);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        editUserName.setText(strFirstName);
        editLastName.setText(strLastName);
        editEmail.setText(strEmail);
        editMobile.setText(strMobile);


        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strUserName = editUserName.getText().toString().trim();
                String strlastName = editLastName.getText().toString().trim();
                String strEditEmail = editEmail.getText().toString().trim();
                String strMobile = editMobile.getText().toString().trim();
                // startActivity(new Intent(ProfileActivity.this, VerifyOtpActivity.class));


                if (strUserName.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Enter Firstname", Toast.LENGTH_SHORT).show();
                } else if (strlastName.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Enter Lastname", Toast.LENGTH_SHORT).show();
                } else if (strEditEmail.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Enter Email", Toast.LENGTH_SHORT).show();
                } else if (strMobile.equals("")) {
                    Toast.makeText(ProfileActivity.this, "Enter Mobile", Toast.LENGTH_SHORT).show();
                } else {

                    String strData= String.valueOf(file);

                    if(!strData.equals("")||!strData.equals(null)){

                        UpdateWithFile(strUserName, strlastName, strEditEmail, strMobile);
                    }
                    else {
                        UpdatewithoutFile(strUserName, strlastName, strEditEmail, strMobile);
                    }

                    //  startActivity(new Intent(Signup.this,VerifyOtpActivity.class));
                }


            }
        });
    }


    private void showPictureDialog() {
        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }


        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    img_profile.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            img_profile.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }

    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }


        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.e("cdnbchjd", file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return "";
    }


    public void UpdateWithFile(String strUserName, String strlastName, String strEditEmail, String strMobile) {

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.upload(HTTPCALL.BaseUrl)
                .addMultipartParameter("control", "update_profile")
                .addMultipartParameter("userID", strUserId)
                .addMultipartParameter("lname", strlastName)
                .addMultipartParameter("fname", strUserName)
                .addMultipartParameter("email", strEditEmail)
                .addMultipartParameter("mobile", strMobile)
                .addMultipartFile("profile_image", file)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                                progressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response.getString("data"));
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.fname, jsonObject.getString("fname"));
                                editor.putString(AppsContants.lname, jsonObject.getString("lname"));
                                editor.putString(AppsContants.email, jsonObject.getString("email"));
                                editor.putString(AppsContants.mobile, jsonObject.getString("mobile"));
                                editor.putString(AppsContants.Path, jsonObject.getString("path"));
                                editor.putString(AppsContants.ProfileImage, jsonObject.getString("profile_image"));
                                editor.putString(AppsContants.AdharNo, jsonObject.getString("adhar_no"));
                                editor.commit();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(ProfileActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }


    public void UpdatewithoutFile(String strUserName, String strlastName, String strEditEmail, String strMobile) {

        progressDialog = new ProgressDialog(ProfileActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.upload(HTTPCALL.BaseUrl)
                .addMultipartParameter("control", "update_profile")
                .addMultipartParameter("userID", strUserId)
                .addMultipartParameter("lname", strlastName)
                .addMultipartParameter("fname", strUserName)
                .addMultipartParameter("email", strEditEmail)
                .addMultipartParameter("mobile", strMobile)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                                progressDialog.dismiss();
                                JSONObject jsonObject = new JSONObject(response.getString("data"));
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.fname, jsonObject.getString("fname"));
                                editor.putString(AppsContants.lname, jsonObject.getString("lname"));
                                editor.putString(AppsContants.email, jsonObject.getString("email"));
                                editor.putString(AppsContants.mobile, jsonObject.getString("mobile"));
                                editor.putString(AppsContants.Path, jsonObject.getString("path"));
                                editor.putString(AppsContants.ProfileImage, jsonObject.getString("profile_image"));
                                editor.putString(AppsContants.AdharNo, jsonObject.getString("adhar_no"));
                                editor.commit();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(ProfileActivity.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });
    }


}