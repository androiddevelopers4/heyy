package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.TimePicker;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.Adapter.TripsAdapter;
import com.mybike.service.Modals.TripModal;
import com.mybike.service.Modals.TripsModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class MyTripsActivity extends AppCompatActivity {

    RecyclerView recyclerview;
    TripsAdapter categoryAdapter;
    List<TripsModal> categoryModalList;
    TextView txtNotFound;
    String strUserID = "";
    TextView txtDate;
    Calendar myCalendar;
    String strDate="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_trips);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserID = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        strDate = df.format(c);

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        txtNotFound = findViewById(R.id.txtNotFound);
        recyclerview = findViewById(R.id.recyclerview);
        recyclerview.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        recyclerview.setLayoutManager(linearLayoutManager);

        ShowTrips();
        myCalendar = Calendar.getInstance();
        final DatePickerDialog.OnDateSetListener dateTO = new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear,
                                  int dayOfMonth) {
                // TODO Auto-generated method stub
                myCalendar.set(Calendar.YEAR, year);
                myCalendar.set(Calendar.MONTH, monthOfYear);
                myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
                updateToLabel();
            }

        };

        txtDate = findViewById(R.id.txtDate);
        txtDate.setText(strDate);

        RelativeLayout relDate = findViewById(R.id.relDate);
        relDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new DatePickerDialog(MyTripsActivity.this, dateTO, myCalendar
                        .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                        myCalendar.get(Calendar.DAY_OF_MONTH)).show();

            }
        });

    }

    private void updateToLabel() {
        String myFormat = "yyyy-MM-dd"; //In which you need put here
        SimpleDateFormat sdf = new SimpleDateFormat(myFormat, Locale.US);
        strDate = sdf.format(myCalendar.getTime());
        txtDate.setText(strDate);
        ShowTrips();
    }

    public void ShowTrips() {

        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("please wait..");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.show();

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_trip_history")
                .addBodyParameter("date", strDate)
                .addBodyParameter("userID", strUserID)
                .setTag("Trips")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", response.toString());
                        try {
                            categoryModalList = new ArrayList<>();
                            if (response.getString("result").equals("true")) {
                                JSONArray jsonArray = new JSONArray(response.getString("data"));
                                for (int i = 0; i < jsonArray.length(); i++) {

                                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                                    TripsModal categoryModal = new TripsModal();
                                    categoryModal.setStartLat(jsonObject.getString("start_latitude"));
                                    categoryModal.setStartLng(jsonObject.getString("start_longitude"));

                                    categoryModal.setEndLat(jsonObject.getString("end_latitude"));
                                    categoryModal.setEndLng(jsonObject.getString("end_longitude"));
                                    categoryModal.setStartAdd(jsonObject.getString("start_address"));
                                    categoryModal.setEndAdd(jsonObject.getString("end_address"));
                                    categoryModal.setDate(jsonObject.getString("date"));
                                    JSONObject jsonObject1 = new JSONObject(jsonObject.getString("user_info"));
                                    categoryModal.setUsername(jsonObject1.getString("fname") + " " + jsonObject1.getString("lname"));
                                    JSONObject jsonObject2 = new JSONObject(jsonObject.getString("package_info"));
                                   // categoryModal.setTotalkm(jsonObject2.getString("km"));
                                    categoryModal.setHour(jsonObject2.getString("hour"));
                                    categoryModalList.add(categoryModal);

                                }
                            }
                            categoryAdapter = new TripsAdapter(categoryModalList, MyTripsActivity.this);
                            recyclerview.setAdapter(categoryAdapter);
                            progressDialog.dismiss();

                            if (categoryModalList.size() == 0) {
                                txtNotFound.setVisibility(View.VISIBLE);
                            } else {
                                txtNotFound.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            progressDialog.dismiss();
                            Log.e("response", e.getMessage());
                        }

                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialog.dismiss();
                        Log.e("sfsdfsdf", anError.toString());

                    }
                });
    }
}