package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.bumptech.glide.Glide;
import com.google.android.gms.maps.model.LatLng;
import com.mybike.service.Adapter.DropStationsAdapter;
import com.mybike.service.Adapter.TransactionAdapter;
import com.mybike.service.Modals.DropStationsModal;
import com.mybike.service.Modals.TransactionModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class WalletActivity extends AppCompatActivity {

    String myuniqueID = "";
    TextView txtCoins;
    CircleImageView sign_email;
   TextView txtUserName;

    RecyclerView recyclerView;
    ArrayList<TransactionModal> transactionModals;
    TransactionAdapter transactionAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserImage = AppsContants.sharedpreferences.getString(AppsContants.ProfileImage, "");
        String strPath = AppsContants.sharedpreferences.getString(AppsContants.Path, "");
        String strFirstName = AppsContants.sharedpreferences.getString(AppsContants.fname, "");
        String strLastName = AppsContants.sharedpreferences.getString(AppsContants.lname, "");


        txtUserName = findViewById(R.id.txtUserName);
        txtUserName.setText(strFirstName+" "+strLastName);

        sign_email = findViewById(R.id.sign_email);

        if(!strUserImage.equals("")){

            Glide.with(this).load(strPath+strUserImage).into(sign_email);

        }
        else {
            Glide.with(this).load(R.drawable.profile).into(sign_email);
        }

        txtCoins = findViewById(R.id.txtCoins);
        Button btnRecharge = findViewById(R.id.btnRecharge);
        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });
        btnRecharge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.RechargeStatus, "1");
                editor.commit();

                startActivity(new Intent(WalletActivity.this, EnterAmount.class));
            }
        });


        recyclerView=findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        LinearLayoutManager linearLayoutManager=new LinearLayoutManager(this);
        recyclerView.setLayoutManager(linearLayoutManager);

        GetLastTransactions();
    }

    @Override
    protected void onResume() {
        super.onResume();
        ShowCoins();
    }

    public void ShowCoins() {

        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        ProgressDialog progressDialogg = new ProgressDialog(WalletActivity.this);
        progressDialogg.setMessage("Please wait...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_coin")
                .addBodyParameter("user_id",strUserId)
                .setTag("Show Coins")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("dfgdfgdfgd", response.toString());
                            if (response.getString("message").equals("coin showing Successfully")) {
                                JSONObject jsonObject = new JSONObject(response.getString("data"));
                                txtCoins.setText(jsonObject.getString("coin")+" "+"INR");
                                progressDialogg.dismiss();
                            }
                            else {
                                progressDialogg.dismiss();
                                Toast.makeText(WalletActivity.this, "Coins Not Added", Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("dfgdfgdfgd", ex.getMessage());
                        }
                    }
                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("dfgdfgdfgd", anError.toString());
                    }
                });

    }

    public void GetLastTransactions() {
        AppsContants.sharedpreferences =getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "show_transaction")
                .addBodyParameter("userID", strUserId)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("dfsdfsdsfsd", response.toString());
                            JSONArray jsonArray = new JSONArray(response.getString("data"));

                            transactionModals=new ArrayList<>();
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                TransactionModal transactionModal=new TransactionModal();
                                transactionModal.setId(jsonObject.getString("transactionID"));
                                transactionModal.setPrice(jsonObject.getString("amount"));
                                transactionModal.setDate(jsonObject.getString("date"));
                                if(jsonObject.getString("type").equals("0")){

                                    transactionModal.setStatus("Coins Credit");
                                }
                                else {
                                    transactionModal.setStatus("Coins Debit");
                                }
                                transactionModals.add(transactionModal);

                                transactionAdapter=new TransactionAdapter(transactionModals,WalletActivity.this);
                                recyclerView.setAdapter(transactionAdapter);
                            }

                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

    }
}