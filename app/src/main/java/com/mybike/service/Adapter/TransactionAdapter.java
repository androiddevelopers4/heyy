package com.mybike.service.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.mybike.service.Modals.TransactionModal;
import com.mybike.service.R;

import java.util.List;

public class TransactionAdapter extends RecyclerView.Adapter<TransactionAdapter.ViewHolder> {

    Context context;
    List<TransactionModal> dataAdapters;

    public TransactionAdapter(List<TransactionModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;


    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.transaction_modal, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        TransactionModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.txtStatus.setText(dataAdapterOBJ.getStatus());
        Viewholder.txtDate.setText(dataAdapterOBJ.getDate());
        Viewholder.tv_price.setText(dataAdapterOBJ.getPrice()+" "+"coins");
    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView txtStatus;
        TextView txtDate;
        TextView tv_price;
        public ViewHolder(View itemView) {

            super(itemView);
            txtStatus=itemView.findViewById(R.id.txtStatus);
            txtDate=itemView.findViewById(R.id.txtDate);
            tv_price=itemView.findViewById(R.id.tv_price);
        }
    }

}