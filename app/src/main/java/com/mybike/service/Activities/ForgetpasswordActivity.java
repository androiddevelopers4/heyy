package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.Random;

public class ForgetpasswordActivity extends AppCompatActivity {

    LinearLayout linearnewPass;
    LinearLayout linearOTP;
    EditText editMObile, editOTP, editPass;
    TextView txtResend;
    Button btnForget;
    String randomOTP = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgetpassword);

        txtResend = findViewById(R.id.txtResend);
        editPass = findViewById(R.id.editPass);
        editOTP = findViewById(R.id.editOTP);
        editMObile = findViewById(R.id.editMObile);
        linearnewPass = findViewById(R.id.linearnewPass);
        linearOTP = findViewById(R.id.linearOTP);

        ImageView imgBack = findViewById(R.id.imgBack);
        imgBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        TextView txtLogIn = findViewById(R.id.txtLogIn);
        txtLogIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        btnForget = findViewById(R.id.btnForget);
        btnForget.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (btnForget.getText().toString().equals("Generate OTP")) {
                    String strMobile = editMObile.getText().toString();

                    if (strMobile.equals("")) {
                        Toast.makeText(ForgetpasswordActivity.this, "Mobile number is empty", Toast.LENGTH_SHORT).show();
                    } else {

                        GenerateOTP(strMobile);
                    }

                } else {

                    String strMobile = editMObile.getText().toString();
                    String strOTP = editOTP.getText().toString();
                    String strPass = editPass.getText().toString();
                    if(strOTP.equals(randomOTP)){

                        Forgot(strMobile, strOTP, strPass);
                    }
                    else {

                        Toast.makeText(ForgetpasswordActivity.this, "Invalid OTP", Toast.LENGTH_SHORT).show();
                    }


                }
            }
        });

        txtResend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                String strMobile = editMObile.getText().toString();

                if (strMobile.equals("")) {
                    Toast.makeText(ForgetpasswordActivity.this, "Mobile number is empty", Toast.LENGTH_SHORT).show();
                } else {

                    GenerateOTP(strMobile);
                }


            }
        });
    }


    public void GenerateOTP(String strMobile) {

        Random rnd = new Random();
        randomOTP = String.valueOf(rnd.nextInt(999999));

        ProgressDialog progressDialog = new ProgressDialog(ForgetpasswordActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        Log.e("sdfsdfsdf", strMobile);

        JSONObject jsonObjectMain = new JSONObject();
        try {

            JSONArray jsonArrayContent = new JSONArray();

            JSONObject objectContent = new JSONObject();
            objectContent.put("destination", strMobile);
            objectContent.put("source", "PXLSMS");
            objectContent.put("type", "TEXT");
            objectContent.put("content", "OTP to forget password is:"+" "+randomOTP);
            objectContent.put("entityId", "1201161243226206633");
            jsonArrayContent.put(objectContent);
            jsonObjectMain.put("data",jsonArrayContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://vas.sevenomedia.com/domestic/sendsms/jsonapi.php")
                .addHeaders("apiKey","cHRvdXJzOk53a2pydnpY")
                .addJSONObjectBody(jsonObjectMain)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            Log.e("response", response.toString());
                            for (int i = 0; i <response.length() ; i++) {

                                JSONObject jsonObject=response.getJSONObject(i);

                                if(jsonObject.getString("cause").equals("SUCCESS")){
                                    progressDialog.dismiss();
                                    Toast.makeText(ForgetpasswordActivity.this, "OTP send successfully", Toast.LENGTH_SHORT).show();
                                    linearnewPass.setVisibility(View.VISIBLE);
                                    linearOTP.setVisibility(View.VISIBLE);
                                    txtResend.setVisibility(View.VISIBLE);
                                    btnForget.setText("Forgot Password");

                                } else {
                                    progressDialog.dismiss();
                                    linearnewPass.setVisibility(View.GONE);
                                    linearOTP.setVisibility(View.GONE);
                                    txtResend.setVisibility(View.GONE);
                                    btnForget.setText("Generate OTP");
                                    Toast.makeText(ForgetpasswordActivity.this, jsonObject.getString("cause"), Toast.LENGTH_SHORT).show();
                                }

                            }


                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


    public void Forgot(String strMobile, String strOTP, String strPass) {

        ProgressDialog progressDialog = new ProgressDialog(ForgetpasswordActivity.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        Log.e("sdfsdfsdf", strMobile);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "update_psw_otp")
                .addBodyParameter("mobile", strMobile)
                .addBodyParameter("newpsw", strPass)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("message").equals("password Updated Successfully")) {
                                progressDialog.dismiss();
                                Toast.makeText(ForgetpasswordActivity.this, "Password change successfully", Toast.LENGTH_SHORT).show();
                                finish();

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(ForgetpasswordActivity.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }
}