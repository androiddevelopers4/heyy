package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.GPSTracker;
import com.mybike.service.R;
import com.mybike.service.Utils.ApiClient;
import com.mybike.service.Utils.ApiService;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LocationReq;
import com.mybike.service.Utils.LoginResponse;
import com.mybike.service.Utils.SignupReq;
import com.mybike.service.Utils.SignupResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Calendar;
import java.util.Random;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Signup extends AppCompatActivity {
    TextView sign_login_txt;
    EditText editUserName;
    EditText editMobile;
    EditText editEmail;
    EditText editAdhar_no;
    EditText editPassword;
    EditText editLastName;
    Button btnRegister;
    ImageView img_profile;
    GPSTracker gpsTracker;
    double lat;
    double lng;
    RequestQueue requestQueue;
    ProgressDialog progressDialog;
    public static String strUserToken = "";

    private static final String IMAGE_DIRECTORY = "/AppNameHere";
    private int GALLERY = 1, CAMERA = 2;
    File file;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        requestQueue = Volley.newRequestQueue(this);

        sign_login_txt = findViewById(R.id.sign_login_txt);
        sign_login_txt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(Signup.this, LoginActivity.class);
                startActivity(intent);
            }
        });

        editUserName = findViewById(R.id.editUserName);
        editEmail = findViewById(R.id.editEmail);
        editMobile = findViewById(R.id.editMobile);
        editPassword = findViewById(R.id.editPassword);
        editLastName = findViewById(R.id.editLastName);
        btnRegister = findViewById(R.id.btnRegister);
        editAdhar_no = findViewById(R.id.editAdhar_no);
        img_profile = findViewById(R.id.img_profile);


        img_profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPictureDialog();
            }
        });

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strUserName = editUserName.getText().toString().trim();
                String strlastName = editLastName.getText().toString().trim();
                String strEditEmail = editEmail.getText().toString().trim();
                String strMobile = editMobile.getText().toString().trim();
                String strPassword = editPassword.getText().toString().trim();

                if (strUserName.equals("")) {
                    Toast.makeText(Signup.this, "Enter Firstname", Toast.LENGTH_SHORT).show();
                } else if (strlastName.equals("")) {
                    Toast.makeText(Signup.this, "Enter Lastname", Toast.LENGTH_SHORT).show();
                } else if (strEditEmail.equals("")) {
                    Toast.makeText(Signup.this, "Enter Email", Toast.LENGTH_SHORT).show();
                } else if (strMobile.equals("")) {
                    Toast.makeText(Signup.this, "Enter Mobile", Toast.LENGTH_SHORT).show();
                } else if (strPassword.equals("")) {
                    Toast.makeText(Signup.this, "Enter Password", Toast.LENGTH_SHORT).show();
                } else {
                    Register(strUserName, strlastName, strEditEmail, strMobile, strPassword);
                    //  startActivity(new Intent(Signup.this,VerifyOtpActivity.class));
                }


            }
        });
    }

    @Override
    protected void onResume() {
        super.onResume();
        gpsTracker = new GPSTracker(this);
        if (gpsTracker.canGetLocation()) {

            lat = gpsTracker.getLatitude();
            lng = gpsTracker.getLongitude();
        }

    }

    public void Register(String strUserName, String strlastName, String strEditEmail, String strMobile, String strPassword) {

        progressDialog = new ProgressDialog(Signup.this);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "signup")
                .addBodyParameter("lname", strlastName)
                .addBodyParameter("fname", strUserName)
                .addBodyParameter("email", strEditEmail)
                .addBodyParameter("mobile", strMobile)
                .addBodyParameter("password", strPassword)
                .addBodyParameter("adhar_no", editAdhar_no.getText().toString())
               /* .addMultipartFile("profile_image", file)*/
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("result").equals("true")) {
                              //  progressDialog.dismiss();
                               // Toast.makeText(Signup.this, "OTP Send", Toast.LENGTH_SHORT).show();
                              //  Toast.makeText(Signup.this, "Registration Successfull", Toast.LENGTH_SHORT).show();
                                AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.mobile, strMobile);
                                editor.putString(AppsContants.email, strEditEmail);
                                editor.putString(AppsContants.password, strPassword);
                                editor.commit();
                                //finish();
                               // startActivity(new Intent(Signup.this, LoginActivity.class));
                               // startActivity(new Intent(Signup.this, VerifyOtpActivity.class));
                                GenerateOTP(strMobile);

                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(Signup.this, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            Toast.makeText(Signup.this,ex.getMessage(), Toast.LENGTH_SHORT).show();
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Toast.makeText(Signup.this, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });


        /*Volley*/

        /*  StringRequest request = new StringRequest(Request.Method.POST, "https://api-aertrak-india.aeris.com/api/things/accounts/create", new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.e("ewtwertwet", response);
                progressDialog.dismiss();

                   *//* try {
                        Log.e("asdasdasda",response.toString());

                        if(response.has("status")){

                            if(response.getString("status").equals("success")){
                                progressDialog.dismiss();
                                Toast.makeText(Signup.this, "Registration Successfull", Toast.LENGTH_SHORT).show();
                                finish();
                            }
                        }

                        else if(response.has("error")) {
                            progressDialog.dismiss();
                            Toast.makeText(Signup.this, response.getString("message"), Toast.LENGTH_SHORT).show();
                        }

                        else {
                            progressDialog.dismiss();
                            Toast.makeText(Signup.this, "HTTP ERROR", Toast.LENGTH_SHORT).show();
                        }

                    } catch (Exception ex) {
                        progressDialog.dismiss();
                        Log.e("qwewqeq", ex.getMessage());
                    }
*//*


            }
        },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.e("ewtwertwet", error.toString());

                    }
                }) {


            @Override
            protected Map<String, String> getParams() {
                HashMap<String, String> params = new HashMap<String, String>();
                params.put("accountname", "TestUser");
                params.put("parentid", "9007008");
                params.put("firstname", "Test");
                params.put("lastname", "User");
                params.put("loginPref", "EMAIL");
                params.put("email", "Test@gmail.com");
                params.put("phone", "9578696523");
                params.put("password", "Virend123");
                params.put("location", jsonObject.toString());
                params.put("timezone", "Asia/Kolkata");
                return params;
            }

            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                Map<String,String>headers=new HashMap<>();
                headers.put("token", "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJFZ2NOSWk1eWRxSmliakpCWjV5VmxNTW1qMlFzYWMxNkFQbG44TmxWUVc0In0.eyJqdGkiOiIwZmQ3MzllMS1iYmMyLTQyYjEtODQ5My1jYzYxNGM5YzVkZGUiLCJleHAiOjE1OTQ3NDM3MDQsIm5iZiI6MCwiaWF0IjoxNTk0NzE0OTA0LCJpc3MiOiJodHRwczovL2FpcmNlbC1zc28uYWVyY2xvdWRpbi5jb20vYXV0aC9yZWFsbXMvYWVydHJhayIsImF1ZCI6ImZsZWV0LWFlcnRyYWstYXBpIiwic3ViIjoiNzFhMjA2MjQtNzljYS00Yzk0LTliMDEtNDQ3NzQ5MWRlMjgyIiwidHlwIjoiSUQiLCJhenAiOiJmbGVldC1hZXJ0cmFrLWFwaSIsImF1dGhfdGltZSI6MCwic2Vzc2lvbl9zdGF0ZSI6IjVjOTYzMWJiLTU1N2EtNDU3ZS04YTYyLTA0MTEwNDI0NzdkYyIsImFjciI6IjEiLCJ1bml0c1N5c3RlbSI6Ik1FVFJJQyIsInJvbGVzIjoiYWRtaW4iLCJsYW5ndWFnZSI6ImVuIiwicHJlZmVycmVkX3VzZXJuYW1lIjoiOTE5NDI1NjA4MzM1IiwiZ2l2ZW5fbmFtZSI6IkFtaXRhYmgiLCJhYWVhcHAiOiJ7XCJuYW1lXCI6XCJBZXJpc0ZsZWV0TWFuYWdlbWVudFwiLFwiY29udGFpbmVyXCI6XCJmbGVldFN0cmVhbVwifSIsInNlcnZpY2VDb25maWciOiJ7XCJlbmRwb2ludFwiOlwiaHR0cDovL2FlcmNsb3VkLXdzLmFlcmlzLXByb2plY3QubmV0OjMxMjA2L3YxXCIsXCJscEVuZHBvaW50XCI6XCJodHRwczovL2xvbmdwb2xsLWFlcmNsb3VkLXByZXByb2QuYWVyaXNjbG91ZC5jb20vdjFcIixcImFwaUtleVwiOlwiYzRhMzU1OTQtYTk3NC00NDcxLWFmNzMtNmQxYTcwMzE1YTk1XCJ9IiwibmFtZSI6IkFtaXRhYmggQmFqcGFpIiwiYWNjSWQiOiI5MDA3MDA4IiwicmVhbG0iOiJhZXJ0cmFrIiwidGhlbWUiOiIxNTg2MzM0NjEyIiwiZmFtaWx5X25hbWUiOiJCYWpwYWkiLCJncm91cCI6ImZsZWV0In0.CIDxFr2vYq-duDGrG1BfwVUn888NHclXfHckRpFG6q2WfuJMqMKh09KIYwCZm29_KLXgGyFYNq0qEYfyhys0u9cHtskNvKdG3KUai2vBRud2obX5eHrXp-_mY5YqZnBywiOfeZYfOZATNckMcsvpd_jMmMYVBwVani2uZYj2QHhCJHBUq0ws2o7QzTPm1CglWJqcFlUibkGQDrsBYdU1l6YtxYFKs-iW6afrBwHTbB40DgMRLk4L_UQIj8xXz3BZMR2HV-PuNiP3HwqUhP-d308evGEhFwOyMSxclbsiB4Kjr8SrSAzAc0fQSInFn3t_RYQqGKYPS_bm8yNJ64FKIQ");
                headers.put("Accept", "application/json");
                return headers;
            }
        };


        request.setRetryPolicy(new RetryPolicy() {
            @Override
            public int getCurrentTimeout() {
                return 30000;
            }

            @Override
            public int getCurrentRetryCount() {
                return 30000;
            }

            @Override
            public void retry(VolleyError error) throws VolleyError {

            }
        });
        requestQueue.add(request);*/

    }



    public void GenerateOTP(String strMobile) {

        Random rnd = new Random();
        String randomOTP = String.valueOf(rnd.nextInt(9999));

        Log.e("sdfsdfsdf", strMobile);

        JSONObject jsonObjectMain = new JSONObject();
        try {

            JSONArray jsonArrayContent = new JSONArray();

            JSONObject objectContent = new JSONObject();
            objectContent.put("destination", strMobile);
            objectContent.put("source", "PXLSMS");
            objectContent.put("type", "TEXT");
            objectContent.put("content", "OTP for registration is:"+" "+randomOTP);
            objectContent.put("entityId", "1201161243226206633");
            jsonArrayContent.put(objectContent);
            jsonObjectMain.put("data",jsonArrayContent);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        AndroidNetworking.post("http://vas.sevenomedia.com/domestic/sendsms/jsonapi.php")
                .addHeaders("apiKey","cHRvdXJzOk53a2pydnpY")
                .addJSONObjectBody(jsonObjectMain)
                .build()
                .getAsJSONArray(new JSONArrayRequestListener() {
                    @Override
                    public void onResponse(JSONArray response) {
                        try {

                            Log.e("response", response.toString());
                            for (int i = 0; i <response.length() ; i++) {

                                JSONObject jsonObject=response.getJSONObject(i);

                                if(jsonObject.getString("cause").equals("SUCCESS")){

                                    AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                    SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                    editor.putString(AppsContants.OTP, randomOTP);
                                    editor.commit();
                                    progressDialog.dismiss();
                                    Toast.makeText(Signup.this, "Verify your mobile", Toast.LENGTH_SHORT).show();
                                    startActivity(new Intent(Signup.this, VerifyOtpActivity.class));

                                } else {

                                    progressDialog.dismiss();
                                    Toast.makeText(Signup.this, jsonObject.getString("cause"), Toast.LENGTH_SHORT).show();
                                }

                            }


                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }


    private void showPictureDialog() {

        AlertDialog.Builder pictureDialog = new AlertDialog.Builder(this);
        pictureDialog.setTitle("Select Action");
        String[] pictureDialogItems = {
                "Select photo from gallery",
                "Capture photo from camera"};
        pictureDialog.setItems(pictureDialogItems,
                new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        switch (which) {
                            case 0:
                                choosePhotoFromGallary();
                                break;
                            case 1:
                                takePhotoFromCamera();
                                break;
                        }
                    }
                });
        pictureDialog.show();
    }

    public void choosePhotoFromGallary() {
        Intent galleryIntent = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        startActivityForResult(galleryIntent, GALLERY);
    }

    private void takePhotoFromCamera() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(intent, CAMERA);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_CANCELED) {
            return;
        }


        if (requestCode == GALLERY) {
            if (data != null) {
                Uri contentURI = data.getData();
                try {
                    Bitmap bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), contentURI);
                    String path = saveImage(bitmap);
                    Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show();
                    img_profile.setImageBitmap(bitmap);

                } catch (IOException e) {
                    e.printStackTrace();
                    Toast.makeText(this, "Failed!", Toast.LENGTH_SHORT).show();
                }
            }

        } else if (requestCode == CAMERA) {
            Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
            img_profile.setImageBitmap(thumbnail);
            saveImage(thumbnail);
            Toast.makeText(this, "Image Saved!", Toast.LENGTH_SHORT).show();
        }

    }

    public String saveImage(Bitmap myBitmap) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 90, bytes);
        File wallpaperDirectory = new File(
                Environment.getExternalStorageDirectory() + IMAGE_DIRECTORY);

        if (!wallpaperDirectory.exists()) {
            wallpaperDirectory.mkdirs();
        }


        try {
            file = new File(wallpaperDirectory, Calendar.getInstance()
                    .getTimeInMillis() + ".jpg");
            file.createNewFile();
            FileOutputStream fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            MediaScannerConnection.scanFile(this,
                    new String[]{file.getPath()},
                    new String[]{"image/jpeg"}, null);
            fo.close();
            Log.e("cdnbchjd", file.getAbsolutePath());

            return file.getAbsolutePath();
        } catch (IOException e1) {
            e1.printStackTrace();
        }

        return "";
    }
}