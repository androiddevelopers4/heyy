package com.mybike.service.Modals;

public class ShowVehicleDetailsModel {
    public String getAltitude() {
        return altitude;
    }

    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    public String getAssetId() {
        return assetId;
    }

    public void setAssetId(String assetId) {
        this.assetId = assetId;
    }

    public String getAssetState() {
        return assetState;
    }

    public void setAssetState(String assetState) {
        this.assetState = assetState;
    }

    public String getAssetType() {
        return assetType;
    }

    public void setAssetType(String assetType) {
        this.assetType = assetType;
    }

    public String getAssetUid() {
        return assetUid;
    }

    public void setAssetUid(String assetUid) {
        this.assetUid = assetUid;
    }

    public String getCarrier() {
        return carrier;
    }

    public void setCarrier(String carrier) {
        this.carrier = carrier;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }

    public String getDevicePowerVoltage() {
        return devicePowerVoltage;
    }

    public void setDevicePowerVoltage(String devicePowerVoltage) {
        this.devicePowerVoltage = devicePowerVoltage;
    }

    public String getLatitude() {
        return latitude;
    }

    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public String getLongitude() {
        return longitude;
    }

    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    public String getVehicleBatteryVoltage() {
        return vehicleBatteryVoltage;
    }

    public void setVehicleBatteryVoltage(String vehicleBatteryVoltage) {
        this.vehicleBatteryVoltage = vehicleBatteryVoltage;
    }

    public String getVehicleStatusReason() {
        return vehicleStatusReason;
    }

    public void setVehicleStatusReason(String vehicleStatusReason) {
        this.vehicleStatusReason = vehicleStatusReason;
    }

    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    String altitude,assetId,assetState,assetType,assetUid,carrier,correlationId,dateOfBirth,deviceId,devicePowerVoltage,latitude,licensePlate,longitude,model,name,speed,vehicleBatteryVoltage,vehicleStatusReason,year;
}