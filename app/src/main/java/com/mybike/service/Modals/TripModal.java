package com.mybike.service.Modals;

import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

public class TripModal {





    private String user_id;
    private String rnd_id;
    private String date;
    private String tripId;
    private String totalkmVehicle;
    private String tripAerisID;
    private String lat;
    private String lng;


    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getRnd_id() {
        return rnd_id;
    }

    public void setRnd_id(String rnd_id) {
        this.rnd_id = rnd_id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getTripAerisID() {
        return tripAerisID;
    }

    public void setTripAerisID(String tripAerisID) {
        this.tripAerisID = tripAerisID;
    }


    public String getTripId() {
        return tripId;
    }

    public void setTripId(String tripId) {
        this.tripId = tripId;
    }

    public String getTotalkmVehicle() {
        return totalkmVehicle;
    }

    public void setTotalkmVehicle(String totalkmVehicle) {
        this.totalkmVehicle = totalkmVehicle;
    }
}