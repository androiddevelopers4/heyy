package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.DirectionsJSONParser;
import com.mybike.service.Utils.GPSTracker;

import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class DrawPathVehicle extends AppCompatActivity implements OnMapReadyCallback{


    String strPickPointVehicleLat = "";
    String strPickPointVehicleLng = "";
    String strArrangePointVehicleLat = "";
    String strArrangePointVehicleLng = "";
    String strArrangeVehicleStatus = "";
    String strStationPathSatus = "";
    Button btnProceed;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw_path_vehicle);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strStationPathSatus = AppsContants.sharedpreferences.getString(AppsContants.StationPathSatus, "");
        if(strStationPathSatus.equals("1")){
            AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
            editor.putString(AppsContants.PickPointVehicleLat, AppsContants.sharedpreferences.getString(AppsContants.StationLat, ""));
            editor.putString(AppsContants.PickPointVehicleLng, AppsContants.sharedpreferences.getString(AppsContants.StationLng, ""));
            editor.commit();
        }

        strPickPointVehicleLat = AppsContants.sharedpreferences.getString(AppsContants.PickPointVehicleLat, "");
        strPickPointVehicleLng = AppsContants.sharedpreferences.getString(AppsContants.PickPointVehicleLng, "");

        strArrangePointVehicleLat = AppsContants.sharedpreferences.getString(AppsContants.ARRANGE_VEHICLE_LAT, "");
        strArrangePointVehicleLng = AppsContants.sharedpreferences.getString(AppsContants.ARRANGE_VEHICLE_LNG, "");

        Log.e("dgfdfgdfgdfdfg",strPickPointVehicleLat);
        Log.e("dgfdfgdfgdfdfg",strPickPointVehicleLng);
        Log.e("dgfdfgdfgdfdfg",strArrangePointVehicleLat);
        Log.e("dgfdfgdfgdfdfg",strArrangePointVehicleLng);

        strArrangeVehicleStatus = AppsContants.sharedpreferences.getString(AppsContants.ARRANGE_VEHICLE_CHECKBOX_STATUS, "");
        Log.e("dgdfgdfgdf", strArrangeVehicleStatus);

        RelativeLayout relBottomLocation = findViewById(R.id.relBottomLocation);
        if(strStationPathSatus.equals("1")){
            relBottomLocation.setVisibility(View.GONE);
        }
        else {
            relBottomLocation.setVisibility(View.VISIBLE);
        }
        ImageView backImage = findViewById(R.id.backImage);
        backImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                finish();
            }
        });

        btnProceed = findViewById(R.id.btnProceed);
        btnProceed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

               startActivity(new Intent(DrawPathVehicle.this,ScanQrcodeActivity.class));
                //startActivity(new Intent(DrawPathVehicle.this,OperateVehicleActivity.class));
                finish();
            }
        });

        SupportMapFragment map = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        map.getMapAsync(this);

    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                this, R.raw.maps_style);
        googleMap.setMapStyle(style);
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setCompassEnabled(false);

        DrawPath();
    }
    public void DrawPath(){
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.25); // offset from edges of the map 10% of screen

        Log.e("dfgdgdgdgf","dedsfdfds"+strArrangePointVehicleLat);
        Log.e("dfgdgdgdgf","dedsfdfds"+strArrangePointVehicleLng);

        double sLat= Double.parseDouble(strArrangePointVehicleLat);
        double sLng= Double.parseDouble(strArrangePointVehicleLng);
        LatLng s_latLng = null;
        if(strArrangeVehicleStatus.equals("true")){

            s_latLng=new LatLng(sLat,sLng);
        }
        else {
            GPSTracker gpsTracker = new GPSTracker(this);
           s_latLng=new LatLng(gpsTracker.getLatitude(),gpsTracker.getLongitude());

        }
        if (s_latLng != null) {

            int heights = 110;
            int widths = 80;
            BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.marker2);
            Bitmap bs = bitmapdraws.getBitmap();

            Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(s_latLng)
                    .title("User Location")
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers)));
            builder.include(marker.getPosition());
        }
        double dLat= Double.parseDouble(strPickPointVehicleLat);
        double dLng= Double.parseDouble(strArrangePointVehicleLng);
        LatLng d_latLng = new LatLng(dLat, dLng);

        if (d_latLng != null) {

            int heights = 100;
            int widths = 90;
            Bitmap bs;
            String Title="";
            if(strStationPathSatus.equals("1")) {
                BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.ddd);
                bs = bitmapdraws.getBitmap();
                Title="Station";
            }

            else {
                 BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.scooter);
                 bs = bitmapdraws.getBitmap();
                Title="Vehicle";
            }

            Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
            Marker marker = mMap.addMarker(new MarkerOptions()
                    .position(d_latLng)
                    .title(Title)
                    .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))
            );

            builder.include(marker.getPosition());
        }

        if (s_latLng != null && d_latLng != null) {
            LatLngBounds bounds = builder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
            mMap.animateCamera(cu);
            String url = getDirectionsUrl(s_latLng, d_latLng);
            DownloadTask downloadTask = new DownloadTask();
            downloadTask.execute(url);

            mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(s_latLng, 18F));
        } else{

            Toast.makeText(DrawPathVehicle.this, "Enter Source and Destination Address", Toast.LENGTH_SHORT).show();
        }


    }

    private String getDirectionsUrl(LatLng origin, LatLng dest) {
        String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        String sensor = "sensor=false";
        String parameters = str_origin + "&" + str_dest + "&" + sensor;
        String output = "json";
        // String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyClkiUQxIeczQEjTrw-dy9h2fj9qjXDk_4";
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + "AIzaSyAURsUjEy5yIa7CTgW_vyn8f5L7iKXz5DI";
        return url;
    }

    private class DownloadTask extends AsyncTask<String, Void, String> {
        @Override
        protected String doInBackground(String... url) {
            String data = "";
            try {
                data = downloadUrl(url[0]);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);
            ParserTask parserTask = new ParserTask();
            parserTask.execute(result);
        }
    }

    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;
            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();
                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList<LatLng> points = null;
            PolylineOptions lineOptions = null;
            try {
                for (int i = 0; i < result.size(); i++) {
                    points = new ArrayList<LatLng>();
                    lineOptions = new PolylineOptions();
                    List<HashMap<String, String>> path = result.get(i);
                    for (int j = 0; j < path.size(); j++) {
                        HashMap<String, String> point = path.get(j);
                        double lat = Double.parseDouble(point.get("lat"));
                        double lng = Double.parseDouble(point.get("lng"));
                        LatLng position = new LatLng(lat, lng);
                        points.add(position);
                    }
                    lineOptions.addAll(points);
                    lineOptions.width(8);
                    lineOptions.color(R.color.red);
                    mMap.addPolyline(lineOptions);
                }
            } catch (Exception ex) {
                Log.e("dfgdfgdgd",ex.getMessage());
            }
        }
    }


    @SuppressLint("LongLogTag")
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);
            urlConnection = (HttpURLConnection) url.openConnection();
            urlConnection.connect();
            iStream = urlConnection.getInputStream();
            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));
            StringBuffer sb = new StringBuffer();
            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            data = sb.toString();
            br.close();
        } catch (Exception e) {
            Log.d("Exception while downloading url", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }

}