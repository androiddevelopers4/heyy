package com.mybike.service.Fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONArrayRequestListener;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.Adapter.VehicleAdapter;
import com.mybike.service.Modals.VehicleModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.Utils.LatLngInterpolator;
import com.mybike.service.Utils.MarkerAnimation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class VehiclesFragment extends Fragment {


    RecyclerView recyclerview1;
    RecyclerView.LayoutManager layoutManager;
    VehicleAdapter vehicleAdapter;
    ArrayList<VehicleModal> vehicleModals;
    String strUserToken = "";
    TextView txtNotFound;
    SwipeRefreshLayout refreshLayout;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_vehicles, container, false);

        AppsContants.sharedpreferences = getActivity().getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        Log.e("sdfsdfsdfsf", strUserToken);

        refreshLayout = view.findViewById(R.id.refreshLayout);
        txtNotFound = view.findViewById(R.id.txtNotFound);
        recyclerview1 = view.findViewById(R.id.recyclerview1);
        recyclerview1.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(getActivity());
        recyclerview1.setLayoutManager(layoutManager);

        refreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                ShowVehiclesRefresh();
            }
        });
        ShowVehicles();
        return view;
    }


    public void ShowVehicles() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dataFlag", "LOCATION");
            jsonObject.put("count", true);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        ProgressDialog progressDialogg = new ProgressDialog(getActivity());
        progressDialogg.setMessage("Loading...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();

        AndroidNetworking.post(HTTPCALL.GetAllVehicles)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strUserToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("dfsdfsdsfsd", response.toString());
                            vehicleModals= new ArrayList<>();
                            if (response.has("status")) {

                                if (response.getString("status").equals("Success")) {

                                    String strData = response.getString("data");
                                    JSONArray jsonArray = new JSONArray(strData);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        VehicleModal vehicleModal = new VehicleModal();
                                        vehicleModal.setId(jsonObject.getString("vehicleUid"));
                                        vehicleModal.setStatus(jsonObject.getString("deviceStatus"));
                                        vehicleModal.setAccount(jsonObject.getString("accountId"));
                                        vehicleModal.setName(jsonObject.getString("vehicleName"));
                                        vehicleModal.setMake(jsonObject.getString("make"));
                                        vehicleModal.setModal(jsonObject.getString("model"));
                                        vehicleModal.setYear(jsonObject.getString("year"));
                                        String strVehicleSpeed=jsonObject.getString("vehicleSpeed");
                                        JSONArray jsonArray1=new JSONArray(strVehicleSpeed);
                                        for (int j = 0; j <jsonArray1.length() ; j++) {

                                            JSONObject jsonObject1=jsonArray1.getJSONObject(j);

                                           if(j==0){

                                               vehicleModal.setSpeed(jsonObject1.getString("value"));
                                           }

                                        }
                                        vehicleModals.add(vehicleModal);
                                    }

                                    vehicleAdapter = new VehicleAdapter(vehicleModals, getActivity());
                                    recyclerview1.setAdapter(vehicleAdapter);
                                    progressDialogg.dismiss();
                                    refreshLayout.setRefreshing(false);

                                    if(vehicleModals.size()==0){

                                        txtNotFound.setVisibility(View.VISIBLE);
                                    }
                                    else {

                                        txtNotFound.setVisibility(View.GONE);
                                    }


                                } else {

                                    progressDialogg.dismiss();
                                    Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                progressDialogg.dismiss();
                                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }




    /*For refresh*/


    public void ShowVehiclesRefresh() {

        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("dataFlag", "LOCATION");
            jsonObject.put("count", true);
            Log.e("DFgdfgdfgf", jsonObject.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }



        AndroidNetworking.post(HTTPCALL.GetAllVehicles)
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strUserToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.e("dfsdfsdsfsd", response.toString());
                            vehicleModals= new ArrayList<>();
                            if (response.has("status")) {

                                if (response.getString("status").equals("Success")) {

                                    String strData = response.getString("data");
                                    JSONArray jsonArray = new JSONArray(strData);
                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject jsonObject = jsonArray.getJSONObject(i);
                                        VehicleModal vehicleModal = new VehicleModal();
                                        vehicleModal.setId(jsonObject.getString("vehicleUid"));
                                        vehicleModal.setStatus(jsonObject.getString("deviceStatus"));
                                        vehicleModal.setAccount(jsonObject.getString("accountId"));
                                        vehicleModal.setName(jsonObject.getString("vehicleName"));
                                        vehicleModal.setMake(jsonObject.getString("make"));
                                        vehicleModal.setModal(jsonObject.getString("model"));
                                        vehicleModal.setYear(jsonObject.getString("year"));
                                        String strVehicleSpeed=jsonObject.getString("vehicleSpeed");
                                        JSONArray jsonArray1=new JSONArray(strVehicleSpeed);
                                        for (int j = 0; j <jsonArray1.length() ; j++) {

                                            JSONObject jsonObject1=jsonArray1.getJSONObject(j);

                                            if(j==0){

                                                vehicleModal.setSpeed(jsonObject1.getString("value"));
                                            }

                                        }
                                        vehicleModals.add(vehicleModal);
                                    }

                                    vehicleAdapter = new VehicleAdapter(vehicleModals, getActivity());
                                    recyclerview1.setAdapter(vehicleAdapter);
                                    refreshLayout.setRefreshing(false);

                                    if(vehicleModals.size()==0){

                                        txtNotFound.setVisibility(View.VISIBLE);
                                    }
                                    else {

                                        txtNotFound.setVisibility(View.GONE);
                                    }


                                } else {

                                    Toast.makeText(getActivity(), response.getString("status"), Toast.LENGTH_SHORT).show();
                                }
                            } else {

                                Toast.makeText(getActivity(), "Server Error", Toast.LENGTH_SHORT).show();
                            }


                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }
}