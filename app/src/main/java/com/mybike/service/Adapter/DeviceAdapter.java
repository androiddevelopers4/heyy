package com.mybike.service.Adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.mybike.service.Modals.DeviceModal;
import com.mybike.service.Modals.VehicleModal;
import com.mybike.service.R;

import java.util.List;

public class DeviceAdapter extends RecyclerView.Adapter<DeviceAdapter.ViewHolder> {

    Context context;

    List<DeviceModal> dataAdapters;

    public DeviceAdapter(List<DeviceModal> getDataAdapter, Context context){

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.device_adapter, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, int position) {

        DeviceModal dataAdapterOBJ =  dataAdapters.get(position);

        Viewholder.txtDeviceId.setText(dataAdapterOBJ.getId());
        Viewholder.cardview1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



            }
        });


    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        public TextView txtDeviceId;
        public CardView cardview1;

        public ViewHolder(View itemView) {

            super(itemView);

            txtDeviceId =itemView.findViewById(R.id.txtDeviceId);
            cardview1 =itemView.findViewById(R.id.cardview1);

        }
    }
}
