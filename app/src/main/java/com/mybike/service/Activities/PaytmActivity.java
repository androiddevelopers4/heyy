package com.mybike.service.Activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;
import com.paytm.pgsdk.PaytmOrder;
import com.paytm.pgsdk.PaytmPGService;
import com.paytm.pgsdk.PaytmPaymentTransactionCallback;
import com.paytm.pgsdk.TransactionManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class PaytmActivity extends AppCompatActivity {
    String orderId = "", mid = "";
    String strUserId = "", strPrice = "";
    String strUserToken = "", VehicleOnOffStatus = "";
    String myuniqueID = "";
    String txnTokenString = "";
    private Integer ActivityRequestCode = 2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_paytm);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserToken = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        VehicleOnOffStatus = AppsContants.sharedpreferences.getString(AppsContants.VehicleOnOffStatus, "");
        strPrice = AppsContants.sharedpreferences.getString(AppsContants.Price, "");

        int i = new Random().nextInt(900000) + 100000;
        orderId = String.valueOf(i);
        Log.e("eteeteete", orderId);
        Log.e("sdfsdfsdfsdfsdfsd", strPrice);
       // strPrice="1";
        // mid = "pN3WACuUJcKRc1Dd"; /// Heyy marchant key
       // mid = "Progre88586068687381"; /// Heyy marchant Id stage
        mid = "Progre07925142612520"; /// Heyy marchant Id live
       // mid = "IGINTE44196006040779"; /// Ig fruit  marchant Id


        myuniqueID = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        Log.e("dfgdfgdfgdfg", myuniqueID);
        Log.e("eteeteete", orderId);

        AndroidNetworking.post("https://heyy.co.in/paytmallinone/init_Transaction.php")
                .addBodyParameter("ORDER_ID", orderId)
                .addBodyParameter("code", "12345")
                .addBodyParameter("AMOUNT", strPrice)
                .addBodyParameter("MID", mid)
                .setTag("Token")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("eteeteete", response.toString());
                            JSONObject jsonObject=response.getJSONObject("body");
                            startPaytmPayment(jsonObject.getString("txnToken"));

                        } catch (Exception ex) {
                            Log.e("dfgdfgdfgd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("dfgdfgdfgd", anError.toString());
                    }
                });

    }


    public void startPaytmPayment (String token){

        txnTokenString = token;
        Log.e("sgsdgdsdgsg", "Token "+txnTokenString);
        // for test mode use it
       // String host = "https://securegw-stage.paytm.in/";

        // for production mode use it
       // String host = "https://securegw.paytm.in/";

      /*  String orderDetails = "MID: " + mid + ", OrderId: " + orderId + ", TxnToken: " + txnTokenString
                + ", Amount: " + strPrice;*/

        String callBackUrl ="https://securegw.paytm.in/theia/paytmCallback?ORDER_ID="+orderId;
       // Log.e("sgsdgdsdgsg", " callback URL "+callBackUrl);

        PaytmOrder paytmOrder = new PaytmOrder(orderId, mid, txnTokenString, strPrice, callBackUrl);
        TransactionManager transactionManager = new TransactionManager(paytmOrder, new PaytmPaymentTransactionCallback(){
            @Override
            public void onTransactionResponse(Bundle bundle) {
                Log.e("sgsdgdsdgsg", "Response (onTransactionResponse) : "+bundle.toString());
                if (bundle.toString().contains("TXN_FAILURE")) {
                    Log.e("ieutyiwehif", "Failure");
                    Toast.makeText(PaytmActivity.this, "Payment Failed", Toast.LENGTH_SHORT).show();
                    finish();
                } else {
                    Log.e("ieutyiwehif", "DONE");
                    // ImmobilizeVehicle();
                  //  AddCoins();
           /* Toast.makeText(this, "Payment Successful", Toast.LENGTH_SHORT).show();
            startActivity(new Intent(PaytmActivity.this,ScanQrcodeActivity.class));
            finish();*/
                }
            }

            @Override
            public void networkNotAvailable() {
                Log.e("sgsdgdsdgsg", "network not available ");
            }

            @Override
            public void onErrorProceed(String s) {
                Log.e("sgsdgdsdgsg", " onErrorProcess "+s.toString());
            }

            @Override
            public void clientAuthenticationFailed(String s) {
                Log.e("sgsdgdsdgsg", "Clientauth "+s);
            }

            @Override
            public void someUIErrorOccurred(String s) {
                Log.e("sgsdgdsdgsg", " UI error "+s);
            }

            @Override
            public void onErrorLoadingWebPage(int i, String s, String s1) {
                Log.e("TAG", " error loading web "+s+"--"+s1);
            }

            @Override
            public void onBackPressedCancelTransaction() {
                Log.e("TAG", "backPress ");
                finish();
            }

            @Override
            public void onTransactionCancel(String s, Bundle bundle) {
                Log.e("TAG", " transaction cancel "+s);
                finish();
            }
        });

        transactionManager.setShowPaymentUrl("https://securegw.paytm.in/theia/api/v1/showPaymentPage");
        transactionManager.startTransaction(this, ActivityRequestCode);

    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Log.e("TAG" ," result code "+resultCode);
        // -1 means successful  // 0 means failed
        // one error is - nativeSdkForMerchantMessage : networkError

        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == ActivityRequestCode && data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                for (String key : bundle.keySet()) {
                    Log.e("TAG", key + " : " + (bundle.get(key) != null ? bundle.get(key) : "NULL"));
                }
            }
            Log.e("TAG", " data "+  data.getStringExtra("nativeSdkForMerchantMessage"));
            Log.e("TAG", " data response - "+data.getStringExtra("response"));

            Log.e("hrwwrhwrhw",data.getStringExtra("response"));
            try {
                JSONObject jsonObject=new JSONObject(data.getStringExtra("response"));
                if(jsonObject.getString("STATUS").equals("TXN_SUCCESS")){

                  //  Toast.makeText(this, "Payment Successfull", Toast.LENGTH_SHORT).show();
                    AddCoins();

                }
                else {
                    Toast.makeText(this, "Payment failed please tray again", Toast.LENGTH_SHORT).show();
                    finish();

                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            Toast.makeText(this, "Payment failed please tray again", Toast.LENGTH_SHORT).show();
            finish();
        }
    }


    public void AddCoins() {

        AppsContants.sharedpreferences = getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
        String strRechargeStatus = AppsContants.sharedpreferences.getString(AppsContants.RechargeStatus, "");


        ProgressDialog progressDialogg = new ProgressDialog(PaytmActivity.this);
        progressDialogg.setMessage("Recharging wallet...");
        progressDialogg.setIndeterminate(true);
        progressDialogg.setCancelable(false);
        progressDialogg.show();
        Log.e("dfgdfgdfgdfg", strPrice);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_coin")
                .addBodyParameter("user_id", strUserId)
                .addBodyParameter("coin", strPrice)
                .setTag("Add Coins")
                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {

                        try {
                            Log.e("dfgdfgdfgd", response.toString());
                            if (response.getString("message").equals("coin Added Successfully")) {
                                progressDialogg.dismiss();
                                Toast.makeText(PaytmActivity.this, strPrice + " " + "Coins added in your wallet", Toast.LENGTH_SHORT).show();

                                if (strRechargeStatus.equals("1")) {
                                    startActivity(new Intent(PaytmActivity.this, WalletActivity.class));
                                    finish();
                                } else {
                                    startActivity(new Intent(PaytmActivity.this, PayNowActivity.class));
                                    finish();
                                }

                            } else {

                                progressDialogg.dismiss();
                                Toast.makeText(PaytmActivity.this, "Coins Not Added", Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialogg.dismiss();
                            Log.e("dfgdfgdfgd", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        progressDialogg.dismiss();
                        Log.e("dfgdfgdfgd", anError.toString());
                    }
                });

    }

}