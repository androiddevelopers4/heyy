package com.mybike.service.Adapter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.Activities.OperateVehicleActivity;
import com.mybike.service.Activities.PricingListActivity;
import com.mybike.service.Modals.PriceListModel;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;

import org.json.JSONObject;

import java.util.List;
import java.util.Random;

import static com.mybike.service.Utils.HTTPCALL.BaseUrl;

public class ShowPriceAdapter extends RecyclerView.Adapter<ShowPriceAdapter.ViewHolder> {

    Context context;
    FragmentManager fragmentManager;
    List<PriceListModel> dataAdapters;
    String strRechargeStatus="";

    public ShowPriceAdapter(List<PriceListModel> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
        this.fragmentManager = fragmentManager;

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.price_list, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);
        AppsContants.sharedpreferences =context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        strRechargeStatus = AppsContants.sharedpreferences.getString(AppsContants.RechargeStatus, "");

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        PriceListModel dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.tv1.setText(dataAdapterOBJ.getHOUR() + " " + "hrs/" + dataAdapterOBJ.getMINUTE() + " " + "km");
        Viewholder.tv_price.setText("INR " + dataAdapterOBJ.getPRICE());
        Viewholder.relContents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                Random rnd = new Random();
                int number = rnd.nextInt(999999);
                Log.e("dfgdfgdg",number+"");

                AppsContants.sharedpreferences =context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.RideId,number+"");
                editor.putString(AppsContants.PACKAGEID,dataAdapterOBJ.getID());
                editor.putString(AppsContants.Price,dataAdapterOBJ.getPRICE());
                editor.putString(AppsContants.USER_SELECTED_KM, dataAdapterOBJ.getMINUTE());
               // editor.putString(AppsContants.USER_SELECTED_KM, "3");
                editor.putString(AppsContants.USER_SELECTED_HOUR, dataAdapterOBJ.getHOUR() + " " + "hrs/" + dataAdapterOBJ.getMINUTE() + " " + "km");
                editor.apply();
                PricingListActivity.rl_next.setEnabled(true);
                PricingListActivity.rl_next.setBackgroundColor(Color.parseColor("#1FC326"));
                PricingListActivity.rl_next.setText("Proceed With INR " +dataAdapterOBJ.getPRICE());
                PricingListActivity.strHour=dataAdapterOBJ.getHOUR();

            }
        });

    }

    @Override
    public int getItemCount() {

        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView tv1, tv_price;
        RelativeLayout relContents;

        public ViewHolder(View itemView) {

            super(itemView);
            tv1 = itemView.findViewById(R.id.tv1);
            tv_price = itemView.findViewById(R.id.tv_price);
            relContents = itemView.findViewById(R.id.relContents);

        }
    }




}