package com.mybike.service.Adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.mybike.service.Activities.DrawPathVehicle;
import com.mybike.service.Activities.OperateVehicleActivity;
import com.mybike.service.Activities.VehicleDetailActivity;
import com.mybike.service.Modals.DropStationsModal;
import com.mybike.service.R;
import com.mybike.service.Utils.AppsContants;
import com.mybike.service.Utils.HTTPCALL;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

public class DropStationsAdapter extends RecyclerView.Adapter<DropStationsAdapter.ViewHolder> {


    Context context;
    List<DropStationsModal> dataAdapters;

    public DropStationsAdapter(List<DropStationsModal> getDataAdapter, Context context) {

        super();
        this.dataAdapters = getDataAdapter;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.drop_vehicle_station, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder Viewholder, final int position) {

        DropStationsModal dataAdapterOBJ = dataAdapters.get(position);
        Viewholder.txtStationName.setText(dataAdapterOBJ.getName());
        Viewholder.relArrangeHere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {



                OperateVehicleActivity.handlerKm.removeCallbacks(OperateVehicleActivity.runnableKm);
                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                editor.putString(AppsContants.StationPathSatus, "1");
                editor.putString(AppsContants.StationLat, dataAdapterOBJ.getLat());
                editor.putString(AppsContants.StationLng, dataAdapterOBJ.getLng());
                editor.putString(AppsContants.ARRANGE_VEHICLE_CHECKBOX_STATUS, "false");
                editor.commit();
                context.startActivity(new Intent(context, OperateVehicleActivity.class));

               /* final Dialog dialog = new Dialog(context);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.arrange_vehicle_popup);

                Button btnPay = (Button) dialog.findViewById(R.id.btnPay);
                btnPay.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {

                        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                        String USER_ID = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");
                        deductCoins(USER_ID);
                        dialog.dismiss();
                    }
                });

                dialog.show();
*/
            }
        });
    }

    @Override
    public int getItemCount() {
        return dataAdapters.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        TextView txtStationName;
        RelativeLayout relArrangeHere;

        public ViewHolder(View itemView) {

            super(itemView);

            txtStationName = itemView.findViewById(R.id.txtStationName);
            relArrangeHere = itemView.findViewById(R.id.relArrangeHere);

        }
    }


    public void deductCoins(String user_id) {
        Log.e("deductCoinsfdfd", "" + user_id);
        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);
        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "deduct_coin")
                .addBodyParameter("user_id", user_id)
                .addBodyParameter("coin", "50")

                .setPriority(Priority.HIGH)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        Log.e("response", "" + response);
                        progressDialog.dismiss();
                        try {
                            if (response.has("result")) {
                                if (response.getString("result").equals("true")) {
                                    EndRideMaually();


                                } else {
                                    Toast.makeText(context, response.getString("message"), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            progressDialog.dismiss();
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("anError", "" + anError);
                        progressDialog.dismiss();
                    }
                });
    }


    public void EndRideMaually() {

        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strParentToken = AppsContants.sharedpreferences.getString(AppsContants.Maintoken, "");
        String strDeviceId = AppsContants.sharedpreferences.getString(AppsContants.DeviceId, "");
        String strAccoundId = AppsContants.sharedpreferences.getString(AppsContants.AccoundId, "");
        String strAssetId = AppsContants.sharedpreferences.getString(AppsContants.AssetId, "");
        JSONObject jsonObject = new JSONObject();

        try {

            jsonObject.put("deviceId", strDeviceId);
            jsonObject.put("request", "REMOTE_IGNITION_OFF");
            jsonObject.put("accountId", strAccoundId);
            Log.e("DFgdfgdfgf", jsonObject.toString());

        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*suryanbajpai33@gmail.com      pass- Manirock33*/

        ProgressDialog commandDialog = new ProgressDialog(context);
        commandDialog.setMessage("Ignition off..");
        commandDialog.setIndeterminate(true);
        commandDialog.setCancelable(false);
        commandDialog.show();
        /* bfb4f730-dae7-11ea-8e74-7defd70ba752 */

        AndroidNetworking.post("https://api-aertrak-india.aeris.com/v1.0/api/things/assets/" + strAssetId + "/command")
                .addHeaders("Content-Type", "application/json")
                .addHeaders("token", strParentToken)
                .addJSONObjectBody(jsonObject)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("degkusdhgksdgf", response.toString());
                            if (response.has("response")) {
                                if (response.getString("response").equals("Command has been accepted successfully")) {
                                    commandDialog.dismiss();

                                    AddTripDetail();

                                } else {
                                    commandDialog.dismiss();
                                    Toast.makeText(context, "Not Accepted", Toast.LENGTH_SHORT).show();
                                }

                            } else {

                                commandDialog.dismiss();
                                Toast.makeText(context, response.getString("response"), Toast.LENGTH_SHORT).show();
                            }
                        } catch (Exception ex) {

                            commandDialog.dismiss();
                            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            Log.e("degkusdhgksdgf", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        commandDialog.dismiss();
                        Toast.makeText(context, anError.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });
    }


    public void AddTripDetail() {

        AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
        String strFLat = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLAT, "");
        String strFLng = AppsContants.sharedpreferences.getString(AppsContants.FIRSTLNG, "");
        String strEndLat = AppsContants.sharedpreferences.getString(AppsContants.LASTLAT, "");
        String strEndLng = AppsContants.sharedpreferences.getString(AppsContants.LASTLNG, "");
        String strPackageID = AppsContants.sharedpreferences.getString(AppsContants.PACKAGEID, "");
        String strUserId = AppsContants.sharedpreferences.getString(AppsContants.UserId, "");

        Log.e("dsfsdfsdfsdf", strFLat);
        Log.e("dsfsdfsdfsdf", strFLng);
        Log.e("dsfsdfsdfsdf", strEndLat);
        Log.e("dsfsdfsdfsdf", strEndLng);
        Log.e("dsfsdfsdfsdf", strPackageID);

        ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setMessage("Please wait..");
        progressDialog.setIndeterminate(true);
        progressDialog.show();
        progressDialog.setCancelable(false);


        Date c = Calendar.getInstance().getTime();
        System.out.println("Current time => " + c);

        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control", "add_trip_history")
                .addBodyParameter("userID", strUserId)
                .addBodyParameter("packageID", strPackageID)
                .addBodyParameter("start_latitude", strFLat)
                .addBodyParameter("start_longitude", strFLng)
                .addBodyParameter("end_latitude", strEndLat)
                .addBodyParameter("end_longitude", strEndLng)
                .addBodyParameter("date", formattedDate)
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {

                            Log.e("response", response.toString());
                            if (response.getString("message").equals("add Successfully")) {
                                progressDialog.dismiss();

                                AppsContants.sharedpreferences = context.getSharedPreferences(AppsContants.MyPREFERENCES, Context.MODE_PRIVATE);
                                SharedPreferences.Editor editor = AppsContants.sharedpreferences.edit();
                                editor.putString(AppsContants.RideId, "");
                                editor.putString(AppsContants.IgnitionType, "");
                                editor.putString(AppsContants.StartIgnitiontime, "");
                                editor.putString(AppsContants.FirstIgnitionStatus, "");
                                editor.putString(AppsContants.DeviceId, "");
                                editor.putString(AppsContants.AccoundId, "");
                                editor.putString(AppsContants.Maintoken, "");
                                editor.putString(AppsContants.FIRSTLAT, "");
                                editor.putString(AppsContants.FIRSTLNG, "");
                                editor.putString(AppsContants.LASTLAT, "");
                                editor.putString(AppsContants.LASTLNG, "");
                                editor.commit();

                                Intent intent = new Intent(Intent.ACTION_DIAL);
                                intent.setData(Uri.parse("tel:918109562764"));
                                context.startActivity(intent);
                                ((Activity) context).finish();
                            } else {
                                progressDialog.dismiss();
                                Toast.makeText(context, response.getString("result"), Toast.LENGTH_SHORT).show();
                            }

                        } catch (Exception ex) {
                            progressDialog.dismiss();
                            Log.e("Catch-Block", ex.toString());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {

                        progressDialog.dismiss();
                        Log.e("onError", "Error" + anError.getMessage());
                    }
                });

    }




}