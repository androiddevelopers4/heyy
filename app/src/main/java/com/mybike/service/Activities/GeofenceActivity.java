package com.mybike.service.Activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.JSONObjectRequestListener;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.mybike.service.R;
import com.mybike.service.Utils.GPSTracker;
import com.mybike.service.Utils.HTTPCALL;
import com.mybike.service.draggable.SelectLocationActivity;

import org.json.JSONArray;
import org.json.JSONObject;

public class GeofenceActivity extends AppCompatActivity implements OnMapReadyCallback{
    GoogleMap mGoogleMap;
    SupportMapFragment mapFrag;
    GPSTracker gpsTracker;
    double lat1;
    double lng1;
    LatLng latLng;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_geofence);
        mapFrag = (SupportMapFragment) this.getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFrag.getMapAsync(this);

        gpsTracker = new GPSTracker(GeofenceActivity.this);
        lat1 = gpsTracker.getLatitude();
        lng1 = gpsTracker.getLongitude();
         latLng = new LatLng(lat1, lng1);
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mGoogleMap = googleMap;

        MapStyleOptions style = MapStyleOptions.loadRawResourceStyle(
                GeofenceActivity.this, R.raw.maps_style);
        googleMap.setMapStyle(style);
        mGoogleMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));
        GetStations();
    }

    @NonNull
    private CameraPosition getCameraPositionWithBearing(LatLng latLng) {
        return new CameraPosition.Builder().target(latLng).zoom(16).build();
    }


    public void GetStations() {

        AndroidNetworking.post(HTTPCALL.BaseUrl)
                .addBodyParameter("control","show_station")
                .build()
                .getAsJSONObject(new JSONObjectRequestListener() {
                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            String strName="";
                            LatLng latLng = null;
                            Log.e("dfsdfsdsfsd", response.toString());

                            JSONArray jsonArray= new JSONArray(response.getString("data"));
                            for (int i = 0; i < jsonArray.length(); i++) {
                                JSONObject jsonObject = jsonArray.getJSONObject(i);
                                if (!jsonObject.getString("latitude").equals(null)) {

                                    double latVehicle = Double.parseDouble(jsonObject.getString("latitude"));
                                    double lngVehicle = Double.parseDouble(jsonObject.getString("longitude"));
                                    strName=jsonObject.getString("name");
                                    latLng = new LatLng(latVehicle, lngVehicle);
                                    int heights = 120;
                                    int widths = 110;
                                    BitmapDrawable bitmapdraws = (BitmapDrawable) getResources().getDrawable(R.drawable.ddd);
                                    Bitmap bs = bitmapdraws.getBitmap();


                                    Bitmap smallMarkers = Bitmap.createScaledBitmap(bs, widths, heights, false);
                                    Marker marker = mGoogleMap.addMarker(new MarkerOptions()
                                            .position(latLng)
                                            .title(strName)
                                            .flat(true)
                                            .icon(BitmapDescriptorFactory.fromBitmap(smallMarkers))

                                    );
                                }
                            }

                            mGoogleMap.animateCamera(CameraUpdateFactory.newCameraPosition(getCameraPositionWithBearing(latLng)));

                            //mGoogleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(latLng, 16F));


                        } catch (Exception ex) {

                            Log.e("degkusdhgksdgf", ex.getMessage());
                        }
                    }

                    @Override
                    public void onError(ANError anError) {
                        Log.e("degkusdhgksdgf", "Error" + anError.getMessage());
                    }
                });

    }
}