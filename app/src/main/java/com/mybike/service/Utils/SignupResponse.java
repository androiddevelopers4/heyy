package com.mybike.service.Utils;

public class SignupResponse {


    /* {
        "status": "success",
            "message": {
        "accountId": "9007568",
                "username": "8799654654"
    }
    }*/

    private String status;
    public String getStatus() {
        return status;
    }

    public static class MessageBean {
        private String accountId;
        public String getAccountId() {
            return accountId;
        }
    }
}
